package pl.put.gi.dao.extended

import pl.put.gi.dao.IDAO
import pl.put.gi.model.extended.FitnessLog

abstract trait IFitnessLogDAO extends IDAO {
    def addLog(log: FitnessLog)
    def getAll() : List[FitnessLog]
}