package pl.put.gi.dao

/**
 * Manages the usage of other DAO in the system.
 */
abstract trait IDAOManager {

    // TODO: make this documentation be a bit more elaborate
    def dropTables()
    def createTables()
    def safeCreateTables()

    /**
     * Gets the specified type of the DAO
     */
    def getDAO[T <: IDAO](cls: Class[T]): T

    /**
     * Gets all the daos registered within IDAOManager implementation
     */
    def getDAOs(): List[IDAO]
}