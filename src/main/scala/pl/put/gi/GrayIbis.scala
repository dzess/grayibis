package pl.put.gi

import org.slf4j.LoggerFactory
import pl.put.gi.conf.PropertiesConfigurationReader
import java.io.FileInputStream
import java.io.File
import pl.put.gi.running.job.JobQueue
import pl.put.gi.settings.modules.ConfigurationModule
import pl.put.gi.settings.modules.DatabaseAccessObjectModule
import pl.put.gi.settings.InjectionManager
import pl.put.gi.settings.modules.HarvestersModule
import scopt.immutable.OptionParser
import scopt.immutable.OptionParser
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.conf.IConfigurationReader
import pl.put.gi.db.util.DatabaseUtil

/**
 * Options for command line interface of the Gray Ibis.
 */
case class GrayIbisOptions(fileName: String = null,
    createSchema: Boolean = false) {

}

/**
 * Entry point for command line running the Gray Ibis library. Has a lot
 * of limitations but shows how the runnable jar can be achieved from libraries
 * type of project such as Gray Ibis.
 */
object GrayIbis {

    private val logger = LoggerFactory.getLogger(this.getClass())

    private var configurationReader: PropertiesConfigurationReader = null

    private def configureOptions(): OptionParser[GrayIbisOptions] = {
        val parser = new OptionParser[GrayIbisOptions]() {
            def options = Seq(
                // NOTE: list all options below 
                arg("<file>", "file name with configuration") { (v: String, o: GrayIbisOptions) => o.copy(fileName = v) },
                flag("c", "create", "indicates if gray ibis is about to create schema") { (o: GrayIbisOptions) => o.copy(createSchema = true) })

        }

        return parser
    }

    private def configureDatabase(fileName: String) = {

        val file = new File(fileName)
        configurationReader = new PropertiesConfigurationReader()
        val dbHelper = configurationReader.loadDatabase(new FileInputStream(file))

        // introduce the guice overall profile system  
        InjectionManager.createInjector(
            new ConfigurationModule(),
            new HarvestersModule(),
            new DatabaseAccessObjectModule(dbHelper))
        logger.info("Guice configured")
    }

    def main(args: Array[String]): Unit = {

        logger.info("Welcome")
        val p = BuildProperties.load()
        logger.info("Project: {}", p.name)
        logger.info("Version: {}", p.version)
        logger.info("Build: {}", p.build)

        // configure parser
        val parser = configureOptions()
        val optionConfig = parser.parse(args, GrayIbisOptions())

        if (optionConfig.isEmpty) {
            logger.error("Not proper arguments. Shutting down.")
            return
        }

        optionConfig.get match {
            case GrayIbisOptions(fileName, false) => {

                logger.info("Running in the 'experiment' mode")
                configureDatabase(fileName)

                // read configuration from file
                val configuration = configurationReader.load(new FileInputStream(fileName))

                logger.info("Configuration read: {}", configuration.print())

                // run things from the configuration
                val job = new JobQueue(configuration)
                job.execute()

            }
            case GrayIbisOptions(fileName, true) => {
                logger.info("Running in the 'schema create' mode")
                configureDatabase(fileName)
                val dbUtil = InjectionManager.injector.getInstance(classOf[DatabaseUtil])
                dbUtil.createSchema()
            }
        }
        logger.info("Finished")

    }
}