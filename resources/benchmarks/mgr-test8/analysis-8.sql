﻿-- basic max query about the hiff
select
	p.key, r.id, max(fl.fitness)
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
	join "logs" l on l."runId" = r."id"
	join "fitness-log" fl on fl."logId" = l."id"
where
	p.key like 'Hiff_%'
and
	r.state = 'done'
group by r.id, p.key;



-- averages of the max fitness achieved
create or replace view v_max_fitness as 
select 
	inside.key as key,
	r.field as exchange,
	q.field as xField,
	s.field as yField,
	avg(inside.maxFitness),
	stddev(inside.maxFitness)
from
	(
	select
		p.key as key,
		r.id,
		max(fl.fitness) as maxFitness
	from 
		"runs" r join "prototypes" p on r."experimentId" = p."id"
		join "logs" l on l."runId" = r."id"
		join "fitness-log" fl on fl."logId" = l."id"
	where
		p.key like 'Hiff_%'
	and 
		r.state = 'done'
	group by r.id, p.key
	) inside
join 
	(
	select * from extract_field_text('breed\.updater\.quality')
	) q
on q.key = inside.key
join 
	(
	select * from extract_field_text('breed\.updater\.search')
	) s
on s.key = inside.key
join 
	(
	select * from extract_field_numeric('breed\.exchange-ratio')
	) r
on r.key = inside.key
group by inside.key, q.field, s.field, r.field;

create or replace view v_tan_max_fitness as
select 
	x.exchange,
	substring(x."xfield" from 'jbnc.measures.(\w+)'),
	x.avg,
	x.stddev
from v_max_fitness x
where x.yfield like 'jbnc.inducers.TANInducer';

create or replace view v_nav_max_fitness as
select 
	x.exchange,
	substring(x."xfield" from 'jbnc.measures.(\w+)'),
	x.avg,
	x.stddev
from v_max_fitness x
where x.yfield not like 'jbnc.inducers.TANInducer';

select * from v_max_fitness;
select * from v_nav_max_fitness;
select * from v_tan_max_fitness;

-- get the max and min
select * from v_max_fitness where avg = (select max(avg) from v_max_fitness);
select * from v_max_fitness where avg = (select min(avg) from v_max_fitness);

select * from extract_field_text('breed\.updater\.quality');
select * from extract_field_text('breed\.updater\.search');