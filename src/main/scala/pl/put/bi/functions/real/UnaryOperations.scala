package pl.put.bi.functions.real

import pl.put.bi.functions.UnaryOperation
import scala.math

class Exp extends UnaryOperation {
    override def toString() = "exp"
    override def combine(r: Double): Double = math.exp(r)
}

class Cos extends UnaryOperation {
    override def toString() = "cos"
    override def combine(r: Double): Double = math.cos(r)
}

class Sin extends UnaryOperation {
    override def toString() = "sin"
    override def combine(r: Double): Double = math.sin(r)
}

class Log extends UnaryOperation {
    override def toString() = "log"
    override def combine(r: Double): Double = math.log(r)
}

class ProtectedLog extends UnaryOperation {
    override def toString() = "plog"
    override def combine(r: Double): Double = if (r > 0) math.log(r) else r

}

