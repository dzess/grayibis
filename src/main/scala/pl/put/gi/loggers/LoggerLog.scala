package pl.put.gi.loggers

import ec.util.Log
import pl.put.gi.loggers.LoggerLevel._
/**
 * Implementation of Log for ECJ which dumps the logs into the SLF4J logger thing.
 */
class LoggerLog(private val _level: LogLevel, postAnnouncments: Boolean)
    extends Log(new LoggerWriter(_level), null, postAnnouncments, false) {
   
    override def restart(): Log = {
        return this
    }

    override def reopen(): Log = {
        return this
    }
}