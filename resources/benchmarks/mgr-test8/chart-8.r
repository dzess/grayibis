# This file does ploting of the data of MGR-TEST-8 using some view
# Requires: gi.setup
# Author: Piotr Jessa

gi.test8.load <- function() {
  all_dataset <- gi.load.view('v_max_fitness','8')
  return(all_dataset)
}

gi.test8.separete <- function(dataset){
  t8a <- subset(dataset,yfield = "jbnc.inducers.NaiveBayesInducer")
  t8b <- subset(dataset,yfield != "jbnc.inducers.NaiveBayesInducer")
  
}

gi.test8.chartA <- function(dataset) {
    
  p <- ggplot(t8a
              ,aes(x=exchange,y=avg, color=substring)
              ) 
  p <- p + geom_line() + geom_point(size = 4, aes(shape = substring)) 
  p <- p + geom_errorbar(limits, width = 0.02)
  p <- p + opts(legend.position = "bottom")
  p <- p + opts(legend.direction = "vertical")
  p <- p + scale_shape_discrete("")
  p <- p + scale_color_discrete("")
  p <- p + scale_x_continuous("exchange-ratio")
  p <- p + scale_y_continuous("average max fitness")
  
  ggsave("mgr8a.pdf",useDingbats=FALSE)
  return (p)
}



gi.test8.chartB <- function(dataset) {
  
  p <- ggplot(t8b
              ,aes(x=exchange,y=avg, color=substring)
  ) 
  p <- p + geom_line() + geom_point(size = 4, aes(shape = substring)) 
  p <- p + geom_errorbar(limits, width = 0.02)
  p <- p + opts(legend.position = "bottom")
  p <- p + opts(legend.direction = "vertical")
  p <- p + scale_shape_discrete("")
  p <- p + scale_color_discrete("")
  p <- p + scale_x_continuous("exchange-ratio")
  p <- p + scale_y_continuous("average max fitness")
    
  ggsave("mgr8b.pdf",useDingbats=FALSE)
  return (p)
}