package pl.put.gi.dao.extended.impl

import org.slf4j.LoggerFactory
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.dao.extended.IMessageLogDAO
import pl.put.gi.model.extended.MessageLog
import com.google.inject.Inject

/**
 * Manages the ORM for those classes using the LogDAOBase.
 *
 * The FitnessLogDAO loads the logs eagerly alongside with the run object.
 */
class MessageLogDAO @Inject() (dbHelper: IDatabaseHelper) extends LogDAOBase(dbHelper, MessageLog, MessageLog) with IMessageLogDAO {

    private val logger = LoggerFactory.getLogger(classOf[MessageLogDAO])

    override def addLog(log: MessageLog) = this.addLogBase(log)
    override def getAll(): List[MessageLog] = this.getAllBase()
}