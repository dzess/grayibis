﻿-- determine workload about the current database

select
	p."key", count(*)
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
where 
	r.state = 'done'
group by p."key"
having count(*) = 10;

-- how many to be done
select count(*) from "prototypes-ecj" ep join "prototypes" p on ep."prototypeId" = p.id;

-- how many done already
select count(*)
from (
	select
		p."key", count(*)
	from 
		"runs" r join "prototypes" p on r."experimentId" = p."id"
	where 
		r.state = 'done'
	group by 
		p."key"
	having 
		count(*) = 10
)x;
	
