package pl.put.bi.data

import ec.gp.GPData

/**
 * Models the evaluated outcome of the Tree Based Program
 */
class DoubleData extends GPData {

    var y: Double = 0

    override def copyTo(other: GPData) = {
        other.asInstanceOf[DoubleData].y = this.y
    }
}