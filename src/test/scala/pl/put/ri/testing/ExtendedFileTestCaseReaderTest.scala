package pl.put.ri.testing

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import pl.put.regression.ITestCaseReader
import org.scalatest.BeforeAndAfterEach
import pl.put.gi.util.scala.FileUtils.using
import java.io.FileWriter
import java.io.File
import org.scalatest.mock.MockitoSugar
import ec.EvolutionState
import ec.util.Parameter
import ec.util.ParameterDatabase
import org.mockito.Mockito
import org.mockito.Matchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import pl.put.regression.ExtendedFileTestCaseReader
import pl.put.regression.RegressionCase

@RunWith(classOf[JUnitRunner])
class ExtendedFileTestCaseReaderTest extends FunSuite
    with BeforeAndAfterEach with MockitoSugar {

    private var extReader: ITestCaseReader = null

    private var evolutionState: EvolutionState = null
    private var parametersDatabase: ParameterDatabase = null
    private var parameter: Parameter = null

    private val fileName = "test-case-file-name"

    override def beforeEach() = {
        extReader = new ExtendedFileTestCaseReader()

        evolutionState = mock[EvolutionState]
        parametersDatabase = mock[ParameterDatabase]
        parameter = mock[Parameter]

        // set up that returning the parameter from ECJ like  
        // gets the proper file 
        evolutionState.parameters = parametersDatabase

        Mockito.when(parametersDatabase.getFile(Matchers.any[Parameter], Matchers.any[Parameter]))
            .thenReturn(new File(fileName))
    }

    override def afterEach() = {
        val f = new File(fileName)
        if (f.exists) {
            f.delete()
        }
    }

    def writeContentIntoFile(input: String) = {
        using(new FileWriter(fileName)) {
            f => f.write(input)
        }
    }

    test("reading empty file results in empty sequence of test cases") {
        val emptyString = ""
        writeContentIntoFile(emptyString)

        val result = extReader.load(evolutionState, parameter)

        assert(result != null)
        expect(0)(result.size)
    }

    test("reading standard file with values") {
        val content = """0 0
            1 1
            2 2
            """
        writeContentIntoFile(content)

        val result: Seq[(RegressionCase, Double)] = extReader.load(evolutionState, parameter)

        assert(result != null)
        expect(3)(result.size)

        expect(true)(result.exists(x => x._2 == 0.0))
        expect(true)(result.exists(x => x._2 == 1.0))
        expect(true)(result.exists(x => x._2 == 2.0))
    }

    test("reading file with comment reads those line not") {
        val content = """# This is comment line and should not be read
            0 0
            1 1
            2 2
            # 3 3
            """
        writeContentIntoFile(content)

        val result: Seq[(RegressionCase, Double)] = extReader.load(evolutionState, parameter)

        assert(result != null)
        expect(3)(result.size)

        expect(true)(result.exists(x => x._2 == 0.0))
        expect(true)(result.exists(x => x._2 == 1.0))
        expect(true)(result.exists(x => x._2 == 2.0))
    }

    test("reading the optional size of the test cases") {
        (pending)
        // this functionality is yet to be implemented
    }

    test("reading the optional arity of the test cases") {
        (pending)
        // this functionality is yet to be implemented
    }

}