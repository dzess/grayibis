package pl.put.gi.testing.dao.extended

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite

import com.google.inject.Guice

import pl.put.gi.dao.extended.ExtendedDAOManager
import pl.put.gi.dao.extended.IFitnessLogDAO
import pl.put.gi.dao.ILogDAO
import pl.put.gi.dao.IPrototypeDAO
import pl.put.gi.dao.IRunDAO
import pl.put.gi.dao.IDAOManager
import pl.put.gi.db.TestDatabaseHelper
import pl.put.gi.settings.modules.DatabaseAccessObjectModule

@RunWith(classOf[JUnitRunner])
class ExtendedDAOManagerTest extends FunSuite with BeforeAndAfterEach {

    private val dbHelper = new TestDatabaseHelper()
    private var daoManager: IDAOManager = null

    // lock object is necessary because of the ddl operations on the database
    private val lockObject = new Object()

    override def beforeEach() = {
        lockObject.synchronized {
            val injector = Guice.createInjector(new DatabaseAccessObjectModule(dbHelper))
            daoManager = injector.getInstance(classOf[ExtendedDAOManager])
            daoManager.safeCreateTables()
        }
    }

    override def afterEach() = {
        lockObject.synchronized {
            daoManager.dropTables()
        }
    }

    test("can get hold of 3 basic daos") {
        lockObject.synchronized {
            val rDao = daoManager.getDAO(classOf[IRunDAO])
            val pDao = daoManager.getDAO(classOf[IPrototypeDAO])
            val lDao = daoManager.getDAO(classOf[ILogDAO])

            assert(rDao != null)
            assert(pDao != null)
            assert(lDao != null)
        }
    }

    test("can get hold of fitness log dao") {
        lockObject.synchronized {
            val fitnessDao = daoManager.getDAO(classOf[IFitnessLogDAO])
            assert(fitnessDao != null)
        }
    }

    test("the number of daos is 5") {
        lockObject.synchronized {
            val result = daoManager.getDAOs()
            assert(result.size == 6)
        }
    }
}