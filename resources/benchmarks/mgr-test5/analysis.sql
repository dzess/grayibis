﻿select * from "prototypes" p join "prototypes-ecj" ep on ep."prototypeId" = p.id;
select * from "runs";
select * from "logs";


-- all the runs
select
	p.key, count(*) as total
from 
 	"runs" r join "prototypes" p on r."experimentId" = p."id"
where
	p.key like 'OneMax_0_%' and r.state = 'done'
group by p.key;

-- get keys of one exchange ratio
select * from extract_field_numeric('breeder\.exchange\-ratio') where field = 0.5;

select * from extract_field_text('breed\.updater\.quality') where key like 'Reworked_%';
select * from extract_field_text('breed\.updater\.search') where key like 'Reworked_%';


-- basic query about the fitness
select
	fl.fitness,
	p.key
from  "runs" r 
join "prototypes" p on r."experimentId" = p."id" 
join "logs" l on l."runId" = r.id
join "fitness-log" fl on fl."logId" = l.id
where 
  p.key in (select key from extract_field_numeric('breeder\.exchange\-ratio') where field = 0.5)
and 
  r.state = 'done';

-- get the aggregations of fitnesses
create or replace view v_max_fitness as
select
  p.key as experimentKey,
  x.field as xField,
  y.field as yField,
  max(fl.fitness) as maximalFitness,
  avg(fl.fitness) as avgFitness,
  stddev_samp(fl.fitness) as stdFitness,
  count(*) as numberOfMeasures
from  "runs" r 
join "prototypes" p on r."experimentId" = p."id" 
left outer join "logs" l on l."runId" = r.id
left outer join "fitness-log" fl on fl."logId" = l.id
join (select * from extract_field_text('breed\.updater\.quality') where key like 'Reworked_%' ) x on p.key = x.key
join (select * from extract_field_text('breed\.updater\.search') where key like 'Reworked_%' ) y on p.key = y.key
where 
  p.key like 'Reworked_%'
and 
  r.state = 'done'
group by 
  p.key,
  x.field,
  y.field
having 
  max(fl.fitness) is not null
order by maximalFitness desc;

-- go with runs and options
select r.state,p.key,x.field,y.field, count(*) as total
from "runs" r 
join "prototypes" p on r."experimentId" = p."id" 
join (select * from extract_field_text('breed\.updater\.quality') where key like 'Reworked_%' ) x on p.key = x.key
join (select * from extract_field_text('breed\.updater\.search') where key like 'Reworked_%' ) y on p.key = y.key
where p.key like 'Reworked_%'
group by r.state,p.key,x.field, y.field;

-- helper queries
select p.key from "prototypes" p where p.key like 'Reworked_%';
select * from extract_field_text('breed\.updater\.quality') where key like 'Reworked_%';
select * from extract_field_text('breed\.updater\.search') where key like 'Reworked_%';

