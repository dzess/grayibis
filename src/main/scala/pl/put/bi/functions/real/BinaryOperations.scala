package pl.put.bi.functions.real

import ec.EvolutionState
import ec.gp.GPData
import ec.gp.ADFStack
import ec.Problem
import ec.gp.GPIndividual
import pl.put.bi.functions.BinaryOperation

class Add extends BinaryOperation {
    override def toString() = "+"
    override def combine(r1: Double, r2: Double): Double = r1 + r2
}

class Sub extends BinaryOperation {
    override def toString() = "-"
    override def combine(r1: Double, r2: Double): Double = r1 - r2
}

class Mul extends BinaryOperation {
    override def toString() = "*"
    override def combine(r1: Double, r2: Double): Double = r1 * r2
}

class Div extends BinaryOperation {
    override def toString() = "/"
    override def combine(r1: Double, r2: Double): Double = r1 / r2
}

class ProtectedDiv extends BinaryOperation {
    override def toString() = "pdiv"
    override def combine(r1: Double, r2: Double): Double = if (r2 != 0) r1 / r2 else r1
}

class Power extends BinaryOperation {
    override def toString() = "^"
    override def combine(r1: Double, r2: Double): Double = math.pow(r1, r2)
}

class Max extends BinaryOperation {
    override def toString() = "max"
    override def combine(r1: Double, r2: Double): Double = math.max(r1, r2)
}

class Min extends BinaryOperation {
    override def toString() = "min"
    override def combine(r1: Double, r2: Double): Double = math.min(r1, r2)
}



