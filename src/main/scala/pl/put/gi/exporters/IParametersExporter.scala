package pl.put.gi.exporters

import java.io.OutputStream
import pl.put.gi.model.Prototype

/**
 * Defines interface for exporting file.
 */
abstract trait IParametersExporter {

    /**
     * Exports the data from Prototype to stream.
     */
    def export(prototype: Prototype, stream: OutputStream)
}