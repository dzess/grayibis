package pl.put.gi

import org.slf4j.LoggerFactory
import db.IDatabaseHelper
import pl.put.gi.util.IFreezable
import pl.put.gi.util.IValidable
import pl.put.gi.running.IRunnerConfigurationProvider
import pl.put.gi.running.IRunnerConfigurationProvider
import pl.put.gi.dao.extended.ExtendedDAOManager
import pl.put.gi.running.IRunnerProvider
import pl.put.gi.running.IRunnerProvider
import pl.put.gi.util.IPrintable

/**
 * Describes the configuration of single Run in Gray Ibis. This class
 * is really the smallest possible model for configuration of advanced experiments.
 *
 * Actually the model configuration is Freezable and Validable set of common classes
 * used by most of IJob implementations.
 *
 * This is model configuration, behind it the various representations might be used. Classes which
 * implement IConfigurationReader trait should be responsible for storage. So what
 * actually is stored within file or database might be different than content of this class.
 *
 * Behavior of the created instances of Configuration should be identical.
 */
class Configuration extends IFreezable with IValidable with IPrintable {

    private val logger = LoggerFactory.getLogger(classOf[Configuration])

    // default compound systems
    var _dbHelper: IDatabaseHelper = null
    var _runConfigurationProvider: IRunnerConfigurationProvider = null
    var _runProvider: IRunnerProvider = null

    // properties
    def dbHelper = _dbHelper
    def dbHelper_=(db: IDatabaseHelper) = this.safeSetter(db) { x => _dbHelper = x }

    def runConfigurationProvider = _runConfigurationProvider
    def runConfigurationProvider_=(provider: IRunnerConfigurationProvider) = this.safeSetter(provider) {
        x => _runConfigurationProvider = x
    }

    def runProvider = _runProvider
    def runProvider_=(provider: IRunnerProvider) = this.safeSetter(provider) {
        x => _runProvider = x
    }

    override def validate() = {

        logger.info("Validating the configuration object {}", this.toString())

        if (_dbHelper == null) {
            throw new IllegalStateException("database helper not set")
        }

        if (_runConfigurationProvider == null) {
            throw new IllegalStateException("run configuration provider not set")
        }

        // NOTE: add here validation
        _dbHelper.validate()
        _runConfigurationProvider.validate()
        _runProvider.validate()

        logger.info("Validating the configuration object {} successful", this.toString())
    }

    override def freeze() = {

        // validate before freezing
        this.validate()

        super.freeze()

        // NOTE: list here all of the compound configuration subsystems
        _dbHelper.freeze()
        _runConfigurationProvider.freeze()
        _runProvider.freeze()
    }

    override def print(): String = {
        var builder = new StringBuilder()
        builder = this.insertHeader(builder)
        builder = this.insertBeginMark(builder)

        builder.append("DBHelper.URL = " + dbHelper.currentURL + "\n")
        builder.append("DBHelper.Driver = " + dbHelper.driver + "\n")

        // TODO: dump information about read parameters ? or so

        builder = this.insertEndMark(builder)
        builder.toString()
    }
}

object Configuration {

    /**
     * Returns the default configuration for the Gray Ibis
     */
    def default: Configuration = {
        val newConf = new Configuration()

        // actually nothing should be used as default

        return newConf
    }
}