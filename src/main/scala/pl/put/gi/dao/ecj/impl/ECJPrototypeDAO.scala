package pl.put.gi.dao.ecj.impl

import org.scalaquery.ql.basic.BasicDriver.Implicit._
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.ql.basic.BasicQueryTemplate
import org.scalaquery.ql.Parameters
import org.slf4j.LoggerFactory
import pl.put.gi.dao.ecj.IECJPrototypeDAO
import pl.put.gi.dao.impl.BaseDAO
import pl.put.gi.dao.impl.PrototypeDAO
import pl.put.gi.dao.IPrototypeDAO
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.model.Prototype
import org.scalaquery.ql.Join
import com.google.inject.Inject

/**
 * The ECJ enhanced version of the Prototype - meant to be used as lynchpin for the
 * other ECJ like modifications of Prototype.
 */
class ECJPrototypeDAO @Inject() (dbHelper: IDatabaseHelper) extends BaseDAO(dbHelper) with IECJPrototypeDAO {

    private val logger = LoggerFactory.getLogger(classOf[ECJPrototypeDAO])

    private val prototypeDAO: IPrototypeDAO = new PrototypeDAO(dbHelper)

    // queries templates
    private val getECJPrototypesQuery = this.generateAllQuery(ECJPrototype)
    private val getECJPrototypeByIdQuery = this.generateOneQuery(ECJPrototype)
    private val maxIdQuery = this.generateMaxQuery(ECJPrototype)

    private def remapPrototypeToECJPrototype(p: Prototype, ep: ECJPrototype) = {
        // manual remapping
        ep.parameters = p.parameters
        ep.key = p.key
        ep.description = p.description
    }

    override def getPrototypes(): List[ECJPrototype] = {
        var prototypes: List[ECJPrototype] = null

        this.db.withSession {
            val rl = getECJPrototypesQuery.list
            prototypes = rl.map(x => ECJPrototype.toModel(x)).toList
        }

        // side effect code
        prototypes.foreach(ep => {
            val p = prototypeDAO.getPrototype(ep.id)
            remapPrototypeToECJPrototype(p, ep)
        })

        prototypes
    }

    override def getPrototypeByKey(key: String): ECJPrototype = {

        logger.debug("Quering DB for ECJPrototype with key '{}'", key)

        val query = for {
            Join(p, ep) <- Prototype innerJoin ECJPrototype on (_.id is _.prototypeId)
            if p.key === key
        } yield ep.id

        var epId: Option[Long] = None
        this.db.withSession {
            epId = query.firstOption

            logger.debug("Found the ID of the joined ECJPrototype - {}", epId)

            if (epId.isEmpty) {
                return null
            }
        }

        this.getPrototype(epId.get)
    }

    override def getPrototype(id: Long): ECJPrototype = {

        logger.debug("Quering DB for ECJPrototype wth id '{}'", id)
        var ep: ECJPrototype = null
        this.db.withSession {
            val result = getECJPrototypeByIdQuery(id).firstOption
            if (result.isEmpty) {
                return null
            }

            ep = ECJPrototype.toModel(result.get)
        }

        val p = prototypeDAO.getPrototype(id)
        remapPrototypeToECJPrototype(p, ep)
        return ep
    }

    /**
     * Saves the ECJPrototype into DB.
     *
     * Works almost identically to normal prototype save.
     */
    override def save(prototype: ECJPrototype) = {

        // note that this code should be created the other way (this ORM is bad)
        // the session of database should be passed down to all the laying down 
        // daos to make creation atomic, by now saving goes into two steps
        prototypeDAO.save(prototype)

        val isObjectInDatabase = this.isObjectInDB(prototype, getPrototype)

        if (!isObjectInDatabase) {
            logger.debug("Object not in DB - creating")
            this.db.withSession {
                val tuple = ECJPrototype.fromModel(prototype)
                ECJPrototype.insert(tuple)
            }
        } else {
            logger.debug("Object already in DB - updating")
            this.db.withSession {

                val updaterQuery = for { p <- ECJPrototype if p.id === prototype.id } yield p
                val tuple = ECJPrototype.fromModel(prototype)
                val updatedRows = updaterQuery.update(tuple)

                logger.debug("Number of row updates - {}", updatedRows)
            }

        }
        logger.debug("Save finished")
    }

    override def createTable() = this.recreateTableBase(ECJPrototype)
    override def dropTable() = this.dropTableBase(ECJPrototype)
    override def isTableCreated(): Boolean = this.isTableCreatedBase(ECJPrototype)

}