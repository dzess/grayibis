package pl.put.gi.testing.learn

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import pl.put.gi.db.MySQLHelper
import org.scalaquery.session.Database
// FIXME: this is not so good result of the 
import org.scalaquery.ql.extended.MySQLDriver.Implicit._ 
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.session.Database

import pl.put.gi.testing.utils.TestTable

@RunWith(classOf[JUnitRunner])
class ConnectingToMySQLTest extends FunSuite {

    val helper = new MySQLHelper("jdbc:mysql://localhost/mgr-test?user=dzess&password=qwerty")

    // this test has been disabled because is environment dependent
    ignore("established the connection to the MySQL") {
        val db = Database.forURL(helper.currentURL, driver = helper.driver)

        db.withSession {
            TestTable.ddl.create

            TestTable.insert(1L, "foo")

            val query = for { t <- TestTable } yield t

            val result = query.first

            assert(result._1 === 1L)
            assert(result._2 === "foo")

            TestTable.ddl.drop
        }
    }
}