package pl.put.ri.mapper

import org.spiderland.Psh.Program
import org.spiderland.Psh.Interpreter

/**
 * Interface for connecting the genome of shorts with Psh program code.
 */
abstract trait ICodeMapper {
    
    def setInterpreter(interpreter : Interpreter)

    def toProgram(genome: Array[Short]): Program

    def toGeome(program: Program): Array[Short]
}