package pl.put.gi.model.extended.fitness

import pl.put.gi.model.extended.NamedFitnessLogTable
import pl.put.gi.settings.Constants

object KozaFitnessLog extends NamedFitnessLogTable(Constants.KOZA_FITNESS_TABLE) {

}

object HitsKozaFitnessLog extends NamedFitnessLogTable(Constants.KOZA_HITS_FITNESS_TABLE) {

}