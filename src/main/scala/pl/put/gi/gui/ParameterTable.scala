package pl.put.gi.gui

import scala.swing._
import scala.swing.event._

import javax.swing.JComponent
import java.awt.event.MouseEvent
import scala.collection.mutable.Set

import pl.put.gi.gui.ui.PopupMenu
import pl.put.gi.gui.table.ParameterTableCellRender
import pl.put.gi.gui.table.ParametersTableModel
import org.slf4j.LoggerFactory

/**
 * Stores the User Interface of the table which can be for various parameters
 */
class ParameterTable(
    private val leftClickFunc: (Set[(Int, Int)], ParametersTableModel) => Unit = null,
    private val addFunc: (ParametersTableModel) => Unit = null,
    private val removeFunc: (ParametersTableModel) => Unit = null) {

    private val logger = LoggerFactory.getLogger(classOf[ParameterTable])

    private val tableViewPopup = new PopupMenu("") {

        val addItem = new MenuItem("Add") {
            reactions += {
                case ButtonClicked(_) => {
                    logger.debug("Menu Item 'Add' clicked")
                    addFunc(tableModel)
                }
            }
        }

        val removeItem = new MenuItem("Remove") {
            reactions += {
                case ButtonClicked(_) => {
                    logger.debug("Menu Item 'Remove' clicked")
                    removeFunc(tableModel)
                }
            }
        }

        contents += addItem
        contents += removeItem

        def hideRemoval(input: Boolean) = {
            removeItem.visible = !input
        }
    }

    private val headers = List("Key", "Value")
    private val tableModel = new ParametersTableModel(headers)
    private val tableView = new Table(1, 2) {
        background = java.awt.Color.WHITE
        model = tableModel

        val cellRenderer = new ParameterTableCellRender()

        // setting default render
        override protected def rendererComponent(isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int) = {
            Component.wrap(cellRenderer.getTableCellRendererComponent(peer, model.getValueAt(row, column), isSelected, hasFocus, row, column).asInstanceOf[JComponent])
        }

        // restrict selection only to rows
        selection.elementMode = Table.ElementMode.Row

        listenTo(mouse.clicks)
        reactions += {
            case e: MouseClicked => {
                logger.debug("Table clicked at '{}'", e.point)

                if (e.peer.getButton == MouseEvent.BUTTON3) {
                    tableViewPopup.hideRemoval(false)
                    tableViewPopup.show(this, e.point.x, e.point.y)
                    tableModel.fireTableStructureChanged()
                } else if (e.peer.getButton == MouseEvent.BUTTON1) {
                    val selectedCells = selection.cells
                    leftClickFunc(selectedCells, tableModel)
                }
                refreshTable()
            }
        }
    }

    private val tableViewScrollPane = new ScrollPane(tableView) {
        listenTo(mouse.clicks)
        reactions += {
            case e: MouseClicked => {
                logger.debug("Clicked on a scroll pane at {}", e.point)

                // this is duplicate of the code above from the 
                if (e.peer.getButton == MouseEvent.BUTTON3) {
                    tableViewPopup.hideRemoval(true)
                    tableViewPopup.show(this, e.point.x, e.point.y)
                    tableModel.fireTableStructureChanged()
                }

            }
        }
    }

    def view: Component = tableViewScrollPane
    def model = tableModel

    def clear(): Unit = {
        tableModel.rowData = Seq()
    }

    def refreshTable(): Unit = {

        tableView.revalidate()
        tableViewScrollPane.revalidate()

        tableView.repaint()
        tableViewScrollPane.repaint()
    }
}