package pl.put.gi.testing.model.ecj

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfterEach
import pl.put.gi.model.ecj.ECJPrototype
import org.scalatest.mock.MockitoSugar
import pl.put.gi.running.ecj.IStatisticsHarvester
import pl.put.gi.model.ecj.util.HarvesterCoder
import pl.put.gi.settings.InjectionManager
import com.google.inject.AbstractModule

/**
 * Tests if setting various vales on the ECJPrototype manages to keep options consistent
 */
@RunWith(classOf[JUnitRunner])
class ECJPrototypeHarvestersTest extends FunSuite with BeforeAndAfterEach with MockitoSugar {

    private var prototype: ECJPrototype = null
    private var mock1: IStatisticsHarvester = null

    override def beforeEach() = {

        prototype = new ECJPrototype()
        mock1 = mock[IStatisticsHarvester]

        val dummyHarvestersModule = new AbstractModule() {
            override def configure() = {
                bind(classOf[IStatisticsHarvester]).toInstance(mock1)
            }
        }
        InjectionManager.createInjector(dummyHarvestersModule)
    }

    test("new prototype has empty harvesters sequence") {
        assert(prototype.harvesters.isEmpty === true)
    }

    test("new proptotype has empty options") {
        assert(prototype.options.isEmpty)
    }

    test("saving harverster adds proper value into options map") {

        prototype.harvesters = Seq(mock1)

        expect(1)(prototype.options.size)
        prototype.options.contains(prototype.entryHarvesters)
    }

    test("changing options map results in updated harvesters settings") {
        val coder = new HarvesterCoder()
        val input = coder.encode(Seq(mock1))

        prototype.options = Map(prototype.entryHarvesters -> input)

        expect(1)(prototype.harvesters.size)
        assert(prototype.harvesters.head.getClass === mock1.getClass)

    }

}