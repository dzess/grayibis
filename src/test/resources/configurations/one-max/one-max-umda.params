# The parameters that were laid out in One Max Problem the simples
# possible EDABreeder - thus the EDA algorithm configured with UMDA models

verbosity	= 0

breedthreads	= 1
evalthreads	= 1
seed.0		= 4357

state		= ec.simple.SimpleEvolutionState

pop		= ec.Population

# The Initializer can be left the initial one, because the initial population 
# is the random vector set of individuals
init		= ec.simple.SimpleInitializer
finish		= ec.simple.SimpleFinisher

# The Breeder is not the default one (the configurable EDA breeder)
# which need to be configured with sampler and updater classes
breed		= pl.put.poznan.eda.SimpleEDABreeder
breed.updater = pl.put.poznan.eda.umda.DenseUMDAUpdater
breed.sampler = pl.put.poznan.eda.umda.DenseUMDASampler

eval		= ec.simple.SimpleEvaluator
stat		= ec.simple.SimpleStatistics
exch		= ec.simple.SimpleExchanger

generations		= 200
quit-on-run-complete	= true
checkpoint		= false
prefix			= ec
checkpoint-modulo	= 1

# TODO: make logging configured a bit more elaborate
stat.file		= $out.stat

# Use the only one subpopulation (and what kind of subpopulation) 
# For EDA model subpopulation is required and setting the model class
# is also required. Note that model class should be compliant with
# the updater and sampler classes.
pop.subpops		= 1
pop.subpop.0		= pl.put.poznan.eda.ModelSubpopulation
pop.subpop.0.model = pl.put.poznan.eda.umda.DenseUMDAModel

# those attributes are specific to the implemented subpopulation
# in case of model subpopulation size is the sampling size  
pop.subpop.0.size 		= 25
pop.subpop.0.duplicate-retries 	= 0
pop.subpop.0.species 		= ec.vector.VectorSpecies

pop.subpop.0.species.fitness 	= ec.simple.SimpleFitness
pop.subpop.0.species.ind	= ec.vector.BitVectorIndividual

# The EDA algorithm uses the only genome size values
pop.subpop.0.species.genome-size	= 20

# Crossover and mutation should be turned off (no mutation in pipeline)
pop.subpop.0.species.crossover-type	= one
pop.subpop.0.species.mutation-prob	= 0.00

# The Breeding Pipeline is not so important - because only the selection methods are important
pop.subpop.0.species.pipe = ec.breed.ReproductionPipeline
pop.subpop.0.species.pipe.source.0 = ec.select.TournamentSelection

# Selection tweaks
select.tournament.size		= 10

# Use standard problem such as MaxOnes
eval.problem		= ec.app.tutorial1.MaxOnes
