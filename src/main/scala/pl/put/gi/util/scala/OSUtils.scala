package pl.put.gi.util.scala

/**
 * Util for determining the operating system in which program works
 */
object OSUtils {
  val os = System.getProperty("os.name").toLowerCase();

  def isWindows: Boolean = os.indexOf("win") >= 0
  def isMac: Boolean = os.indexOf("mac") >= 0
  def isUnix: Boolean = os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0
  def isSolaris: Boolean = os.indexOf("sunos") >= 0

}