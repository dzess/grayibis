package pl.put.gi.loaders

import java.util.Properties
import java.io.File
import java.io.FileInputStream
import org.slf4j.LoggerFactory
import java.util.Map.Entry
import scala.collection.JavaConversions._
import scala.util.matching.Regex

/**
 * Loads the properties from the pinpointed location. If file contains the parent directive
 * then the parent file is loaded first. The sequence of files is preserved during load.
 */
class HierarchicalFileParametersLoader(private val path: String) extends IParametersLoader {

    private val logger = LoggerFactory.getLogger(classOf[HierarchicalFileParametersLoader])

    // for checking if there is no problem with dependencies
    private var alreadyLoaded = Set[String]()

    override def loadProperties(): Map[String, String] = {
        // this class should be called not only once
        alreadyLoaded = Set[String]()
        loadProperties(path)
    }

    private def isParent(e: Entry[Object, Object]): Boolean = {
        val key = e.getKey().toString()

        if (key.startsWith("parent")) {
            logger.debug("Found dependency on parent file with key {}", key)
            true
        } else {
            false
        }
    }

    private def toParentTuple(e: Entry[Object, Object]): (String, Int) = {
        val (key, value) = toNormalTuple(e)

        // search for regular expression like parent.{number} with group name number
        val reg = new Regex("""parent\.(\d+)""", "number")

        val orderingNumber: Int = reg findFirstIn (key) match {
            case Some(reg(number)) => number.toInt
            case _ => throw new IllegalStateException("Parent is not matched without ordering number")
        }

        (value, orderingNumber)
    }

    private def toNormalTuple(e: Entry[Object, Object]): (String, String) = {
        val key = e.getKey().toString()
        val value = e.getValue().toString()
        (key, value)
    }

    private def loadProperties(inputPath: String): Map[String, String] = {

        val leafProperties = new Properties()
        val file = new File(inputPath)

        if (!file.exists) {
            throw new IllegalArgumentException("Not found file " + inputPath)
        }

        try {
            leafProperties.load(new FileInputStream(file))
        } catch {
            case e: Exception => {
                logger.error("Problem reading the params file", e)
                throw new RuntimeException(e)
            }
        }

        alreadyLoaded += inputPath

        // Int is ordering number of parent location
        val parents: Seq[(String, Int)] = leafProperties.entrySet()
            .filter(x => isParent(x))
            .map(x => toParentTuple(x))
            .toSeq

        // check for duplicates
        if (parents.exists(parent => alreadyLoaded.contains(parent._1))) {
            throw new IllegalArgumentException("Cycles found in the parental relations")
        }

        // sort by ordering number
        val parentsSorted = parents.sortWith((t1, t2) => t1._2 < t2._2)

        var intermediateMap = Map[String, String]()
        parentsSorted.foreach(parent => {
            val location = getLocation(parent._1, inputPath)
            logger.debug("Loading parent number {} from location {}", parent._2, location)

            intermediateMap = intermediateMap ++ loadProperties(location)
        })

        val normalValues = leafProperties.entrySet()
            .filter(x => !isParent(x))
            .map(x => toNormalTuple(x))
            .toMap

        intermediateMap = intermediateMap ++ normalValues
        intermediateMap
    }

    private def getLocation(parentPath: String, filePath: String): String = {
        // get absolute path of the current location 
        val absParent = new File(parentPath).getAbsolutePath
        if (absParent.equals(parentPath)) {
            // user provided absolute path in parent location path 
            // this is unusual but possible
            parentPath
        } else {
            // relative path
            val current = new File(filePath)
            val absCurrent = current.getAbsolutePath
            var parentCurrentPath = new File(absCurrent).getParent()
            val directoryCurrent = new File(parentCurrentPath)
            val absDirectoryCurrent = directoryCurrent.getAbsolutePath
            
            val newPath = absDirectoryCurrent + File.separator + parentPath
            new File(newPath).getAbsolutePath
        }
    }
}