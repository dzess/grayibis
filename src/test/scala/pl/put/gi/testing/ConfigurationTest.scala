package pl.put.gi.testing

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.mock.MockitoSugar
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite
import pl.put.gi.Configuration
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.running.IRunnerConfigurationProvider
import org.mockito.Mockito._
import org.mockito.Matchers._
import pl.put.gi.running.IRunnerProvider

@RunWith(classOf[JUnitRunner])
class ConfigurationTest extends FunSuite with MockitoSugar with BeforeAndAfterEach {

    var conf: Configuration = null
    val dbHelperMock = mock[IDatabaseHelper]
    val runConfigurationProviderMock = mock[IRunnerConfigurationProvider]
    val runProviderMock = mock[IRunnerProvider]

    test("validations are called before freeze and deafult configuration is not valid") {
        conf = Configuration.default

        intercept[IllegalStateException] {
            conf.freeze()
        }
    }

    test("creating the job freezes the all configurations and compound element too") {
        conf.dbHelper = dbHelperMock
        conf.runConfigurationProvider = runConfigurationProviderMock
        conf.runProvider = runProviderMock

        conf.freeze()

        assert(conf.isFrozen())
        
        verify(dbHelperMock,times(1)).validate()
        verify(runConfigurationProviderMock,times(1)).validate
        verify(runProviderMock,times(1)).validate()
        
        verify(dbHelperMock,times(1)).freeze()
        verify(runConfigurationProviderMock,times(1)).freeze()
        verify(runProviderMock,times(1)).freeze()
        
    }

    test("settng null on safe setter property invokes exception") {
        conf = Configuration.default

        intercept[IllegalArgumentException] {
            conf.dbHelper = null
        }

        intercept[IllegalArgumentException] {
            conf.runConfigurationProvider = null
        }
    }
}