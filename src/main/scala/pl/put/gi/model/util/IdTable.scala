package pl.put.gi.model.util

import org.scalaquery.ql.basic.BasicTable
import org.scalaquery.ql.NamedColumn

/**
 * Table to be used with DAO half implemented.
 */
abstract class IdTable[T](tableName: String) extends BasicTable[T](tableName) {

    def id: NamedColumn[Long]
}