package pl.put.gi.running

import pl.put.gi.util.IFreezable
import pl.put.gi.util.IValidable

/**
 * Factory class responsible for producing the IRunner implementation
 */
abstract trait IRunnerProvider extends IFreezable with IValidable {

    /**
     * Produces the implementation of already configured IRunner instance ready to
     * be run.
     */
    def produce(runConfiguration: RunnerConfiguration): IRunner
}