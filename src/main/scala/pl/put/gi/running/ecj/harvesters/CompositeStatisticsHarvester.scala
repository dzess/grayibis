package pl.put.gi.running.ecj.harvesters

import org.slf4j.LoggerFactory

import ec.Statistics

import pl.put.gi.model.Run
import pl.put.gi.running.ecj.IStatisticsHarvester

/**
 * Allows composing various statistics harvester to be used as one and transparently
 * for the runner, especially ECJRunner.
 */
class CompositeStatisticsHarvester(private val harvesters: Seq[IStatisticsHarvester]) extends IStatisticsHarvester {

    private val logger = LoggerFactory.getLogger(classOf[CompositeStatisticsHarvester])

    if (harvesters == null) {
        throw new IllegalArgumentException("composite harvesters cannot be constructed from null sequence")
    }

    if (harvesters.size < 1) {
        throw new IllegalArgumentException("composite harvesters cannot be construted from empty sequence")
    }

    override def onStart(statistic: Statistics, run: Run) = {
        logger.debug("On Start Statisitcs invoked with {}", statistic)
        harvesters.foreach(h => h.onStart(statistic, run))
    }

    override def onFinish(statistic: Statistics, run: Run) = {
        logger.debug("On Finish Statisitcs invoked with {}", statistic)
        harvesters.foreach(h => h.onFinish(statistic, run))
    }

    override def onChange(statistic: Statistics, run: Run) = {
        logger.debug("On Change Statisitcs invoked with {}", statistic)
        harvesters.foreach(h => h.onChange(statistic, run))
    }
}