package pl.put.gi.use.onemax

import org.slf4j.LoggerFactory
import pl.put.gi.use.ExampleRunnerBase

/**
 * Example that shows how to use Gray Ibis
 * codes.
 *
 * This class shows the utility to put prototype data
 * into database, how to configure the GrayIbis and how to
 * run sample tests.
 *
 * This class uses the EDA algorithms to run the data especially the
 * UMDA algorithm.
 *
 * Note that the example used here is the simples possible UMDA implementation
 * the proper implementation (using the fully fledged extension points)
 * is done in OneMaxProblemRunnerUMDA.
 */
object OneMaxProblemRunnerSimpleUMDA extends App with ExampleRunnerBase {

    private val logger = LoggerFactory.getLogger(this.getClass)

    // data preparation - read from files and insert into
    // database using the provided tools
    logger.info("Starting the data preparation")

    experimentKey = "OneMax_1"
    experimentDescription = "One Max Problem with Simple EDA solution"
    propertiesPath = "src/test/resources/configurations/one-max/one-max-umda-learn.params"
    preparePrototype()

    logger.info("Starting the experiment preparation")
    prepareRun()
    conf.runDescription = "One Max Run Simple UMDA"

    logger.info("Starting the experiment")
    run()
}