package pl.put.gi.settings

/**
 * In the code constants. Not to be changed without deep
 * understanding.
 */
object InnerConstants {

  /**
   * The Long identifier of the DB id which is not applicable
   * for use. The id of the objects not stored in database yet.
   */
  def undefinedIdentifier: Long = -1
}