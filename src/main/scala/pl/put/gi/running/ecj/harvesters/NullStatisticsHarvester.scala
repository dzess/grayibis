package pl.put.gi.running.ecj.harvesters

import org.slf4j.LoggerFactory

import ec.Statistics
import pl.put.gi.model.Run
import pl.put.gi.running.ecj.IStatisticsHarvester

/**
 * Basic implementation of nullable class pattern.
 */
class NullStatisticsHarvester extends IStatisticsHarvester {

    private val logger = LoggerFactory.getLogger(classOf[NullStatisticsHarvester])

    override def onStart(statistic: Statistics, run: Run) = {
        logger.debug("On Start Statisitcs invoked with {}", statistic)
    }

    override def onFinish(statistic: Statistics, run: Run) = {
        logger.debug("On Finish Statisitcs invoked with {}", statistic)
    }

    override def onChange(statistic: Statistics, run: Run) = {
        logger.debug("On Change Statisitcs invoked with {}", statistic)
    }
}