package pl.put.gi.running.ecj.prototype

import pl.put.gi.running.ecj.IPrototypeReader
import pl.put.gi.model.Prototype

/**
 * The simples prototype reader. Works with every type of Prototype and reads only
 * the parameters field from the Prototype.
 */
class GenericPrototypeReader extends IPrototypeReader {
    override def generateParameters(prototype: Prototype): Map[String, String] = prototype.parameters
}