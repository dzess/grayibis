package pl.put.gi.dao.impl

import org.scalaquery.ql.basic.BasicDriver.Implicit._
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.ql.basic.BasicQueryTemplate
import org.scalaquery.ql.Parameters
import org.slf4j.LoggerFactory
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.model.Prototype
import pl.put.gi.dao.IPrototypeDAO
import com.google.inject.Inject

/**
 * Shows the methods for manipulating the Prototype classes
 * used for experiments.
 *
 * Also behaves as the very thin object relational mapping.
 */
class PrototypeDAO @Inject() (dbHelper: IDatabaseHelper) extends BaseDAO(dbHelper) with IPrototypeDAO {

    private val logger = LoggerFactory.getLogger(classOf[PrototypeDAO])

    // queries templates
    private val getPrototypesQuery = this.generateAllQuery(Prototype)
    private val getPrototypeByIdQuery = this.generateOneQuery(Prototype)
    private val maxIdQuery = this.generateMaxQuery(Prototype)

    // specific queries templates
    private val getPrototypeByKeyQuery = for {
        searchKey <- Parameters[String];
        p <- Prototype if p.key === searchKey
    } yield p

    override def getPrototypes(): List[Prototype] = {
        logger.debug("Quering DB for Prototype")

        var prototypeList: List[Prototype] = null

        this.db.withSession {
            val resultList = getPrototypesQuery.list
            logger.debug("Quering DB for Prototype - tuples {}", resultList)

            // re mapping list of tuples into the Prototype models
            prototypeList = resultList
                .map(x => Prototype.toModel(x))
                .toList

            logger.debug("Quering DB for Prototype - resulted {}", prototypeList)
        }

        return prototypeList
    }

    private def queryPrototype[T](param: T, query: BasicQueryTemplate[T, (Long, String, Option[String], String)]): Prototype = {

        var prototype: Prototype = null
        this.db.withSession {
            val result = query(param).list
            logger.debug("Quering DB for Prototype - tuples {}", result)

            if (result.size == 0) {
                prototype = null
            } else {
                prototype = Prototype.toModel(result.head)
            }
        }
        return prototype
    }

    override def getPrototypeByKey(key: String): Prototype = {

        logger.debug("Quering DB for Prototype by Key: {}", key)
        val prototype = this.queryPrototype(key, this.getPrototypeByKeyQuery)
        logger.debug("Quering DB for Prototype by Key {}", prototype)
        return prototype
    }

    override def getPrototype(id: Long): Prototype = {
        logger.debug("Quering DB for Prototype by ID: {}", id)
        val prototype = this.queryPrototype(id, this.getPrototypeByIdQuery)
        logger.debug("Quering DB for Prototype by ID {}", prototype)
        return prototype
    }

    /**
     * Saves the prototype into the DB.
     *
     * Unlike the JPA the save performs the update of the already existing
     * entity or creates the new one if such entity is not available.
     *
     * Upon any problems the exception is being thrown
     */
    override def save(prototype: Prototype) = {
        logger.debug("Saving the Prototype: {}", prototype)

        val inBaseObject = this.isObjectInDB(prototype, this.getPrototype)
        if (!inBaseObject) {
            logger.debug("Object not in DB - creating")

            this.db.withTransaction {

                // set the new id for the inserted object
                prototype.id = this.getNewId(this.maxIdQuery)
                val tuple = Prototype.fromModel(prototype)

                logger.debug("Creating the object in DB: {}", tuple)
                Prototype.insert(tuple)

            }
        } else {
            logger.debug("Object already in DB - updating")

            // check hasCodes if the object need to be updated
            val inBasePrototype = this.getPrototype(prototype.id)
            if (inBasePrototype.hashCode == prototype.hashCode) {
                logger.debug("HashCode the same - NOT updating")
            } else {
                logger.debug("HashCode differ - updating")

                this.db.withSession {
                    // prepare update query
                    val updateQuery = for { p <- Prototype if p.id === prototype.id } yield p
                    val tuple = Prototype.fromModel(prototype)
                    val numberOfUpdated = updateQuery.update(tuple)

                    logger.debug("Number of row updated - {}", numberOfUpdated)
                }
            }
        }
        logger.debug("Save finished")
    }

    override def createTable() = this.recreateTableBase(Prototype)
    override def dropTable() = this.dropTableBase(Prototype)
    override def isTableCreated(): Boolean = this.isTableCreatedBase(Prototype)
}