package pl.put.gi.use.regression.integer

import org.slf4j.LoggerFactory
import pl.put.gi.running.ecj.harvesters.KozaFitnessHarvester
import pl.put.gi.settings.InjectionManager
import pl.put.gi.use.ExampleRunnerBase

object IntRegressionRunnerClassic extends App with ExampleRunnerBase {

    private val logger = LoggerFactory.getLogger(this.getClass)

    logger.info("Starting the data preparation")

    // prepare the needed data
    experimentKey = "Regression_Psh_0"
    experimentDescription = "Integer Symbolic Regression for Psh"
    propertiesPath = "src/test/resources/configurations/regression-int/psh-mapped-classic.params"
    preparePrototype()

    logger.info("Starting the experiment preparation")
    prepareRun()
    conf.runDescription = "Symbolic Regression with Psh"
    harvesters = Seq(InjectionManager.injector.getInstance(classOf[KozaFitnessHarvester]))

    logger.info("Starting the experiment")
    run()

}