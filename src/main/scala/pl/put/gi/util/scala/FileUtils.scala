package pl.put.gi.util.scala

/**
 * Some useful code for file manipulation
 */
object FileUtils {
    def using[A <: { def close(): Unit }, B](param: A)(f: A => B): B =
        try { f(param) } finally { param.close() }
}

/**
 * Useful implicits for file manipulation
 */
object FileUtilsImplicits {

}