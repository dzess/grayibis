package pl.put.gi.model

import org.scalaquery.ql.basic.BasicTable
import org.scalaquery.ql.TypeMapper._
import org.scalaquery.ql._
import pl.put.gi.model.util.IMapable
import pl.put.gi.model.util.MapStringCoder
import pl.put.gi.settings.InnerConstants
import pl.put.gi.model.util.IdTable

/**
 * Models the single experiment to be conducted using the GI modeling.
 * This is the simples possible model of experiment prototype.
 *
 * Experiment consists of:
 * -the key identifier for experiment which should be unique.
 * -optional description which can be quite large
 * -parameters map which hold values String String - exactly same as ECJ
 *
 *
 */
class Prototype(
    var key: String = null,
    var description: Option[String] = None,
    private var _parameters: Map[String, String] = Map(),
    private var _id: Long = InnerConstants.undefinedIdentifier) {

    def parameters: Map[String, String] = _parameters
    def parameters_=(other: Map[String, String]) {
        if (other == null) {
            throw new IllegalArgumentException("null cannot be used as parameters")
        }
        _parameters = other
    }

    def id: Long = _id
    def id_=(other: Long) {
        if (other == InnerConstants.undefinedIdentifier) {
            throw new IllegalArgumentException("id can be undefined only for creation")
        }
        _id = other
    }

    override def hashCode(): Int = {
        val keyHash = if (key != null) key.hashCode else 13
        (
            keyHash ^
            description.getOrElse("").hashCode ^
            _parameters.hashCode ^
            _id.hashCode).toInt
    }
}

/**
 * ScalaQuery table mapping object for the Prototype
 */
object Prototype extends IdTable[(Long, String, Option[String], String)]("prototypes")
    with IMapable[(Long, String, Option[String], String), Prototype] {

    override def id = column[Long]("id", O PrimaryKey)

    def key = column[String]("key", O NotNull)
    def description = column[Option[String]]("description", O Nullable)
    def parameters = column[String]("parameters", O NotNull, O.DBType("varchar(16384)"))

    def * = id ~ key ~ description ~ parameters

    override def toModel(x: (Long, String, Option[String], String)): Prototype = {
        val p = new Prototype()
        p.id = x._1
        p.key = x._2
        p.description = x._3
        p.parameters = MapStringCoder.stringToMap(x._4)

        return p
    }

    override def fromModel(p: Prototype): (Long, String, Option[String], String) = {

        val params = MapStringCoder.mapToString(p.parameters)
        val fitnessId = InnerConstants.undefinedIdentifier
        (p.id, p.key, p.description, params)
    }
}