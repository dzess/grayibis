# This is file holds functions for drawing basic plots from data
# Requires: gi.setup
# Author: Piotr Jessa

gi.basic.rank <- function(dataset){
  ddply(dataset,c("id","key"),function(x){
    r <- 1:length(x$id)
    data.frame(r,x$id,x$key,x$fitness)
  })
}

gi.basic.plot.fitness <- function(dataset) {
  
  # use column names instead of expression because ggplot2 cannot understand
  # the idea of expressions in functions
  dr <- "r"
  df <- "x.fitness"
  dk <- "x.key"
  
  p <- ggplot(dataset,aes_string(x = dr,y =df))
  p <- p + geom_point(aes_string(colour= dk))
  P <- p + theme_bw()
  return (p)
}
