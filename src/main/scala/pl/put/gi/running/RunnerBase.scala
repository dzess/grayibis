package pl.put.gi.running

import org.slf4j.LoggerFactory
import pl.put.gi.dao.IDAOManager
import pl.put.gi.dao.IRunDAO
import pl.put.gi.dao.IPrototypeDAO
import pl.put.gi.dao.ILogDAO
import pl.put.gi.model.Prototype
import pl.put.gi.model.Run
import pl.put.gi.model.State
import java.sql.Timestamp
import java.util.Date
import pl.put.gi.dao.extended.IMessageLogDAO

/**
 * Base implementation of any Gray-Ibis runner. All the DAO related stuff
 * is set in this class.
 */
abstract class RunnerBase(protected val configuration: RunnerConfiguration,
    protected val daoManager: IDAOManager) extends IRunner {

    private val logger = LoggerFactory.getLogger(classOf[RunnerBase])

    // dao management
    protected var runDao: IRunDAO = null
    protected var prototypeDao: IPrototypeDAO = null
    protected var logDao: IMessageLogDAO = null

    // prototype to be used for this run
    protected var _expPrototype: Prototype = null
    protected var _expRun: Run = null

    // getters part of code of the protected variables
    def expPrototype: Prototype = _expPrototype
    def expRun: Run = _expRun

    // validation of the dao manager class
    if (daoManager == null) {
        throw new IllegalArgumentException("dao manager cannot be null")
    }

    if (configuration == null) {
        throw new IllegalArgumentException("configuration cannot be null")
    }

    // freeze the configuration, this disallowing any other
    // changes after loading it into scope
    configuration.freeze()

    override def initialize() = {

        // assert configuration is valid
        configuration.validate()

        // try connecting to the DB
        // all of those 3 daos are required at the basic level of computations
        daoManager.safeCreateTables()

        runDao = daoManager.getDAO(classOf[IRunDAO])
        prototypeDao = daoManager.getDAO(classOf[IPrototypeDAO])
        logDao = daoManager.getDAO(classOf[IMessageLogDAO])

        // validate that daos are on its places
        if (!daoManager.getDAOs.forall(x => x != null)) {
            throw new IllegalStateException("at least one dao has not been properly initialized")
        }
        logger.info("The database context has been established")

        val key = configuration.prototypeKey
        logger.info("Searching for Prototype Key '{}'", key)
        _expPrototype = prototypeDao.getPrototypeByKey(key)

        if (_expPrototype == null) {
            throw new IllegalStateException("Cannot find the provided prototype")
        }

        // create the run with the proper state
        _expRun = new Run()
        _expRun.prototype = _expPrototype
        _expRun.description = Some(configuration.runDescription)
        _expRun.state = State.initializing
        runDao.save(_expRun)

        // specific invocations of initialization
        try {
            logger.debug("Experiment initialing started")
            this.initializeExperiment()
            logger.debug("Experiment initialing finished")
        } catch {
            case e: Exception => {
                this.handleException(e)
                throw e
            }
        }

        _expRun.state = State.initialized
        runDao.save(_expRun)
    }

    override def run() = {

        _expRun.startTime = this.getCurrentTimestamp()
        _expRun.state = State.running
        runDao.save(_expRun)

        // invoke experiment here
        try {
            logger.info("Experiment started")
            this.runExperiment()
            logger.info("Experiment finished")
        } catch {
            case e: Exception => {
                this.handleException(e)
                throw e
            }
        }

        // after the experiment end 
        _expRun.finishTime = this.getCurrentTimestamp()
        _expRun.state = State.runned
        runDao.save(_expRun)
        logger.info("Run object has been updated '{}'", _expRun)
    }

    override def cleanup() = {

        _expRun.state = State.cleaning
        runDao.save(_expRun)

        try {
            logger.info("Cleaning up experiment started")
            this.cleanupExperiment()
            logger.info("Cleaning up experiment finished")
        } catch {
            case e: Exception => {
                this.handleException(e)
                throw e
            }
        }

        _expRun.state = State.done
        runDao.save(_expRun)
    }

    private def getCurrentTimestamp(): Timestamp = {
        new Timestamp(new Date().getTime())
    }

    private def handleException(exp: Exception) = {
        logger.warn("Exception during the operating on experiment - saving")
        _expRun.state = State.error
        _expRun.error = Some(exp.getStackTraceString)
        runDao.save(_expRun)
        logger.warn("Exception during the operating on experiment - saved")
    }

    /*
     * This method has to be override in subclasses for performing experiment.
     */
    protected def initializeExperiment()

    /**
     * This method has to be overridden in subclasses for performing experiment.
     */
    protected def runExperiment()

    /**
     * This method has to be override in subclass after performing experiment
     */
    protected def cleanupExperiment()
}