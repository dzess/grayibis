﻿select * from "prototypes-ecj" ep join "prototypes" p on ep."prototypeId" = p.id;

-- select all runs which are important
select
	r."id",r."modify-time", r."state", r."start-time",r."finish-time", p."key"
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
where
	p.key like 'OneMax_2_%'
order by r."modify-time" desc;

-- sum up the number of runs
select
	p.key, count(*) as total
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
where
	p.key like 'OneMax_2_%'
group by p.key;


-- get all the fintess logs
select
	p.key, fl.fitness, r.id
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
	join "logs" l on l."runId" = r."id"
	join "fitness-log" fl on fl."logId" = l."id"
where
	p.key like 'OneMax_2_%';

-- average of max fitness
create or replace view v_max_fitness as 
select
	x.key, y.field as "weight", avg(x."fitness-max"), stddev(x."fitness-max")
from 
	(
	select
		p.key, max(fl.fitness) as "fitness-max"
	from 
		"runs" r join "prototypes" p on r."experimentId" = p."id"
		join "logs" l on l."runId" = r."id"
		join "fitness-log" fl on fl."logId" = l."id"
	where
		p.key like 'OneMax_2_%'
	group by p.key,r.id
	) x
join 
	(
	select * from extract_field_numeric('breed\.updater\.weight')
	) y
on x.key = y.key
group by x.key, y.field
order by y.field asc;

-- average of avg fitness
select
	p.key, avg(fl.fitness) as "fitness-avg", stddev(fl.fitness) as "fitness-stdev"
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
	join "logs" l on l."runId" = r."id"
	join "fitness-log" fl on fl."logId" = l."id"
where
	p.key like 'OneMax_2_%'
group by p.key,r.id
;


