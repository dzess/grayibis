package pl.put.regression

import ec.util.Parameter
import ec.EvolutionState
import java.io.FileReader
import java.util.Locale
import java.util.Scanner
import pl.put.gi.util.scala.FileUtils.using
import java.io.BufferedReader
import org.slf4j.LoggerFactory

/**
 * Extended version of test case reader. Enables commented files, and what's more
 * has a validation incorporated for the number and arity of test cases.
 *
 * Some of those features are yet to be implemented
 */
class ExtendedFileTestCaseReader extends ITestCaseReader {

    private val logger = LoggerFactory.getLogger(classOf[ExtendedFileTestCaseReader])

    val comment = "#"
    val parameter = "test-cases-file"

    def load(state: EvolutionState, base: Parameter): Seq[(RegressionCase, Double)] = {
        val param = base.push(parameter)
        val file = state.parameters.getFile(param, null)

        var result = Seq[(RegressionCase, Double)]()

        using(new FileReader(file))(reader => {
            val buff = new BufferedReader(reader)
            var line = "";
            line = buff.readLine()
            while (line != null) {
                // remove trailing whitespace characters
                line = line.trim

                // only process not comments and not empty lines
                if (!line.startsWith(comment) && (!line.isEmpty())) {
                    val values = line.split("\\s+")
                    val ins = values.slice(0, values.length - 1).map(x => x.toDouble)
                    val r = new RegressionCase(ins.toSeq)
                    val out = values.last.toDouble
                    result = result ++ Seq((r, out))

                }
                // onto next loop
                line = buff.readLine()
            }
        })
        result
    }
}