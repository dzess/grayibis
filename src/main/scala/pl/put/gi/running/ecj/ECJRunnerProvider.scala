package pl.put.gi.running.ecj

import pl.put.gi.running.IRunnerProvider
import pl.put.gi.running.IRunner
import pl.put.gi.running.RunnerConfiguration
import org.slf4j.LoggerFactory
import pl.put.gi.dao.IDAOManager
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.dao.extended.ExtendedDAOManager
import pl.put.gi.dao.ecj.IECJPrototypeDAO
import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.running.ecj.harvesters.CompositeStatisticsHarvester
import com.google.inject.Inject
import pl.put.gi.settings.Constants
import pl.put.gi.running.universal.NullRunner

/**
 * Creates the ECJRunners using passed configurations. Depends heavily on
 * ExtendedDAOManager which is DAO manager suited specially for ECJ.
 */
class ECJRunnerProvider @Inject() (
    private val daoManager: IDAOManager) extends IRunnerProvider {

    private val logger = LoggerFactory.getLogger(classOf[ECJRunnerProvider])

    override def produce(runConfiguration: RunnerConfiguration): IRunner = {

        val ecjDAO = daoManager.getDAO(classOf[IECJPrototypeDAO])
        val key = runConfiguration.prototypeKey
        val prototype = ecjDAO.getPrototypeByKey(key)

        // this is important: if not ecj prototype is found then what should be done ?
        if (prototype == null) {
            if (Constants.THROW_UPON_MISSING_PROTOTYPE) {
                throw new IllegalStateException("not found one of the passed prototypes")
            } else {
                logger.warn("Prototype with key '{}' was not found", key)
                return new NullRunner()
            }
        }

        val statistics = readPrototype(prototype)

        // not we are not using the guice here because this would require a lot of
        // code changes which are not so important for now
        new ECJRunner(runConfiguration, daoManager, statistics)
    }

    override def validate() = {
        // no validation for this class is needed
    }

    override def freeze() = {
        super.freeze()
    }

    private def readPrototype(prototype: ECJPrototype): IStatisticsHarvester = {
        val harvesters = prototype.harvesters
        new CompositeStatisticsHarvester(harvesters)
    }
}