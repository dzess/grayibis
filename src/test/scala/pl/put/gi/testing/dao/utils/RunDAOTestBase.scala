package pl.put.gi.testing.dao.utils

import java.sql.Timestamp
import java.util.Date
import pl.put.gi.dao.impl.PrototypeDAO
import pl.put.gi.dao.impl.RunDAO
import pl.put.gi.db.PostgreSQLHelper
import pl.put.gi.db.TestDatabaseHelper
import pl.put.gi.model.Prototype
import pl.put.gi.model.Run

/**
 * Base class for managing the tests for RunDAO
 */
class RunDAOTestBase {

    protected var dao: RunDAO = null
    protected var prototypeDAO: PrototypeDAO = null
    protected val dbHelper = new TestDatabaseHelper()

    // run values
    protected val runDesc = Some("foo")
    protected val errorMsg = Some("this is some error message")
    protected val startDate = new Date()
    protected val finishDate = new Date()
    startDate.setTime(2055520)
    finishDate.setTime(2055520 + 1)

    // prototype values
    protected val prototypeDesc = Some("Some prototype")
    protected val prototypeKey = "key_1"

    protected def beforeEachMethod() = {
        // NOTE: this sequence of creation of relations
        // is quite important because of the FK relation between them
        prototypeDAO = new PrototypeDAO(dbHelper)
        prototypeDAO.createTable()

        dao = new RunDAO(dbHelper)
        dao.createTable()
    }

    protected def afterEachMethod() = {
        dao.dropTable()
        prototypeDAO.dropTable()
    }

    protected def getPrototype(): Prototype = {
        val prototype = new Prototype()
        prototype.description = prototypeDesc
        prototype.key = prototypeKey

        prototypeDAO.save(prototype)

        prototype
    }

    protected def getRun(): Run = {
        val run = new Run()
        run.description = runDesc
        run.error = errorMsg
        run.startTime = new Timestamp(startDate.getTime())
        run.finishTime = new Timestamp(finishDate.getTime())
        run.prototype = this.getPrototype()

        run
    }
}