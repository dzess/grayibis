package pl.put.gi.dao.extended.impl

import com.google.inject.Inject
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.dao.impl.BaseDAO
import org.slf4j.LoggerFactory
import pl.put.gi.model.extended.FitnessLog
import pl.put.gi.dao.extended.IFitnessLogDAO
import pl.put.gi.model.extended.fitness.KozaFitnessLog
import pl.put.gi.model.extended.fitness.HitsKozaFitnessLog
import pl.put.gi.model.extended.NamedFitnessLog
import pl.put.gi.model.extended.NamedFitnessLog
import pl.put.gi.settings.Constants

class NamedFitnessLogDAO @Inject() (dbHelper: IDatabaseHelper)
    extends LogDAOBase(dbHelper, FitnessLog, FitnessLog) with IFitnessLogDAO {

    private val logger = LoggerFactory.getLogger(classOf[NamedFitnessLogDAO])

    class KozaFitnessDAO extends LogDAOBase(dbHelper, KozaFitnessLog, KozaFitnessLog) {}
    class KozaHitFitnessDAO extends LogDAOBase(dbHelper, HitsKozaFitnessLog, HitsKozaFitnessLog) {}

    val kozaDAO = new KozaFitnessDAO()
    val kozaHitsDAO = new KozaHitFitnessDAO()

    override def addLog(log: FitnessLog) = {
        if (log.isInstanceOf[NamedFitnessLog]) {
            logger.debug("Is instance of Named Fitness Log")
            val namedLog = log.asInstanceOf[NamedFitnessLog]

            // depending on the name change the way it is persisted
            namedLog.category match {
                case Constants.KOZA_FITNESS_TABLE => {
                    logger.debug("Koza Adjusted fitness option")
                    kozaDAO.addLogBase(log)
                }

                case Constants.KOZA_HITS_FITNESS_TABLE => {
                    logger.debug("Koza Hits fitness option")
                    kozaHitsDAO.addLogBase(log)
                }
                case _ => throw new IllegalStateException("not recognized option found")
            }

        } else {
            logger.debug("Is NOT instance of Named Fitness Log")
            this.addLogBase(log)
        }
    }

    override def getAll(): List[FitnessLog] = {
        // this operation is not supported by now
        throw new IllegalStateException("not implemented yet")
    }

    override def dropTable() = {
        kozaDAO.dropTable()
        kozaHitsDAO.dropTable()
        super.dropTable()
    }

    override def createTable() = {
        super.createTable()
        kozaDAO.createTable()
        kozaHitsDAO.createTable()
    }

    override def isTableCreated() = isTableCreatedBase(KozaFitnessLog) && isTableCreatedBase(HitsKozaFitnessLog)
}