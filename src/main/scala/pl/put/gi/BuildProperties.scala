package pl.put.gi

import java.util.Properties
import org.slf4j.LoggerFactory

/**
 * Used for storing build dependent information like version number,
 * git SHA1 or other things.
 *
 * This class will be extended in future maybe.
 */
case class BuildProperties(name: String, version: String, build: String) {

    private def validateSting(input: String) = {
        if (input == null || input.isEmpty) {
            throw new IllegalArgumentException("none of build properties should be null")
        }
    }

    def validate() = {
        this.validateSting(name)
        this.validateSting(version)
        this.validateSting(build)
    }
}

object BuildProperties {

    private val logger = LoggerFactory.getLogger(this.getClass())

    val nameProperty = "project.name"
    val versionProperty = "project.version"
    val timestampProperty = "project.timestamp"

    val buildPropertiesFileName = "build.properties"

    def load(): BuildProperties = {

        val config = new Properties();
        val loader = this.getClass().getClassLoader();
        val istream = loader.getResourceAsStream(buildPropertiesFileName);

        try {
            config.load(istream);
        } catch {
            case e: Throwable => {
                logger.error("Exception happend during loading build.properties from classpath")
                throw new IllegalStateException("Exception happend during loading build.properties from classpath", e)
            }
        }

        val name = config.getProperty(nameProperty)
        val version = config.getProperty(versionProperty)
        val build = config.getProperty(timestampProperty)

        val p = BuildProperties(name, version, build)
        p.validate()

        return p
    }
}