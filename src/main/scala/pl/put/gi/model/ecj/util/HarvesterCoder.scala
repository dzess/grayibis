package pl.put.gi.model.ecj.util

import pl.put.gi.running.ecj.IStatisticsHarvester
import org.slf4j.LoggerFactory
import pl.put.gi.settings.InjectionManager

/**
 * Changes strings into the IStatisticsHarvester instances. Might in future be
 * extracted to some interface.
 */
class HarvesterCoder {

    private val logger = LoggerFactory.getLogger(classOf[HarvesterCoder])

    private val separator = ";"

    def encode(harvesters: Seq[IStatisticsHarvester]): String = {

        if (harvesters == null) {
            throw new IllegalArgumentException("harvester cannot be null")
        }

        if (harvesters.isEmpty) {
            return ""
        }

        val names = harvesters.map(h => h.getClass.getName).toSeq

        // check for duplicates
        val uniqueNames = names.distinct
        if (uniqueNames.size != names.size) {
            throw new IllegalArgumentException("collection contains duplicates")
        }

        val result = names.reduce((x, y) => x + separator + y)

        logger.debug("Harvester Encoded '{}'", result)

        return result
    }

    def decode(input: String): Seq[IStatisticsHarvester] = {

        if (input == null) {
            throw new IllegalArgumentException("input string cannot be null")
        }

        if (input.isEmpty) {
            return Seq()
        }

        val names = input.split(separator)
        val seq = names.map(name => toInstance(name)).toSeq

        logger.debug("Harvester Decoded '{}'", seq)
        return seq
    }

    private def toInstance(name: String): IStatisticsHarvester = {
        val cls = Class.forName(name)
        logger.debug("Found class '{}'", cls)

        // ask the dependency injection for the new instance of the harvester
        // which should wire up all the dependencies 
        InjectionManager.injector.getInstance(cls).asInstanceOf[IStatisticsHarvester]
    }
}