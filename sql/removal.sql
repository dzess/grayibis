﻿CREATE OR REPLACE FUNCTION remove_prototype(fieldName varchar) RETURNS VOID AS
$$ 
DECLARE
	
BEGIN
	DELETE FROM "koza-adjusted-fitness-log" x
	WHERE x.id in 
		(
			select kAF.id from "runs" r 
			join "prototypes" p on r."experimentId" = p."id" 
			join "logs" l on l."runId" = r."id"
			join "koza-adjusted-fitness-log" kAF on kAF."logId" = l."id"
			where p.key like fieldName
		);

	DELETE FROM "koza-hits-fitness-log" x
	WHERE x.id in 
		(
			select kHF.id from "runs" r 
			join "prototypes" p on r."experimentId" = p."id" 
			join "logs" l on l."runId" = r."id"
			join "koza-hits-fitness-log" kHF on kHF."logId" = l."id"
			where p.key like fieldName
		);

	DELETE FROM "message-log" x
	WHERE x.id in 
		(
			select ml.id from "runs" r 
			join "prototypes" p on r."experimentId" = p."id" 
			join "logs" l on l."runId" = r."id"
			join "message-log" ml on ml."logId" = l."id"
			where p.key like fieldName
		);

	
	DELETE FROM "fitness-log" x
	WHERE x.id in 
		(
			select ml.id from "runs" r 
			join "prototypes" p on r."experimentId" = p."id" 
			join "logs" l on l."runId" = r."id"
			join "fitness-log" ml on ml."logId" = l."id"
			where p.key like fieldName
		);

	DELETE FROM "logs" x
	WHERE x.id in 
		(
			select l.id from "runs" r 
			join "prototypes" p on r."experimentId" = p."id" 
			join "logs" l on l."runId" = r."id"
			where p.key like fieldName
		);

	DELETE FROM "runs" x
	WHERE x.id in 
		(
			select r.id from "runs" r 
			join "prototypes" p on r."experimentId" = p."id" 
			where p.key like fieldName
		);

	DELETE FROM "prototypes-ecj" x
	WHERE x.id in 
		(
			select ep.id from "prototypes-ecj" ep
			join "prototypes" p on ep."prototypeId" = p.id
			where p.key like fieldName
		);

	DELETE FROM "prototypes" x
	WHERE x.key like fieldName;
END;
$$
LANGUAGE plpgsql;