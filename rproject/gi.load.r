# This is file holds data loading functions from the database
# Requires: gi.setup
# Author: Piotr Jessa


gi.load.all <- function() {
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv,dbname = "mgr-benchmarks")
  
  all_query <- dbSendQuery(con,"select * from v_all")
  all_dataset <- fetch(all_query,n=-1)
  return (all_dataset)
}

gi.load.one_max <- function() {
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv,dbname = "mgr-benchmarks")
  
  all_query <- dbSendQuery(con,"select * from v_onemax")
  all_dataset <- fetch(all_query,n=-1)
  return (all_dataset)
}

gi.load.hiff <- function() {
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv,dbname = "mgr-benchmarks")
  
  all_query <- dbSendQuery(con,"select * from v_hiff")
  all_dataset <- fetch(all_query,n=-1)
  return (all_dataset)
}

