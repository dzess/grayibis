package pl.put.gi.model.extended

import java.sql.Timestamp
import pl.put.gi.model.Run
import pl.put.gi.model.Log
import pl.put.gi.model.util.IMapable
import pl.put.gi.model.util.IdTable

/**
 * Models single entry in DB which has also some additional information.
 *
 * Assumption that this additional information is known as the message and is
 * up to 1024 characters long.
 */
class MessageLog(
    private var _message: String,
    private var _timestamp: Timestamp = null,
    private var _run: Run = null) extends Log(_timestamp, _run) {

    def message: String = _message
    def message_=(other: String) {
        if (other == null || other.isEmpty) {
            throw new IllegalArgumentException("message cannot be null or empty")
        }
        _message = other
    }

}

object MessageLog extends IdTable[(Long, Long, String)]("message-log")
    with IMapable[(Long, Long, String), MessageLog] {

    override def id = column[Long]("id", O PrimaryKey)
    def logId = column[Long]("logId", O NotNull)
    def message = column[String]("message", O NotNull, O.DBType("varchar(16384)"))

    def * = id ~ logId ~ message

    // foreign key on the Log table
    def fitnessLogFK = foreignKey("messageLogFK", logId, Log)(_.id)

    override def fromModel(inputModel: MessageLog): (Long, Long, String) = {
        (inputModel.id, inputModel.id, inputModel.message)
    }

    override def toModel(inputTuple: (Long, Long, String)): MessageLog = {
        val logId = inputTuple._1
        val refLogId = inputTuple._2
        val value = inputTuple._3

        // construct the fitness log (but some value are meant to be missing)
        val log = new MessageLog(value)
        log.id = logId

        return log
    }
}