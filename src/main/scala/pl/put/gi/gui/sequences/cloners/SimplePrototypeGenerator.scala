package pl.put.gi.gui.sequences.cloners

import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.gui.sequences.IPrototypeGenerator

class SimplePrototypeGenerator extends IPrototypeGenerator {

    protected var counter = 0

    protected def generateKey(base: String): String = {
        val s = "%s_%d".format(base, counter)
        counter += 1
        return s
    }

    protected def clonePrototype(base: ECJPrototype): ECJPrototype = {
        // TODO: this code should be injected via some system of sequence generation
        // at least should be uniform
        val key = this.generateKey(base.key)
        val description = Some(base.description.getOrElse(""))
        val parameters: Map[String, String] = base.parameters.toMap
        val harvesters = base.harvesters.toSeq

        val newPrototype = new ECJPrototype()
        newPrototype.key = key
        newPrototype.description = description

        newPrototype.parameters = parameters
        newPrototype.harvesters = harvesters

        return newPrototype
    }

    override def generateSingle[T](paramKey: String, paramValue: T, basingPrototype: ECJPrototype): ECJPrototype = {
        val clonedPrototype = this.clonePrototype(basingPrototype)

        // put the proper parameter
        val params = clonedPrototype.parameters
        val mapFiltered = params.filter(x => x._1 != paramKey).toMap
        clonedPrototype.parameters = mapFiltered ++ Map(paramKey -> paramValue.toString)

        return clonedPrototype
    }

    override def reset() = {
        counter = 0
    }

}