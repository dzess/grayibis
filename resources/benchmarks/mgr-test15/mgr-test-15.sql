﻿-- Getting all of the statistics from the runs:
-- adjusted fitness
-- hits
-- number of generations


select
	x.key
	,q.description
	,avg(x."adjusted-fitness") as "avg-fitness"
	,stddev(x."adjusted-fitness") as "stddev-fitness"
	,avg(x."hits-fitness") as "avg-hits"
	,stddev(x."hits-fitness") as "stddev-hits"
	,avg(x."generations") as "avg-generations"
	,stddev(x."generations") as "stddev-generations"
	,count(*) as "runs"
from 
(
	select 
	r.id
	,p.key as "key"
	,max(kaf.fitness) as "adjusted-fitness"
	,max(khf.fitness) as "hits-fitness"
	,count(*) as "generations"
	from "runs" r 
	join "prototypes" p on r."experimentId" = p."id" 
	join "logs" l on l."runId" = r."id"
	join "koza-adjusted-fitness-log" kaf on kaf."logId" = l."id"
	join "koza-hits-fitness-log" khf on khf."logId" = l."id" + 1
	where p.key not like '%Dumping%'
	and p.key not like '%\_B%'
	and r.state = 'done'
	group by r.id, p.key
	order by r.id 
) x
join "prototypes" q
on x.key = q.key
group by x.key,q.description
order by x.key;

select * from "prototypes" p where p.key like '%';

select 
  r.id
  ,p.key
  ,r."modify-time"
  ,r.state
  ,r."start-time"
  ,r."finish-time"
from "runs" r join "prototypes" p on r."experimentId" = p.id
where p.key like '%\__'
order by r."modify-time";

