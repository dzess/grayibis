# This file does ploting of the data of MGR-TEST-4 using some views
# Requires: gi.setup
# Author: Piotr Jessa

gi.test4.load <- function() {
  all_dataset <- gi.load.view('v_max_fitness','4')
  return(all_dataset)
}


gi.test4.chart1 <- function(dataset) {
  
  dr <- "xfield"
  df <- "yfield"
  dk <- "avgfitness"

  p <- ggplot(dataset,aes_string(x = df, y = dr))
  p <- p + geom_tile(aes_string(fill = dk))
  p <- p + scale_fill_gradientn(name = "average fitness",colours=topo.colors(255))
  #p <- p + scale_fill_gradient(name = "average fitness",low = "white", high="black")
  p <- p + labs( x = "", y = "")
  p <- p + theme_grey()  
  p <- p + scale_x_discrete(expand = c(0, 0)) + scale_y_discrete(expand = c(0, 0)) 
  p <- p + opts(axis.ticks = theme_blank())
  
  ggsave("mgr4.pdf",useDingbats=FALSE)
  return (p)
}

gi.test4.chart2 <- function(dataset) {
  
}


