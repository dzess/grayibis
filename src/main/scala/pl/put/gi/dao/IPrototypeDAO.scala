package pl.put.gi.dao

import pl.put.gi.model.Prototype

abstract trait IPrototypeDAO extends IDAO {

    def getPrototypes(): List[Prototype]
    def getPrototypeByKey(key: String): Prototype
    def getPrototype(id: Long): Prototype
    def save(prototype: Prototype)

}