package pl.put.gi.model

import org.scalaquery.ql.basic.BasicTable
import org.scalaquery.ql.TypeMapper._
import org.scalaquery.ql._
import pl.put.gi.model.util.IMapable
import java.sql.Timestamp
import java.util.Date
import pl.put.gi.settings.InnerConstants
import pl.put.gi.model.State._
import pl.put.gi.model.util.IdTable

/**
 * Models single run of the experiment. Contains all the information
 * passed via interface of properties file.
 *
 */
class Run(
    var startTime: Timestamp = null,
    var finishTime: Timestamp = null,
    var description: Option[String] = None,
    private var _error: Option[String] = None,
    private var _state: State = State.undefined,
    private var _prototype: Prototype = null,
    private var _id: Long = InnerConstants.undefinedIdentifier) {

    // reference attributes
    def prototype: Prototype = _prototype
    def prototype_=(other: Prototype) {
        if (other == null) {
            throw new IllegalArgumentException("prototype cannot be null")
        }
        _prototype = other
    }

    def id: Long = _id
    def id_=(other: Long) {
        if (other == InnerConstants.undefinedIdentifier) {
            throw new IllegalArgumentException("id can be undefined only for creation")
        }
        _id = other
    }

    def state = _state
    def state_=(other: State) {
        if (other == null) {
            throw new IllegalArgumentException("state cannot be null")
        }
        _state = other
    }

    def error = _error
    def error_=(other: Option[String]) {
        if (other == null) {
            throw new IllegalArgumentException("error cannot be null")
        }
        _error = other
    }

    override def hashCode(): Int = {
        val errorHash = error.getOrElse("").hashCode()
        val finishTimeHash = if (finishTime != null) finishTime.getTime() else 21
        val startTimeHash = if (startTime != null) startTime.getTime else 17
        (weakHashCode() ^ finishTimeHash
            ^ startTimeHash ^ state.hashCode ^ errorHash).toInt
    }

    /**
     * Hash code which is computed from all element except:
     *
     * start time, finish time, state
     */
    def weakHashCode(): Int = {
        val prototypeHash = if (_prototype != null) _prototype.hashCode() else 37
        val descriptionHash = description.getOrElse("").hashCode()

        (_id.hashCode ^ prototypeHash ^ descriptionHash).toInt
    }
}

/**
 * ScalaQuery table mapping object for the Run
 */
object Run extends IdTable[(Long, Long, String, Option[String], Timestamp, Timestamp, Option[String], Timestamp)]("runs")
    with IMapable[(Long, Long, String, Option[String], Timestamp, Timestamp, Option[String], Timestamp), Run] {

    override def id = column[Long]("id", O PrimaryKey)
    def experimentId = column[Long]("experimentId", O NotNull)

    def state = column[String]("state", O NotNull)
    def error = column[Option[String]]("error-message", O Nullable, O.DBType("varchar(16384)"))

    def startTime = column[Timestamp]("start-time", O Nullable)
    def finishTime = column[Timestamp]("finish-time", O Nullable)
    def description = column[Option[String]]("description", O Nullable)

    // modification time is not visible to the object model
    def modifyTime = column[Timestamp]("modify-time", O Nullable)

    def * = id ~ experimentId ~ state ~ error ~ startTime ~ finishTime ~ description ~ modifyTime

    // foreign keys
    def runFK = foreignKey("runs_FK", experimentId, Prototype)(_.id)

    // other constraints
    // NOTE: scala query provides no means for creating the constraints
    // in the code maybe the SLICK project will allow to do it

    override def toModel(inputTuple: (Long, Long, String, Option[String], Timestamp, Timestamp, Option[String], Timestamp)): Run = {
        val r = new Run()
        r.id = inputTuple._1

        // this sample mapping implementation does not provide
        // means for mapping the referenced attributes

        r.startTime = inputTuple._5
        r.finishTime = inputTuple._6
        r.description = inputTuple._7

        r.state = State.withName(inputTuple._3)
        r.error = inputTuple._4

        return r
    }

    override def fromModel(inputModel: Run): (Long, Long, String, Option[String], Timestamp, Timestamp, Option[String], Timestamp) = {

        // this code could make it possible to enter the id of the
        // referenced id, however it would make the concept of mappable
        // classes inconsistent
        (inputModel.id, InnerConstants.undefinedIdentifier,
            inputModel.state.toString(), inputModel.error,
            inputModel.startTime, inputModel.finishTime, inputModel.description,
            // generate date during creation from model
            new Timestamp(new Date().getTime))
    }

}