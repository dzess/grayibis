# Friedman analysis of the data
# Requires: gi.setup or sth like this



gi.friedman.inner <- function(dataset){
  for(i in 1:5){
    for(j in i:5){
      
      if(i != j){
        q1 <- do.call(rbind, dataset[i])
        q2 <- do.call(rbind, dataset[j]) 
        
        colnames(q1) <- c("id","key","fitness","hits","generations")
        colnames(q2) <- c("id","key","fitness","hits","generations")
        
        # get distinct name ?
        q1Name <- q1[1,2]
        q2Name <- q2[1,2]
        
        #print(sprintf("Indexes on list (%d,%d)",i,j))
        #print(sprintf("Selected Q1: %s, Q2: %s",q1Name,q2Name))
        
        df1 <- data.frame(q1$id,q1$key,q1$fitness)  
        df2 <- data.frame(q2$id,q2$key,q2$fitness)
        df <- data.frame(q1$fitness,q2$fitness)
        
        fresult <- friedman.test(as.matrix(df))
        
        # write the results to the data frame:
        line <- c(q1Name,q2Name,fresult$p.value)
        print(line)
      }
    }
  }
  return ("")
}

gi.wilcoxon.inner <- function(dataset){
  for(i in 1:5){
    for(j in i:5){
      
      if(i != j){
        q1 <- do.call(rbind, dataset[i])
        q2 <- do.call(rbind, dataset[j]) 
        
        colnames(q1) <- c("id","key","fitness","hits","generations")
        colnames(q2) <- c("id","key","fitness","hits","generations")
        
        # get distinct name ?
        q1Name <- q1[1,2]
        q2Name <- q2[1,2]
        
        #print(sprintf("Indexes on list (%d,%d)",i,j))
        #print(sprintf("Selected Q1: %s, Q2: %s",q1Name,q2Name))
        
        df1 <- data.frame(q1$id,q1$key,q1$fitness)  
        df2 <- data.frame(q2$id,q2$key,q2$fitness)
        df <- data.frame(q1$fitness,q2$fitness)
        
        res <- wilcox.test(q1$fitness,q2$fitness,paired=TRUE)
        
        # write the results to the data frame:
        line <- c(q1Name,q2Name,res$p.value)
        print(line)
      }
    }
  }
  return ("")
}


gi.wilcoxon <- function(db_name){
  all <- gi.load.view('v_aggregate_maxes',db_name)
  datasets <- split(all,all$key)
  
  quartic <- list()
  quintic <- list()
  sextic <- list()
  
  # wrap up the data to the point where we have quartic, quintic and sextic
  # TODO: should be done in a loop
  quartic[1] = datasets[1]
  quintic[1] = datasets[2]
  sextic[1] = datasets[3]
  
  quartic[2] = datasets[4]
  quintic[2] = datasets[5]
  sextic[2] = datasets[6]
  
  quartic[3] = datasets[7]
  quintic[3] = datasets[8]
  sextic[3] = datasets[9]
  
  quartic[4] = datasets[10]
  quintic[4] = datasets[11]
  sextic[4] = datasets[12]
  
  quartic[5] = datasets[13]
  quintic[5] = datasets[14]
  sextic[5] = datasets[15]
  
  r <- list()
  r[1] <- gi.friedman.inner(quartic)
  r[2] <- gi.friedman.inner(quintic)
  r[3] <- gi.friedman.inner(sextic)
  
  return (r)
}