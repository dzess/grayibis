package pl.put.gi.settings.modules

import com.google.inject.AbstractModule
import pl.put.gi.running.ecj.harvesters.FitnessHarvester
import org.slf4j.LoggerFactory
import pl.put.gi.running.ecj.IStatisticsHarvester
import pl.put.gi.running.ecj.harvesters.NullStatisticsHarvester

/**
 * Registers the harvesters to be used.
 */
class HarvestersModule extends AbstractModule {

    private val logger = LoggerFactory.getLogger(classOf[HarvestersModule])

    override def configure() = {

        bind(classOf[IStatisticsHarvester])
            .to(classOf[NullStatisticsHarvester])

        logger.debug("Harvesters Module configured")
    }

}