package pl.put.gi.running.ecj.harvesters

import java.sql.Timestamp
import java.util.Date
import com.google.inject.Inject
import pl.put.gi.dao.extended.IMessageLogDAO
import pl.put.gi.running.ecj.IStatisticsHarvester
import org.slf4j.LoggerFactory
import pl.put.gi.model.Run
import ec.Statistics
import pl.put.gi.model.extended.MessageLog
import ec.simple.SimpleShortStatistics
import ec.simple.SimpleStatistics
import pl.put.poznan.eda.statistics.BayesianNetworkStatistics
import scala.annotation.tailrec
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import pl.put.poznan.eda.boa.util.graph.GraphNetwork
import scala.collection.JavaConversions._

/**
 * Harvester specially designed for BOA algorithm. Dumps data to the selected tables. According to
 * some options set in its configuration.
 *
 * Dumps the network topology
 * Dumps the network coefficients
 *
 * The Bayesian Network is read from the subpopulation state.
 */
class BOAHarvester @Inject() (private val messageLog: IMessageLogDAO) extends IStatisticsHarvester {

    private val logger = LoggerFactory.getLogger(classOf[BOAHarvester])

    override def onStart(statistic: Statistics, run: Run) = {
        logger.debug("On Start Statisitcs invoked with {}", statistic)
    }

    override def onFinish(statistic: Statistics, run: Run) = {
        logger.debug("Dumping the best solution")
    }

    private def printGraph(graph: GraphNetwork): String = {
        val sb = new StringBuilder()
        val header = sb.append("List of successors: \n")

        for (index <- 0 until graph.getSuccessorArray().size) {
            val graphTupleSrc = graph.searchForGraphTuple(index)
            val followersIndexes = graph.getSuccessorArray()(index)

            sb.append("Index '%s':".format(graphTupleSrc.variable.get_name()))
            for (followerIndex <- followersIndexes) {
                val follower = graph.searchForGraphTuple(followerIndex)
                sb.append(" '%s'".format(follower.variable.get_name()))
            }
            sb.append("\n")
        }

        sb.toString()
    }

    override def onChange(statistic: Statistics, run: Run) = {
        logger.debug("Dumping the current generation's solution")

        if (statistic.isInstanceOf[BayesianNetworkStatistics]) {
            // dump data
            val bStat = statistic.asInstanceOf[BayesianNetworkStatistics]

            logger.warn("Dumped network: ")
            val baos = new ByteArrayOutputStream();
            val ps = new PrintStream(baos);
            bStat.getNetwork.print(ps)

            val networkString = baos.toString()

            // dump network to the logger
            // actually the network to be dump is so big that reading it would be a waste of time..
            logger.debug(networkString)

            // try dumping the structure of the network
            val graph = bStat.getGraphNetwork()
            logger.warn(printGraph(graph))

            // dump network to string
            val timestamp = new Timestamp(new Date().getTime())
            val log = new MessageLog(networkString, timestamp, run)

            // save to database
            //messageLog.addLog(log)

        } else if (statistic.children.isEmpty) {
            // finish the call
        } else {
            for (child <- statistic.children) {
                this.onChange(child, run)
            }
        }
    }
}