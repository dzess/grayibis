package pl.put.gi.use.onemax

import org.slf4j.LoggerFactory
import pl.put.gi.use.ExampleRunnerBase
/**
 * Example that shows how to use Gray Ibis
 * codes.
 *
 * This class shows the utility to put prototype data
 * into database, how to configure the GrayIbis and how to
 * run sample tests.
 *
 * This class uses the EDA algorithms to run the data especially the
 * BOA algorithm.
 */
object OneMaxProblemRunnerBOA extends App with ExampleRunnerBase {

    private val logger = LoggerFactory.getLogger(this.getClass)

    // data preparation - read from files and insert into
    // database using the provided tools
    logger.info("Starting the data preparation")

    // prepare the needed data
    experimentKey = "OneMax_3"
    experimentDescription = "One Max Problem with EDA - BOA solution"
    propertiesPath = "src/test/resources/configurations/one-max/one-max-boa.params"
    preparePrototype()

    logger.info("Starting the experiment preparation")
    prepareRun()
    conf.runDescription = "One Max Run EDA - BOA"

    logger.info("Starting the experiment")
    run()
}