package pl.put.gi.running.ecj.harvesters

import java.sql.Timestamp
import java.util.Date
import org.slf4j.LoggerFactory
import com.google.inject.Inject
import ec.Statistics
import pl.put.gi.dao.extended.IFitnessLogDAO
import pl.put.gi.model.extended.NamedFitnessLog
import pl.put.gi.model.Run
import pl.put.gi.running.ecj.IStatisticsHarvester
import ec.simple.SimpleStatistics
import ec.gp.koza.KozaFitness
import pl.put.gi.settings.Constants
import ec.simple.SimpleShortStatistics

class KozaFitnessHarvester @Inject() (private val fitnessLog: IFitnessLogDAO) extends IStatisticsHarvester {
    private val logger = LoggerFactory.getLogger(classOf[KozaFitnessHarvester])

    private var adjustedFitness: Float = 0
    private var hitsFitness: Int = 0

    override def onStart(statistic: Statistics, run: Run) = {
        logger.debug("On Start logging nothing")
    }

    override def onFinish(statistic: Statistics, run: Run) = {
        logger.debug("On finish logging best value {}", adjustedFitness)
    }

    override def onChange(statistic: Statistics, run: Run) = {

        // try working with simple short statistics as well 
        // this feature is pretty dumb in ecj, that simple short statistics 
        // does not inherit from simple statistics
        val ind = if (statistic.isInstanceOf[SimpleShortStatistics]) {
            statistic.asInstanceOf[SimpleShortStatistics].best_of_run(0)
        } else {
            statistic.asInstanceOf[SimpleStatistics].best_of_run(0)
        }

        val fitness = ind.fitness.asInstanceOf[KozaFitness]

        // but there are two values that need to be logged adjusted fitness
        adjustedFitness = fitness.fitness()
        // and number of hits
        hitsFitness = fitness.hits

        logger.debug("On Change logging the fitness value {}", adjustedFitness)

        val timestamp = new Timestamp(new Date().getTime())
        val log1 = new NamedFitnessLog(Constants.KOZA_FITNESS_TABLE, adjustedFitness, timestamp, run)
        val log2 = new NamedFitnessLog(Constants.KOZA_HITS_FITNESS_TABLE, hitsFitness, timestamp, run)

        // saving to the DB
        fitnessLog.addLog(log1)
        fitnessLog.addLog(log2)
    }
}