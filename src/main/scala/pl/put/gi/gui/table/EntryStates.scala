package pl.put.gi.gui.table

/**
 * The state in which single entry of parameters can be
 */
object EntryStates extends Enumeration {
    type EntryState = Value

    val neutral = Value("neutral")
    val changedValue = Value("valueChanged")
    val changedKey = Value("keyChanged")
    val removed = Value("removed")
    val added = Value("added")
}