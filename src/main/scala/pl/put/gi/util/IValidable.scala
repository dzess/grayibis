package pl.put.gi.util

/**
 * Marks the class as the one able to validate
 * itself.
 */
abstract trait IValidable {

  /**
   * Check for full production level validity of the
   * instances.
   *
   * This can assume connecting to the database or parsing some files,
   * this validation should be called not so often.
   */
  def validate()
}