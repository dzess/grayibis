package pl.put.gi.running.universal

import pl.put.gi.running.IRunnerConfigurationProvider
import pl.put.gi.running.RunnerConfiguration
import org.slf4j.LoggerFactory
import scala.collection.mutable.Queue



/**
 * Basic implementation of configuration providers. Reads the map which holds the
 * values prototype-key : exact number of runs. Creates the run description basing
 * on the map values.
 */
class RunnerConfigurationProvider(val map: Map[String, Int]) extends IRunnerConfigurationProvider {

    private val logger = LoggerFactory.getLogger(classOf[RunnerConfigurationProvider])

    // map validation
    this.validate()

    override def generate(): List[RunnerConfiguration] = {
        val allConfigurations = Queue[RunnerConfiguration]()
        
        logger.debug("Generating the map of RunnerConfigurations")
        
        map.foreach( (entry) => {
            
            val key = entry._1
            val value = entry._2
            
            for(i <- 0 until value){
                val newConf = new RunnerConfiguration()
                newConf.prototypeKey = key
                newConf.runDescription = "%s:%d".format(key,i)
                
                allConfigurations +=(newConf)
            }
        })

        logger.debug("Generated list of RunnerConfigurations: {}",allConfigurations)
        
        // change to immutable version of this list
        allConfigurations.toList
    }

    override def validate() = {
        // map has to contain at least one element
        if (map == null || map.size < 1) {
            throw new IllegalArgumentException("tha passed map has to have at least one element")
        }
    }
}