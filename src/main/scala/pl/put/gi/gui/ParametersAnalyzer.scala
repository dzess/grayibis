package pl.put.gi.gui

import java.awt.event.MouseEvent
import java.awt.Dimension
import java.awt.Point
import java.awt.Toolkit
import java.io.File
import scala.collection.mutable.Set
import scala.swing.event.ButtonClicked
import scala.swing.event.MouseClicked
import scala.swing.event.SelectionChanged
import scala.swing.Action
import scala.swing.Alignment
import scala.swing.BorderPanel
import scala.swing.BoxPanel
import scala.swing.Button
import scala.swing.Dialog
import scala.swing.FileChooser
import scala.swing.FlowPanel
import scala.swing.MainFrame
import scala.swing.MenuItem
import scala.swing.Orientation
import scala.swing.ListView
import scala.swing.Label
import scala.swing.ScrollPane
import scala.swing.Separator
import scala.swing.SimpleSwingApplication
import scala.swing.TabbedPane
import scala.swing.TextField
import org.slf4j.LoggerFactory
import javax.swing.filechooser.FileFilter
import pl.put.gi.gui.table.EntryStates
import pl.put.gi.gui.table.ParametersTableModel
import pl.put.gi.gui.ui.PopupMenu
import pl.put.gi.BuildProperties
import scala.swing.event.ButtonClicked
import scala.swing.event.ButtonClicked
import pl.put.gi.running.ecj.ECJRunner
import pl.put.gi.running.ecj.ECJRunnerProvider

import pl.put.gi.settings.InjectionManager
import pl.put.gi.running.RunnerConfiguration
import pl.put.gi.dao.IDAOManager

/**
 * Sample GUI for editing and creating the various parameters of all the experiments.
 *
 * This code is extremely simple and is meant only as utility for editing Gray Ibis.
 */
object ParametersAnalyzer extends SimpleSwingApplication {

    private val logger = LoggerFactory.getLogger(this.getClass())
    logger.info("Parameters Analyzer started")

    val buildProperties = BuildProperties.load()
    val prototypeModel = new ParametersAnalyzerModel()

    var mainFrame: MainFrame = null

    val height = 768
    val width = 1024

    // defined actions (the Command pattern, well not exactly command patter)
    // but something fairy similar in scala terms
    val removePrototypeActon = Action("Remove Prototype") {
        Dialog.showMessage(null, "This action is not supported", "Warning")
    }

    val clonePrototypeAction = Action("Clone Prototype") {
        logger.debug("Clone Prototype Action is used")
        prototypeModel.clonePrototype()
        loadFreshPrototype()
    }

    val generatorFrame = new GeneratorDialog(mainFrame)

    val prototypeKeyText = new TextField {
        text = prototypeModel.getPrototypeName()
        editable = true
    }

    val prototypeDescriptionText = new TextField {
        text = ""
        editable = true
    }

    val runAction = Action("Run experiment") {
        logger.debug("Run experiment action is used")

        val prototype = prototypeModel.currentPrototype

        val configuration = new RunnerConfiguration()
        configuration.prototypeKey = prototype.key
        configuration.runDescription = "Run from gui: %s".format(prototype.description)

        val daoManager = InjectionManager.injector.getInstance(classOf[IDAOManager])
        val provider = new ECJRunnerProvider(daoManager)
        val runner = provider.produce(configuration)

        try {

            runner.initialize()
            runner.run()
            runner.cleanup()
        } catch {
            case e => {
                // TODO: make exception handled
                logger.warn("Exception occured during saving", e)
                Dialog.showMessage(null, e.getMessage(), "Exception")
            }
        }

        logger.debug("Run experiment action is finished")
    }

    val generateAction = Action("Generate Series") {
        logger.debug("Generate Series Actions is used")
        generatorFrame.centerOnScreen
        generatorFrame.modal = true
        // the selected prototype should be already loaded
        generatorFrame.prototype = prototypeModel.currentPrototype
        generatorFrame.open()
    }

    val refreshListAction = Action("Refresh") {
        logger.debug("Refresh Prototype View Action is used")
        prototypeView.listData = prototypeModel.getData
    }

    val refreshFullAction = Action("Full Refresh") {
        logger.debug("Full Refresh Prototype View Action is used")
        prototypeView.listData = prototypeModel.getData
        ecjTable.clear()
        giTable.clear()

        // clear all the text fields
        prototypeKeyText.text = ""
        prototypeDescriptionText.text = ""
    }

    val prototypeView = new ListView(prototypeModel.getData()) {

        val prototypeViewPopup = new PopupMenu("") {
            contents += new MenuItem("Clone") {
                action = clonePrototypeAction
            }
            contents += new MenuItem("Generate") {
                action = generateAction
            }
            contents += new Separator()
            contents += new MenuItem("Remove") {
                action = removePrototypeActon
            }
            contents += new Separator()
            contents += new MenuItem("Run") {
                action = runAction
            }
        }

        listenTo(selection, mouse.clicks)
        reactions += {

            case e: MouseClicked => {
                logger.debug("Prototype View Selection clicked at point {}", e.point)

                if (e.peer.getButton == MouseEvent.BUTTON3) {
                    prototypeViewPopup.show(this, e.point.x, e.point.y)
                }
            }

            case SelectionChanged(_) => {
                logger.debug("Selection changed")

                val items = selection.items
                if (items.size > 1) {
                    throw new IllegalStateException("not supported range of items")
                } else if (items.size == 1) {
                    val selectedItem = items.head
                    prototypeModel.currentPrototype = selectedItem.prototype
                    loadFreshPrototype()
                    logger.debug("Updated Table")
                } else {
                    logger.debug("No selecton at all")
                }
            }
        }
    }

    def addEntry(model: ParametersTableModel): Unit = {
        logger.debug("Add Entry Action used")

        val entry = ("Key", "Value")
        model.addEntry(entry)
        prototypeModel.currentEntry = entry

        logger.debug("Add Entry Action Finished")
    }

    def removeEntry(model: ParametersTableModel): Unit = {
        logger.debug("Remove Entry Action used")

        val entry = prototypeModel.currentEntry
        model.removeEntry(entry)
        prototypeModel.removeEntry()
        logger.debug("Remove Entry Action finished")
    }

    def leftClickFunc(selected: Set[(Int, Int)], model: ParametersTableModel): Unit = {
        if (selected.size == 1) {
            val entry = retriveEntry(selected, model)
            prototypeModel.currentEntry = entry
        }
    }

    private def retriveEntry(selected: Set[(Int, Int)], model: ParametersTableModel): (String, String) = {
        // the selected was one row so get those values
        logger.debug("Selected cells {}", selected)
        val cellPair = selected.head
        val row = cellPair._1
        logger.debug("Row number {}", row)

        val key = model.getValueAt(row, 0).toString
        val value = model.getValueAt(row, 1).toString

        // dump the information into model
        (key, value)
    }

    //create the specialize tables for this task
    val ecjTable = new ParameterTable(leftClickFunc, addEntry, removeEntry)

    def leftGIFunc(selected: Set[(Int, Int)], model: ParametersTableModel) = {
        if (selected.size == 1) {
            val option = retriveEntry(selected, model)
            prototypeModel.currentOption = option
        }
    }

    def addEntryGI(model: ParametersTableModel): Unit = {
        val option = ("New Key", "New Value")
        model.addEntry(option)
        prototypeModel.currentOption = option
    }

    def removeEntryGI(model: ParametersTableModel): Unit = {
        val option = prototypeModel.currentOption
        model.removeEntry(option)
        prototypeModel.removeOption()
    }

    val giTable = new ParameterTable(leftGIFunc, addEntryGI, removeEntryGI)

    private def refreshTable(): Unit = {

        ecjTable.refreshTable()
        giTable.refreshTable()

        tabbedPane.revalidate()
        tabbedPane.repaint()

        tablePanel.revalidate()
        tablePanel.repaint()
    }

    private def loadFreshPrototype(): Unit = {
        prototypeKeyText.text = prototypeModel.getPrototypeName()
        prototypeDescriptionText.text = prototypeModel.getPrototypeDesc()
        ecjTable.model.rowData = prototypeModel.getPrototypeParameters()
        giTable.model.rowData = prototypeModel.getPrototypeOptions()

        refreshTable()
    }

    private def clearMap(model: ParametersTableModel): Map[String, String] = {
        val clearedMap = model.rowData.filter {
            // pass except state removed
            case (_, _, EntryStates.removed) => false
            case _ => true
        }

        clearedMap.map(x => (x._1, x._2)).toMap
    }

    private def retrivePrototype(): Unit = {
        val map = clearMap(ecjTable.model)
        val options = clearMap(giTable.model)
        val key = prototypeKeyText.text
        val desc = prototypeDescriptionText.text

        prototypeModel.saveExisting(key, desc, map, options)
    }

    val paramsFileChooser = new FileChooser(new File(".")) {
        fileSelectionMode = FileChooser.SelectionMode.FilesAndDirectories
        fileFilter = new FileFilter() {
            override def accept(file: File): Boolean = {
                file.isDirectory() || file.getName().endsWith(".params")
            }
            override def getDescription(): String = "*.params"
        }
    }

    val exportButton = new Button("Export") {
        reactions += {
            case ButtonClicked(_) => {
                logger.debug("Export Clicked")

                val result = paramsFileChooser.showSaveDialog(this)
                if (result == FileChooser.Result.Approve) {
                    logger.debug("File selected")
                    var file = paramsFileChooser.selectedFile
                    logger.debug("'{}'", file)

                    // some nice logic to add params output
                    if (!file.getName.endsWith(".params")) {
                        val filePath = file.getAbsolutePath + ".params"
                        file = new File(filePath)
                    }

                    logger.debug("File used")
                    logger.debug("'{}'", file)

                    prototypeModel.exportToFile(file)
                    logger.debug("Exporting finished")
                } else {
                    logger.debug("Exporting cancelled")
                }

            }
        }
    }

    val loadButton = new Button("Import")
    loadButton.reactions += {
        case ButtonClicked(_) => {
            logger.debug("Load Clicked")
            val result = paramsFileChooser.showOpenDialog(null)
            if (result == FileChooser.Result.Approve) {
                logger.debug("File selected")
                val file = paramsFileChooser.selectedFile
                logger.debug("'{}'", file)

                prototypeModel.loadPrototypeFromFile(file)
                loadFreshPrototype()
            } else {
                logger.debug("File not selected")
            }
        }
    }

    val cloneButton = new Button("Clone")
    cloneButton.reactions += {
        case ButtonClicked(_) => {
            logger.debug("Clone Clicked")
            prototypeModel.clonePrototype()
            loadFreshPrototype()
        }
    }

    val saveButton = new Button("Save")
    saveButton.reactions += {
        case ButtonClicked(_) => {
            logger.debug("Save Clicked")

            try {
                retrivePrototype()
            } catch {
                case e => {
                    logger.warn("Exception occured during saving", e)
                    Dialog.showMessage(null, e.getMessage(), "Exception")
                }
            }

            // refresh the list on right side
            refreshListAction.apply()
            logger.debug("Saved finished")
        }
    }

    val refreshButton = new Button("Refresh") {
        action = refreshListAction
    }

    val exitButton = new Button("Exit")
    exitButton.reactions += {
        case ButtonClicked(_) => {
            logger.info("Closing")
            mainFrame.closeOperation()
        }
    }

    val flowPanel = new FlowPanel {
        contents += exportButton
        contents += loadButton
        contents += cloneButton
        contents += saveButton
        contents += refreshButton
        contents += exitButton
    }

    val jdbcTextField = new TextField() {
        text = prototypeModel.dbHelper.currentURL
        preferredSize = new Dimension(600, 22)
    }

    val databaseChoosePanel = new FlowPanel {
        contents += new Label("Active database: ")
        contents += jdbcTextField
        contents += new Button("Switch") {
            reactions += {
                case ButtonClicked(_) => {
                    logger.debug("Button of Switching context clicked")

                    val jdbc = jdbcTextField.text
                    logger.debug("Swithing to {}", jdbc)

                    // switch context of the model
                    prototypeModel.reconnectToDatabase(jdbc)

                    // refresh all the views
                    refreshFullAction.apply()
                }
            }
        }
    }

    val combinedSouthPane = new BorderPanel {
        add(flowPanel, BorderPanel.Position.North)
        add(databaseChoosePanel, BorderPanel.Position.South)
    }

    val listPanel = new BorderPanel {
        add(new ScrollPane(prototypeView), BorderPanel.Position.Center)
    }

    val tabbedPane = new TabbedPane {
        pages += new TabbedPane.Page("ECJ", ecjTable.view)
        pages += new TabbedPane.Page("GI", giTable.view)

        tabPlacement(Alignment.Bottom)
    }

    val tablePanel = new BorderPanel {
        add(new BoxPanel(Orientation.Vertical) {
            contents += prototypeKeyText
            contents += prototypeDescriptionText
        }, BorderPanel.Position.North)
        add(tabbedPane, BorderPanel.Position.Center)
    }

    def top =
        {
            val mf = new MainFrame {
                title = getTitle()
                size = new Dimension(height, width)
                preferredSize = new Dimension(width, height)
                location = getScreenCenter()

                contents = new BorderPanel {
                    add(combinedSouthPane, BorderPanel.Position.South)
                    add(listPanel, BorderPanel.Position.East)
                    add(tablePanel, BorderPanel.Position.Center)
                }
            }
            mainFrame = mf
            mf
        }

    override def startup(args: Array[String]) = {
        super.startup(args)

        val systemName = System.getProperty("os.name")
        logger.debug("Operating System {}", systemName)

        if (systemName == "Linux") {
            try {
            } catch {
                case e: Exception => {
                    logger.warn("Exception occured during loading GTK look and feel", e)
                    // fall back to other Look and Feel
                }
            }
        }

    }

    private def getTitle(): String = {
        "Parameters Analyzer on %s version %s".format(buildProperties.name, buildProperties.version)
    }

    private def getScreenCenter(): Point = {
        val screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        // middle of the screen (one screen) :/
        val xLoc = (screenSize.getWidth / 2).asInstanceOf[Int]
        val yLoc = (screenSize.getHeight / 2).asInstanceOf[Int]

        // move to match center of window center of screen
        val xLocMoved = xLoc - (width / 2)
        val yLocMoved = yLoc - (height / 2)

        new Point(xLocMoved, yLocMoved)
    }

}