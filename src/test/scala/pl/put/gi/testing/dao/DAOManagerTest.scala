package pl.put.gi.testing.dao

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.BeforeAndAfterAll
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite

import com.google.inject.Guice

import pl.put.gi.dao.impl.DAOManager
import pl.put.gi.dao.impl.LogDAO
import pl.put.gi.dao.impl.PrototypeDAO
import pl.put.gi.dao.impl.RunDAO
import pl.put.gi.dao.ILogDAO
import pl.put.gi.dao.IPrototypeDAO
import pl.put.gi.dao.IRunDAO
import pl.put.gi.db.TestDatabaseHelper
import pl.put.gi.settings.modules.DatabaseAccessObjectModule

@RunWith(classOf[JUnitRunner])
class DAOManagerTest extends FunSuite with BeforeAndAfterEach with BeforeAndAfterAll {

    private val dbHelper = new TestDatabaseHelper()
    private var daoManager: DAOManager = null

    // lock object is necessary because of the ddl operations on the database
    private val lockObject = new Object()

    override def beforeEach() = {

        // this test is integration one of the scope so use production configuration
        val injector = Guice.createInjector(new DatabaseAccessObjectModule(dbHelper))
        daoManager = injector.getInstance(classOf[DAOManager])
    }

    override def afterEach() = {
        daoManager.dropTables()
    }

    override def afterAll() = {
        daoManager.dropTables()
    }

    test("basic manager configuration has 3 daos") {
        lockObject.synchronized {
            val rDao = daoManager.getDAO(classOf[RunDAO])
            val pDao = daoManager.getDAO(classOf[PrototypeDAO])
            val lDao = daoManager.getDAO(classOf[LogDAO])

            assert(rDao != null)
            assert(pDao != null)
            assert(lDao != null)
        }
    }

    test("the daos can be retrived by quering for idaos") {
        lockObject.synchronized {
            val rDao = daoManager.getDAO(classOf[IRunDAO])
            val pDao = daoManager.getDAO(classOf[IPrototypeDAO])
            val lDao = daoManager.getDAO(classOf[ILogDAO])

            assert(rDao != null)
            assert(pDao != null)
            assert(lDao != null)
        }
    }

    test("safe creation of tables works when tables are not existing") {
        lockObject.synchronized {
            try {
                daoManager.safeCreateTables()
                val daos = daoManager.getDAOs()
                assert(daos.forall(x => x.isTableCreated))
            } finally {
                daoManager.dropTables()
            }
        }
    }

    test("safe creation of tables does not work tables are existing") {
        lockObject.synchronized {
            try {
                val daos = daoManager.getDAOs()
                daoManager.createTables()
                assert(daos.forall(x => x.isTableCreated))
                daoManager.safeCreateTables()
                assert(daos.forall(x => x.isTableCreated))
            } finally {
                daoManager.dropTables()
            }
        }
    }

    test("safe creation of tables throws exception if at least one table is existing") {
        lockObject.synchronized {
            try {
                val daos = daoManager.getDAOs()
                daos(0).createTable()
                daos(1).createTable()

                // up this point should everything work
                intercept[IllegalStateException] {
                    daoManager.safeCreateTables()
                }
            } finally {
                daoManager.dropTables()
            }
        }
    }
}