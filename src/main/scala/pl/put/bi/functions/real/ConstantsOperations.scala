package pl.put.bi.functions.real

import pl.put.bi.functions.NullaryOperation
import ec.EvolutionState
import ec.gp.GPData
import ec.gp.ADFStack
import ec.Problem
import ec.gp.GPIndividual
import pl.put.bi.data.DoubleData
import pl.put.bi.problem.DoubleRegressionProblem

abstract class Constant extends NullaryOperation {

    override def eval(state: EvolutionState, thread: Int, input: GPData, stack: ADFStack, individual: GPIndividual, problem: Problem): Unit = {
        val data = input.asInstanceOf[DoubleData]
        data.y = getValue
    }
    protected def getValue: Double
}

class Zero extends Constant {
    override def toString(): String = "0"
    override def getValue = 0.0
}

class One extends Constant {
    override def toString(): String = "1"
    override def getValue = 1.0
}

class Two extends Constant {
    override def toString(): String = "2"
    override def getValue = 2.0
}

class Three extends Constant {
    override def toString(): String = "3"
    override def getValue = 3.0
}

class Four extends Constant {
    override def toString(): String = "4"
    override def getValue = 4.0
}

class Five extends Constant {
    override def toString(): String = "5"
    override def getValue = 5.0
}