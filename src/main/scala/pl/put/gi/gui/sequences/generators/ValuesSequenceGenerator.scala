package pl.put.gi.gui.sequences.generators

import org.slf4j.LoggerFactory
import pl.put.gi.gui.sequences.InputParameter
import pl.put.gi.gui.sequences.SequenceGeneratorBase
import pl.put.gi.gui.sequences.StringInputParameter
import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.gui.sequences.IPrototypeGenerator

/**
 * Generates the exactly the same prototypes as base except parameter which are
 */
class ValuesSequenceGenerator(private val prototypeGenerator: IPrototypeGenerator) extends SequenceGeneratorBase {

    private val logger = LoggerFactory.getLogger(classOf[ValuesSequenceGenerator])

    val parameter = StringInputParameter("parameter")
    val valuesParameter = StringInputParameter("values")

    private val sequences = Seq(parameter, valuesParameter)

    override def inputs: Seq[InputParameter] = sequences

    override def generate(basingPrototype: ECJPrototype, inputs: Seq[InputParameter]): Seq[ECJPrototype] = {

        val parameterName = findParameter[String](parameter, inputs)
        val valuesString = findParameter[String](valuesParameter, inputs)

        // parse 
        val values = valuesString.split(";")
        prototypeGenerator.reset()
        for (value <- values) yield {
            prototypeGenerator.generateSingle(parameterName, value, basingPrototype)
        }
    }

}