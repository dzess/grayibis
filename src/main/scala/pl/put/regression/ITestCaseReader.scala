package pl.put.regression

import ec.util.Parameter
import ec.EvolutionState

/**
 * Interface like for reading the test cases for symbolic regression
 */
abstract trait ITestCaseReader {

    def load(state: EvolutionState, base: Parameter): Seq[(RegressionCase, Double)]
}