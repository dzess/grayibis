# This file does ploting of the data of MGR-TEST-6 using some view
# Requires: gi.setup
# Author: Piotr Jessa

gi.test6.load <- function() {
  all_dataset <- gi.load.view('v_max_fitness','6')
  return(all_dataset)
}

gi.test6.chart <- function(dataset){
  df <- data.frame(x = dataset$xfield, y=dataset$yfield, z = dataset$avg)
  p <- wireframe(z ~x*y,data=df,
            xlab = "weight", 
            ylab = "exchange-ratio", 
            zlab = "avg max fitness",
            drape = TRUE, colorkey = TRUE,
            screen = list(z = 30,x = -60),
            distance = .5)
  return (p)
}