package pl.put.gi.testing.gui.generators

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.scalatest.mock.MockitoSugar
import org.scalatest.BeforeAndAfterEach
import pl.put.gi.gui.sequences.generators.CartesianSequenceGenerator
import pl.put.gi.gui.sequences.ISequenceGenerator
import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.gui.sequences.CompositeGeneratorInputParameter
import pl.put.gi.gui.sequences.InputParameter
import pl.put.gi.exceptions.IllegalSequenceParametersException

import org.mockito.Mockito._
import org.mockito.Matchers._

@RunWith(classOf[JUnitRunner])
class CartesianSequenceGeneratorTest extends FunSuite with MockitoSugar with BeforeAndAfterEach {

    private var sequence: CartesianSequenceGenerator = null

    override def beforeEach() = {
        val others = Seq[ISequenceGenerator]()
        sequence = new CartesianSequenceGenerator(others)
    }

    test("series with zero generators throws exception") {

        val basingPrototype = mock[ECJPrototype]

        val values: Seq[(ISequenceGenerator, Seq[InputParameter])] = Seq()
        val compositeParam = CompositeGeneratorInputParameter(sequence.generatorsParameter.name, sequence.generatorsParameter.predefined, values)
        val inputs = Seq(compositeParam)

        intercept[IllegalSequenceParametersException] {
            sequence.generate(basingPrototype, inputs)
        }
    }

    test("series with one generator uses this one generator") {

        val basingPrototype = mock[ECJPrototype]

        val gen1 = mock[ISequenceGenerator]
        val params1: Seq[InputParameter] = Seq()
        val tuple1 = (gen1, params1)

        val gen1result = Seq(new ECJPrototype(), new ECJPrototype())

        when(gen1.generate(any[ECJPrototype], any[Seq[InputParameter]])).thenReturn(gen1result)

        val values: Seq[(ISequenceGenerator, Seq[InputParameter])] = Seq(tuple1)
        val compositeParam = CompositeGeneratorInputParameter(sequence.generatorsParameter.name, sequence.generatorsParameter.predefined, values)
        val inputs = Seq(compositeParam)

        val result = sequence.generate(basingPrototype, inputs)

        expect(2)(result.size)

    }

    test("series with two generators creates the cartesian product") {
        val basingPrototype = mock[ECJPrototype]

        val gen1 = mock[ISequenceGenerator]
        val params1: Seq[InputParameter] = Seq()
        val tuple1 = (gen1, params1)

        val gen2 = mock[ISequenceGenerator]
        val params2: Seq[InputParameter] = Seq()
        val tuple2 = (gen2, params2)

        val gen1result = Seq(new ECJPrototype(), new ECJPrototype())
        val gen2restuls = Seq(new ECJPrototype(), new ECJPrototype(), new ECJPrototype())

        when(gen1.generate(any[ECJPrototype], any[Seq[InputParameter]])).thenReturn(gen1result)
        when(gen2.generate(any[ECJPrototype], any[Seq[InputParameter]])).thenReturn(gen2restuls)

        val values: Seq[(ISequenceGenerator, Seq[InputParameter])] = Seq(tuple1, tuple2)
        val compositeParam = CompositeGeneratorInputParameter(sequence.generatorsParameter.name, sequence.generatorsParameter.predefined, values)
        val inputs = Seq(compositeParam)

        val result = sequence.generate(basingPrototype, inputs)

        expect(6)(result.size)
    }
}