package pl.put.gi.use

import pl.put.gi.dao.extended.IFitnessLogDAO
import pl.put.gi.dao.extended.ExtendedDAOManager
import pl.put.gi.dao.IDAOManager
import pl.put.gi.db.PostgreSQLHelper
import pl.put.gi.loaders.HierarchicalFileParametersLoader
import pl.put.gi.running.ecj.ECJRunner
import pl.put.gi.running.RunnerConfiguration
import pl.put.gi.running.ecj.harvesters.FitnessHarvester
import pl.put.gi.dao.ecj.impl.ECJPrototypeDAO
import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.settings.modules.DatabaseAccessObjectModule
import pl.put.gi.running.ecj.harvesters.NullStatisticsHarvester
import pl.put.gi.settings.modules.HarvestersModule
import pl.put.gi.settings.InjectionManager
import pl.put.gi.conf.DatabaseLocationReader
import pl.put.gi.running.ecj.IStatisticsHarvester
import pl.put.gi.running.ecj.harvesters.CompositeStatisticsHarvester

/**
 * Base class for all the examples from the <code>package</code>.
 * Responsible for creating the new Prototype in the database and running the experiment.
 */
trait ExampleRunnerBase {

    protected var experimentKey: String = null
    protected var experimentDescription: String = null
    protected var connectionString: String = null
    protected var propertiesPath: String = null

    protected var daoManager: IDAOManager = null
    protected var conf: RunnerConfiguration = null

    protected var harvesters: Seq[IStatisticsHarvester] = null

    def preparePrototype() = {

        // here make loading of connection string from some sort of properties file
        val dbHelperReader = new DatabaseLocationReader()
        val dbHelper = dbHelperReader.load()

        // turn up the DAO module from Guice (for all the database wiring)
        InjectionManager.createInjector(
            new HarvestersModule(),
            new DatabaseAccessObjectModule(dbHelper))

        // read the properties file from some location
        val loader = new HierarchicalFileParametersLoader(propertiesPath)
        val properties = loader.loadProperties()
        daoManager = InjectionManager.injector.getInstance(classOf[ExtendedDAOManager])
        daoManager.safeCreateTables()
        val dao = daoManager.getDAO(classOf[ECJPrototypeDAO])

        // update the prototype to the DB (or create new one)
        var prototype = dao.getPrototypeByKey(experimentKey)
        if (prototype == null) {
            prototype = new ECJPrototype()
            prototype.key = experimentKey
            prototype.description = Some(experimentDescription)

            // ECJ gray ibis running parameters (only if there is no element)
            prototype.harvesters = Seq(new NullStatisticsHarvester())
        }
        // ECJ experiment parameters
        prototype.parameters = properties

        dao.save(prototype)
    }

    def prepareRun() = {
        conf = new RunnerConfiguration()
        conf.prototypeKey = experimentKey
    }

    def run() = {
        //  but if nothing is specified then fitness is being measured 
        val fitnessDAO = daoManager.getDAO(classOf[IFitnessLogDAO])

        if (harvesters == null) {
            val fitnessStatistics = new FitnessHarvester(fitnessDAO)
            harvesters = Seq(fitnessStatistics)
        }

        val statistics = new CompositeStatisticsHarvester(harvesters)

        // create runner and pass the configuration
        val runner = new ECJRunner(conf, daoManager, statistics)
        runner.initialize()
        runner.run()
        runner.cleanup()
    }
}