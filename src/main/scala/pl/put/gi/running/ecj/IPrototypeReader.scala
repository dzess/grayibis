package pl.put.gi.running.ecj

import pl.put.gi.model.Prototype

/**
 * Reads prototype data and merges it into one single map of common settings for experiment.
 * Currently this is specific for the ECJ.
 */
abstract trait IPrototypeReader {
    def generateParameters(prototype: Prototype): Map[String, String]
}