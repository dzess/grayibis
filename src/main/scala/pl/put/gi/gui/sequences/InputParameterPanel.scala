package pl.put.gi.gui.sequences

import org.slf4j.LoggerFactory
import scala.swing._
import scala.swing.event._
import javax.swing.border.SoftBevelBorder
import javax.swing.BorderFactory
import javax.swing.border.LineBorder
import javax.swing.border.TitledBorder
import scala.collection.mutable

class InputParameterPanel extends Panel {

    private val logger = LoggerFactory.getLogger(classOf[InputParameterPanel])

    type ValueMap = mutable.Map[InputParameter, Object]

    private var _parametersMap: ValueMap = mutable.Map()

    private def generateCompositeControl(index: Int, defined: Seq[ISequenceGenerator], panelsMap: mutable.Map[Int, (ISequenceGenerator, InputParameterPanel)]): Component = {

        val withName = "Composite: %d".format(index)
        val innerInputPanel = new InputParameterPanel()

        // generate combo box with some values
        val combo = new ComboBox(defined) {
            listenTo(selection)
            reactions += {
                case SelectionChanged(_) => {
                    logger.debug("Selection Changed on Composite Panel")

                    val selectedGenerator = selection.item
                    // putting at the same index will make this code use references
                    panelsMap += (index -> (selectedGenerator, innerInputPanel))
                    innerInputPanel.updateControls(selectedGenerator.inputs)
                    innerInputPanel.revalidate()
                    innerInputPanel.repaint()
                }
            }
        }

        panelsMap += (index -> (defined(0), innerInputPanel))

        // include self in a composition with some 
        new BorderPanel() {
            add(combo, BorderPanel.Position.North)
            add(innerInputPanel, BorderPanel.Position.Center)

            border = LineBorder.createGrayLineBorder()
        }
    }

    private def generateControl(parameter: InputParameter, parametersMap: ValueMap): Component = {

        parameter match {
            case param @ StringInputParameter(name, _) => {
                logger.debug("Generating TextField for '{}'", name)

                val labelSize = 200

                val textField = new TextField() {
                    preferredSize = new Dimension(labelSize, 22)
                }
                val caption = "%s:".format(name)

                // save the text field into some structure for rereading
                parametersMap += (param -> textField)

                new FlowPanel() {
                    contents += new Label(caption) {
                        preferredSize = new Dimension(labelSize, 22)
                    }
                    contents += textField
                }
            }
            case param @ CompositeGeneratorInputParameter(name, defined, _) => {
                logger.debug("Generating composit generator for '{}'", name)

                val listOfPanels = new GridPanel(1, 1)

                def generateComposites(count: Int) = {
                    listOfPanels.rows = count
                    listOfPanels.contents.clear()

                    var controlMap: mutable.Map[Int, (ISequenceGenerator, InputParameterPanel)] = mutable.Map()

                    for (i <- 0.until(count)) {

                        val innerParametersMap: ValueMap = mutable.Map()

                        // add this control into grid panel
                        val ctl = this.generateCompositeControl(i, defined, controlMap)
                        listOfPanels.contents += ctl

                    }
                    listOfPanels.revalidate()
                    listOfPanels.repaint()

                    // dump into proper map (the one passed onto method)
                    parametersMap += (param -> controlMap)
                }

                // here goes number of possible folds
                val range = 1.until(5)
                val countCombo = new ComboBox(range) {
                    listenTo(selection)
                    reactions += {
                        case SelectionChanged(_) => {
                            val count = selection.item
                            logger.debug("Number of inner composits '{}'", count)
                            generateComposites(count)
                        }
                    }
                }

                // first firing
                val count = countCombo.selection.item
                generateComposites(count)

                val finalPanel = new BorderPanel() {
                    add(countCombo, BorderPanel.Position.North)
                    add(listOfPanels, BorderPanel.Position.Center)
                }

                // return this from case
                finalPanel
            }
            case _ => throw new IllegalArgumentException("unsupported type of input parameter")
        }
    }

    private def readControl(pair: (InputParameter, Object)): InputParameter = {

        pair match {
            case (StringInputParameter(name, _), component) => {
                val textField = component.asInstanceOf[TextField]
                val value = textField.text
                logger.debug("Read TextField for {},{}", name, value)
                StringInputParameter(name, value)
            }
            case (CompositeGeneratorInputParameter(name, predefined, _), obj) => {
                logger.debug("Reading Composite with name {}", name)

                val innerMap = obj.asInstanceOf[mutable.Map[Int, (ISequenceGenerator, InputParameterPanel)]]

                var values: Seq[(ISequenceGenerator, Seq[InputParameter])] = Seq()
                for (entry <- innerMap) {
                    val (index, tuple) = entry
                    val (gen, panel) = tuple

                    val params = panel.readControls()
                    values = values ++ Seq((gen, params))
                }

                // here goes value 
                CompositeGeneratorInputParameter(name, predefined, values)
            }
            case _ => throw new IllegalArgumentException("unsupported type of input parameter")
        }
    }

    def updateControls(parameters: Seq[InputParameter]): Unit = {
        _parametersMap = mutable.Map()
        this.updateControls(parameters, _parametersMap)
    }

    private def updateControls(parameters: Seq[InputParameter], parametersMap: ValueMap): Unit = {

        val components = parameters.map(x => generateControl(x, parametersMap))

        val container = new BoxPanel(Orientation.Vertical) {
            for (component <- components) {
                contents += component
            }
        }

        val paramComponents = contents.asInstanceOf[Content]
        paramComponents.clear()
        paramComponents += container
    }

    def readControls(): Seq[InputParameter] = {
        _parametersMap.map(x => readControl(x)).toSeq
    }
}