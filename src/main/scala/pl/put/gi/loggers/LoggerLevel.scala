package pl.put.gi.loggers

/**
 * Describes the used log level in the Gray Ibis. Used only for interaction with
 * LoggerWriter objects.
 */
object LoggerLevel extends Enumeration {
    type LogLevel = Value
    val debug = Value("debug-level")
    val info = Value("info-level")
}