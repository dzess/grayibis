package pl.put.bi.functions

import ec.gp.GPNode
import ec.EvolutionState
import ec.gp.GPIndividual
import ec.util.Parameter

abstract class NullaryOperation extends GPNode {
    override def checkConstraints(state: EvolutionState, tree: Int, ind: GPIndividual, base: Parameter) = {
        super.checkConstraints(state, tree, ind, base)

        if (children.length != 0) {
            val msg = "Incorrect number of children for nullary node %s at %s".format(toStringForError, base)
            state.output.error(msg)
        }
    }
}