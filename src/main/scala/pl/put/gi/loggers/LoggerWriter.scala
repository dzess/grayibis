package pl.put.gi.loggers

import org.slf4j.LoggerFactory

import java.io.Writer
import pl.put.gi.loggers.LoggerLevel._

/**
 * Class used for the solely purpose of capturing the [[PrintStream]] classes
 * and dumping them into logger
 */
class LoggerWriter(private val _level: LogLevel) extends Writer {

    private val logger = LoggerFactory.getLogger(classOf[LoggerWriter])

    val writingFuncDebug = (buffer: Array[Char]) => logger.debug(new String(buffer))

    val wirtingFuncInfo = (buffer: Array[Char]) => logger.info(new String(buffer))

    val currentWritingFunc = _level match {
        case LoggerLevel.debug => writingFuncDebug
        case LoggerLevel.info => wirtingFuncInfo
    }

    override def flush() = {
        // actually not used
    }

    override def close() = {
        // actually not used
    }

    override def write(buff: Array[Char], offset: Int, length: Int) = {
        currentWritingFunc(buff)
    }
}