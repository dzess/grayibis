package pl.put.gi.db

/**
 * Helpers class for database management for the SQLite connections.
 */
class SQLiteHelper(val connectionString: String) extends DatabaseHelperBase {

    // the other way the SQL lie can work
    //jdbc:sqlite:/tmp/gray-ibis/mydatabase.db"
    if (connectionString == null || connectionString.isEmpty) {
        throw new IllegalArgumentException("connection string cannot be null or empty")
    }

    this.driver = "org.sqlite.JDBC"
    this.currentURL = "jdbc:sqlite::memory:"

}