package pl.put.gi.running

/**
 * Runs Gray - Ibis enables experiment.
 *
 * The main stage of the running experiments has been divided into to parts - one used for
 * the initialization, the second one used for the run and after the run the cleanup phase.
 */
abstract trait IRunner {

    /**
     * Sets up the whole overall experiment
     */
    def initialize()

    /**
     * Runs the overall experiment
     */
    def run()

    /**
     * Ends the experiment
     */
    def cleanup()
}