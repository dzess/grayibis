package pl.put.gi.use.hiff

import org.slf4j.LoggerFactory
import pl.put.gi.use.ExampleRunnerBase

/**
 * Sample benchmark using the HIFF problem.
 */
object HiffProblemRunnerBOA extends App with ExampleRunnerBase{
    private val logger = LoggerFactory.getLogger(this.getClass)

    logger.info("Starting the data preparation")

    // all data
    experimentKey = "Hiff_2"
    experimentDescription = "Hiff Problem from ECJ Tutorial"
    propertiesPath = "src/test/resources/configurations/hiff/hiff-boa.params"
    preparePrototype()

    logger.info("Starting the experiment preparation")
    prepareRun()
    conf.runDescription = "Hiff Run BOA"

    logger.info("Startin the experiment")
    run()
}