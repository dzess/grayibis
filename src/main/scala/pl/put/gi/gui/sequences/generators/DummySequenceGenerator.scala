package pl.put.gi.gui.sequences.generators

import org.slf4j.LoggerFactory
import pl.put.gi.gui.sequences.InputParameter
import pl.put.gi.gui.sequences.SequenceGeneratorBase
import pl.put.gi.gui.sequences.StringInputParameter
import pl.put.gi.model.ecj.ECJPrototype

class DummySequenceGenerator extends SequenceGeneratorBase {
    
    private val logger = LoggerFactory.getLogger(classOf[DummySequenceGenerator])

    val parameter = StringInputParameter("parameter")

    override def inputs: Seq[InputParameter] = Seq(parameter)

    override def generate(basingPrototype: ECJPrototype, inputs: Seq[InputParameter]): Seq[ECJPrototype] = {
        throw new RuntimeException("not yet implemented")
    }

}