package pl.put.gi.settings

import com.google.inject.Guice
import org.slf4j.LoggerFactory
import com.google.inject.Injector
import scala.collection.JavaConverters._
import com.google.inject.util.Modules
import com.google.inject.Module

/**
 * Always visible accessory for the Google Guice injection objects.
 */
object InjectionManager {

    private val logger = LoggerFactory.getLogger(InjectionManager.this.getClass)

    private var _injector: Injector = null
    def injector = _injector

    def createInjector(modules: Module*) = {

        logger.debug("Initializing the Guice for modules '{}'", modules)
        val listModule = modules.asJavaCollection
        _injector = Guice.createInjector(listModule)
    }
}