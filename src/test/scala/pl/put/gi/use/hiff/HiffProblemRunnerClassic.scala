package pl.put.gi.use.hiff

import org.slf4j.LoggerFactory
import pl.put.gi.use.ExampleRunnerBase

/**
 * Sample benchmark using the HIFF problem with typical genetic algorithm.
 */
object HiffProblemRunnerClassic extends App with ExampleRunnerBase {

    private val logger = LoggerFactory.getLogger(HiffProblemRunnerClassic.this.getClass)

    logger.info("Starting the data preparation")

    // all data
    experimentKey = "Hiff_0"
    experimentDescription = "Hiff Problem from ECJ Tutorial"
    propertiesPath = "src/test/resources/configurations/hiff/hiff-classic.params"
    preparePrototype()

    logger.info("Starting the experiment preparation")
    prepareRun()
    conf.runDescription = "Hiff Run BOA"

    logger.info("Startin the experiment")
    run()
}