# This file does ploting of the data of MGR-TEST-1 using some view
# Requires: gi.setup
# Author: Piotr Jessa

gi.test1.load <- function() {
  all_dataset <- gi.load.view('v_max_fitness','1')
  return(all_dataset)
}

gi.test1.chart <- function(dataset) {
  
  names(dataset)[2] = "exchange"
  dr <- "exchange"
  df <- "avg"
  dk <- "stddev"
  
  p <- ggplot(dataset,aes_string(x = dr,y =df))
  p <- p + geom_point()
  p <- p + labs(x ="exchange-ratio", y = "average fitness")
  
  ggsave("mgr1.pdf",useDingbats=FALSE)
  return (p)
}