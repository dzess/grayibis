package pl.put.bi.functions

import ec.gp.GPNode
import ec.EvolutionState
import ec.gp.GPIndividual
import ec.util.Parameter
import pl.put.bi.data.DoubleData
import ec.gp.GPData
import ec.gp.ADFStack
import ec.Problem

abstract class BinaryOperation extends GPNode {
    override def checkConstraints(state: EvolutionState, tree: Int, ind: GPIndividual, base: Parameter) = {
        super.checkConstraints(state, tree, ind, base)

        if (children.length != 2) {
            val msg = "Incorrect number of children for binary node %s at %s".format(toStringForError, base)
            state.output.error(msg)
        }
    }

    protected def combine(r1: Double, r2: Double): Double

    override def eval(state: EvolutionState, thread: Int, input: GPData, stack: ADFStack, individual: GPIndividual, problem: Problem): Unit = {
        val data = input.asInstanceOf[DoubleData]
        children(0).eval(state, thread, input, stack, individual, problem)
        val res1 = data.y

        children(1).eval(state, thread, input, stack, individual, problem)
        val res2 = data.y

        data.y = combine(res1, res2)
    }
}