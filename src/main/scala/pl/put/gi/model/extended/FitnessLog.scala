package pl.put.gi.model.extended

import java.sql.Timestamp
import org.scalaquery.ql.TypeMapper.LongTypeMapper
import org.scalaquery.ql.TypeMapper.FloatTypeMapper
import org.scalaquery.ql.basic.BasicTable
import pl.put.gi.settings.InnerConstants
import pl.put.gi.model.util.IMapable
import pl.put.gi.model.Log
import pl.put.gi.model.Run
import pl.put.gi.model.util.IdTable

/**
 * Models single entry in DB which has also the information about the fitness.
 *
 * Assumption that fitness value is the Real number represented by double value in
 * Gray Ibis and by Double.
 *
 * NOTE: this code is example of expanding the basic model utility with the
 * additional data in GrayIbis. Other way of expanding are not encouraged because of
 * the possible complications. Foreign key referencing in datamodel is the least
 * troublesome way of relational model expanding.
 */
class FitnessLog(
    private var _value: Float,
    private var _timeStamp: Timestamp = null,
    private var _run: Run = null) extends Log(_timeStamp, _run) {

    def value: Float = _value
    def value_=(other: Float) {
        if (other <= 0) {
            throw new IllegalArgumentException("value cannot be null")
        }
        _value = other
    }
 
}

object FitnessLog extends IdTable[(Long, Long, Float)]("fitness-log")
    with IMapable[(Long, Long, Float), FitnessLog] {

    override def id = column[Long]("id", O PrimaryKey)
    def logId = column[Long]("logId", O NotNull)
    def fitness = column[Float]("fitness", O NotNull)

    def * = id ~ logId ~ fitness

    // foreign key on the Log table
    def fitnessLogFK = foreignKey("fitnessLogFK", logId, Log)(_.id)

    override def fromModel(inputModel: FitnessLog): (Long, Long, Float) = {
        (inputModel.id, inputModel.id, inputModel.value)
    }

    override def toModel(inputTuple: (Long, Long, Float)): FitnessLog = {
        val logId = inputTuple._1
        val refLogId = inputTuple._2
        val value = inputTuple._3

        // construct the fitness log (but some value are meant to be missing)
        val log = new FitnessLog(value)
        log.id = logId

        return log
    }
}