# Example script for the RGL

gi.volcano <- function() {
  data(volcano)
  z <- 2 * volcano
  # Exaggerate the relief
  x <- 10 * (1:nrow(z))
  # 10 meter spacing (S to N)
  y <- 10 * (1:ncol(z))
  # 10 meter spacing (E to W)
  zlim <- range(y)
  zlen <- zlim[2] - zlim[1] + 1
  colorlut <- terrain.colors(zlen) # height color lookup table
  col <- colorlut[ z-zlim[1]+1 ] # assign colors to heights for each point
  open3d()
  surface3d(x, y, z, color=col, back="lines")  
}
