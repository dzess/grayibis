package pl.put.gi.conf

import java.util.Properties
import pl.put.gi.db.IDatabaseHelper
import org.slf4j.LoggerFactory

/**
 * Loads the IDatabaseHelper from the resource stream (being in a classpath)
 */
class DatabaseLocationReader {

    private val logger = LoggerFactory.getLogger(classOf[DatabaseLocationReader])

    val defaultResource = "database.properties"

    def load(resource: String = defaultResource): IDatabaseHelper = {

        val reader = new PropertiesConfigurationReader()
        val config = new Properties();
        val loader = this.getClass().getClassLoader();
        val istream = loader.getResourceAsStream(resource);

        try {
            config.load(istream);
        } catch {
            case e: Throwable => {
                logger.error("Exception happend during loading the test-database.properties from classpath")
                throw new IllegalStateException("Exception happend during loading the test-database.properties from classpath", e)
            }
        }
        val db = reader.readDatabase(config)
        logger.debug("Read the DatabaseHelper: {}", db)
        return db
    }
}