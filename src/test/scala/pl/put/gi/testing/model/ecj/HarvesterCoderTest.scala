package pl.put.gi.testing.model.ecj

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfterEach
import pl.put.gi.model.ecj.util.HarvesterCoder
import org.scalatest.mock.MockitoSugar
import pl.put.gi.running.ecj.IStatisticsHarvester
import pl.put.gi.running.ecj.harvesters.FitnessHarvester
import pl.put.gi.dao.extended.IFitnessLogDAO
import pl.put.gi.settings.InjectionManager
import com.google.inject.AbstractModule

@RunWith(classOf[JUnitRunner])
class HarvesterCoderTest extends FunSuite with BeforeAndAfterEach with MockitoSugar {

    private var coder: HarvesterCoder = null

    override def beforeEach() = {
        
        // introduce dummy creation of harvesters to make up for the other ways
        val logDAOMock = mock[IFitnessLogDAO]
        val fitnessInstance = new FitnessHarvester(logDAOMock)
        val dummyHarvestersModule = new AbstractModule(){
            override def configure() = {
                bind(classOf[FitnessHarvester]).toInstance(fitnessInstance)
            }
        }
        
        InjectionManager.createInjector(dummyHarvestersModule)
        coder = new HarvesterCoder()
    }

    test("decoding empty string") {
        val resutl = coder.decode("")
        assert(resutl.isEmpty === true)
    }

    test("encoding empty sequence") {
        val r = coder.encode(Seq())
        assert(r.isEmpty)
    }

    test("encoding one harvester") {
        val harv = mock[IStatisticsHarvester]
        val result = coder.encode(Seq(harv))
        expect(harv.getClass.getName)(result)
    }

    test("endcoding two same class harvesters throws an exception") {
        val mock1 = mock[IStatisticsHarvester]
        val mock2 = mock[IStatisticsHarvester]

        intercept[IllegalArgumentException] {
            coder.encode(Seq(mock1, mock2))
        }
    }

    private def performCycle(harvester: IStatisticsHarvester) = {
        val s = coder.encode(Seq(harvester))
        val harvesters = coder.decode(s)

        expect(1)(harvesters.size)
        assert(harvester.getClass === harvesters.head.getClass)
    }

    test("full encoding decoding cycle") {
        val harvester = mock[IStatisticsHarvester]
        performCycle(harvester)
    }

    test("integration full encoding decoding cycle") {
        val mockLogDAO = mock[IFitnessLogDAO]
        val harvester = new FitnessHarvester(mockLogDAO)
        performCycle(harvester)
    }

}