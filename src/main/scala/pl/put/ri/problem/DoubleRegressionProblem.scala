package pl.put.ri.problem

import pl.put.regression.RegressionCase
import ec.EvolutionState

/**
 * Generic approach for symbolic regression. Mostly based on @linkplain IntRegressionProblem
 * with exception to being n-dimensional. The n values of X are put into stack.
 * The one value should be returned.
 *
 * @author Piotr Jessa
 */
class DoubleRegressionProblem extends PshRegressionProblem {

    override def evaluateTestCase(testCase: RegressionCase, state: EvolutionState, thread: Int): Double = {

        interpreter.ClearStacks()

        // pushing input value to float stack, this many times as 
        // parameters repeat in stack specifies
        for (i <- 0 until repeatInStack) {
            for (value <- testCase.variables) {
                interpreter.floatStack().push(value.toFloat)
            }
        }

        // setting input value to input stack
        interpreter.inputStack.push(testCase.variables.head)

        // executing the program
        interpreter.Execute(program, interpreter.getExecutionLimit());

        // penalize individual if there is no result on the stack
        // passing NaN means that penalization is maximal
        if (interpreter.floatStack().size() == 0) {
            return Float.NaN
        }

        // return the first element from the stack
        return interpreter.floatStack().top()
    }

}