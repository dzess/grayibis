package pl.put.gi.loaders

/**
 * Responsible for loading ECJ parameters from various sources.
 */
abstract trait IParametersLoader {

    /**
     * Loads parameters of ECJ and transforms it into
     * string maps. Which is natural way to represent the data set and the
     * configuration of the experiment.
     */
    def loadProperties(): Map[String, String]
}