package pl.put.gi.gui.sequences

import pl.put.gi.model.ecj.ECJPrototype

/**
 * Defines what generator should do - should generate valid prototypes
 */
abstract trait ISequenceGenerator {

    def inputs: Seq[InputParameter]

    def generate(basingPrototype: ECJPrototype, inputs: Seq[InputParameter]): Seq[ECJPrototype]
}