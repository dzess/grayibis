package pl.put.gi.util.scala

/**
 * Various utilities method to overcome the JVM type erasure, and enable scala to work with
 * the fullest with its generics.
 */
object TypeUtils {

    /**
     * Gets the companion object instance from the generic type.
     */
    def companionOf[T: Manifest]: Option[T] = {
        try {
            val classOfT = implicitly[Manifest[T]].erasure
            val companionClassName = classOfT.getName + "$"
            val companionClass = Class.forName(companionClassName)
            val moduleField = companionClass.getField("MODULE$")
            Some(moduleField.get(null).asInstanceOf[T])
        } catch {
            case e => {
                e.printStackTrace()
                None
            }
        }
    }
}

object TypeUtilsImplicit{
    
}