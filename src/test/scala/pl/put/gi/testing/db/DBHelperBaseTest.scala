package pl.put.gi.testing.db
import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import pl.put.gi.db.DatabaseHelperBase
import org.scalatest.BeforeAndAfterEach

@RunWith(classOf[JUnitRunner])
class DBHelperBaseTest extends FunSuite with BeforeAndAfterEach {

    private var dbHelper: DatabaseHelperBase = null

    override def beforeEach() = {
        dbHelper = new DatabaseHelperBase()
    }

    test("cannot set URL with wrong values") {
        intercept[IllegalArgumentException] {
            dbHelper.currentURL = null
        }

        intercept[IllegalArgumentException] {
            dbHelper.currentURL = ""
        }
    }

    test("cannot set driver with wrong values") {
        intercept[IllegalArgumentException] {
            dbHelper.driver = null
        }

        intercept[IllegalArgumentException] {
            dbHelper.driver = ""
        }
    }

    test("on frozen configuration setting values throws exception") {
        dbHelper.currentURL = "foo"
        dbHelper.driver = "bar"
        dbHelper.freeze()

        intercept[IllegalStateException] {
            dbHelper.currentURL = "foo_2"
        }

        intercept[IllegalStateException] {
            dbHelper.driver = "bar_2"
        }
    }

    test("freezing not valid configuration throws exception") {
        intercept[IllegalArgumentException] {
            dbHelper.freeze()
        }
    }
}