package pl.put.gi.running.ecj.harvesters

import java.sql.Timestamp
import java.util.Date

import org.slf4j.LoggerFactory

import com.google.inject.Inject

import ec.simple.SimpleStatistics
import ec.Statistics
import pl.put.gi.dao.extended.IMessageLogDAO
import pl.put.gi.model.extended.MessageLog
import pl.put.gi.model.Run
import pl.put.gi.running.ecj.IStatisticsHarvester

/**
 *  Dumps genome to the database making the logging inner structure easier. Dumps the
 *  genome of the most proficient element.
 */
class GenomeHarvester @Inject() (private val messageLog: IMessageLogDAO) extends IStatisticsHarvester {

    private val logger = LoggerFactory.getLogger(classOf[GenomeHarvester])

    override def onStart(statistic: Statistics, run: Run) = {
        logger.debug("On Start Statisitcs invoked with {}", statistic)

    }

    override def onFinish(statistic: Statistics, run: Run) = {
        logger.debug("On Finish Statisitcs invoked with {}", statistic)
    }

    override def onChange(statistic: Statistics, run: Run) = {

        val stat = statistic.asInstanceOf[SimpleStatistics]
        val ind = stat.best_of_run(0)
        val genome = ind.genotypeToString()

        logger.debug("On Change logging genome value {}", genome)
        val timestamp = new Timestamp(new Date().getTime())
        val log = new MessageLog(genome, timestamp, run)

        // save to database
        messageLog.addLog(log)
    }
}