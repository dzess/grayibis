package pl.put.gi.testing.running.single

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.mock.MockitoSugar
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite
import pl.put.gi.dao.IDAOManager
import pl.put.gi.running.ecj.ECJRunner
import pl.put.gi.running.RunnerConfiguration

@RunWith(classOf[JUnitRunner])
class RunnigECJFreezesConfigurationTest extends FunSuite with MockitoSugar with BeforeAndAfterEach {

    val daoManagerMock = mock[IDAOManager]

    test("creating the whole runner freezes its configuration") {
        val conf = new RunnerConfiguration()

        val runner = new ECJRunner(conf, daoManagerMock)

        assert(conf.isFrozen(), "The configuration was not frozen upon creation")
    }

    test("runner will not initialize unless all sample options have value") {
        val conf = new RunnerConfiguration()
        // safe setter will not accept setting nulls so the purely created
        // run configuration is the only option
        
        val runner = new ECJRunner(conf, daoManagerMock)

        intercept[IllegalArgumentException] {

            runner.initialize()

        }
    }
    
    test("settng null on safe setter property invokes exception") {
        val conf  = new RunnerConfiguration()
        
        intercept[IllegalArgumentException]{
            conf.prototypeKey = null
        }
        
        intercept[IllegalArgumentException]{
            conf.runDescription = null
        }
    }
}