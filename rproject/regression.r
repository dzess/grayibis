# Helper for thesis work -- not used in any experiment
p <- ggplot(df,aes(x=x,y=y)) 
p <- p + geom_line(aes(x=x,y=yn,colour="f'(x)")) + geom_line(aes(x=x,y=y,colour="f(x)"))
p <- p + geom_point() 
p <- p + scale_color_manual("",values=c("#000000", "#FF0000"))


ggsave("regression.pdf",useDingbats=FALSE)