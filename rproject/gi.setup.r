# This is file imports all the libraries which are used by the gray ibis.
# Requires: -
# Author: Piotr Jessa

library('DBI')
library('plyr')

# Colours
library("RColorBrewer")

# 2D plotting libraries
library('ggplot2')

# 3D plotting libraries
library('rgl')
library('lattice')

gi.setup.postgres <- function(){
  library('RPostgreSQL')  
}

gi.setup.connection <- function(databaseName){
  drv <- dbDriver("PostgreSQL")
  con <- dbConnect(drv,
                   user="dzess", 
                   password = "qwerty",
                   host="192.168.1.99", 
                   dbname = databaseName)
  return (con)
}

gi.load.view <- function(view,number){
  gi.setup.postgres();
  
  location <- paste("mgr-test-",number,sep = "")
  con <-gi.setup.connection(location)
  
  select <- paste("select * from ",view,sep = "")
  
  all_query <- dbSendQuery(con,select)
  all_dataset <- fetch(all_query,n=-1)
  return(all_dataset)
}

gi.teardown.postgres <- function(){
  detach('package:RPostgreSQL')
}