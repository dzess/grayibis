package pl.put.gi.loaders

import pl.put.gi.model.Prototype
import java.util.Properties
import org.slf4j.LoggerFactory
import java.io.FileInputStream
import java.io.File
import scala.collection.JavaConversions._

/*
 * Loads the properties from pinpointed location into Map<String,String> from the properties file.
 * Note that only <b>flat</b> files are loaded. Loading hierarchical file (with parents) will
 * result in a Exception.
 */
class FlatFileParametersLoader(private val path: String) extends IParametersLoader {

    private val logger = LoggerFactory.getLogger(classOf[FlatFileParametersLoader])

    override def loadProperties(): Map[String, String] = {
        var properties: Properties = new Properties()
        val file = new File(path)

        if (!file.exists) {
            throw new IllegalArgumentException("Not found file " + path)
        }

        try {
            properties.load(new FileInputStream(file))
        } catch {
            case e: Exception => {
                logger.error("Problem reading the params file", e)
                throw new RuntimeException(e)
            }
        }

        var finalMap = Map[String, String]()
        for (entry <- properties.entrySet()) {

            // check if there is a parent assumption in the code
            val key = entry.getKey().toString()
            if (key.startsWith("parent")) {
                logger.error("Parent PARAMS file are not supported")
                throw new IllegalArgumentException("Parent PARAMS file are not supported")
            }

            finalMap = finalMap + (entry.getKey().toString() -> entry.getValue().toString())
        }

        return finalMap
    }
}