package pl.put.gi.testing.conf

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mock.MockitoSugar
import pl.put.gi.conf.PropertiesConfigurationReader
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.util.IllegalFormatException
import java.io.InputStream
import pl.put.gi.db.PostgreSQLHelper
import pl.put.gi.settings.InjectionManager
import pl.put.gi.testing.utils.MockModule

@RunWith(classOf[JUnitRunner])
class PropertiesConfigurationReaderTest extends FunSuite
    with BeforeAndAfterEach with MockitoSugar {

    var reader: PropertiesConfigurationReader = null

    override def beforeEach() = {
        InjectionManager.createInjector(new MockModule())
        reader = new PropertiesConfigurationReader()
    }

    private def dumpStringToStream(input: String): InputStream = {
        new ByteArrayInputStream(input.getBytes())
    }

    test("reading inproper format file throw some exception") {
        val content = "stupid file"
        val stream = this.dumpStringToStream(content)

        // how to check that format file is wrong ?
        intercept[IllegalArgumentException] {
            reader.load(stream)
        }
    }

    test("reading proper file but with wrong database driver") {
        val builder = new StringBuffer()
        builder.append("database = oracle")
        builder.append("\n")

        builder.append("jdbc = some stupid string")
        builder.append("\n")

        var fileContent = this.dumpStringToStream(builder.toString)

        intercept[IllegalStateException] {
            reader.load(fileContent)
        }
    }

    test("reading proper file creates valid configuration which can be freezed") {
        val builder = new StringBuffer()

        // database configuration properties
        builder.append("database = postgresql")
        builder.append("\n")

        builder.append("jdbc = some stupid string")
        builder.append("\n")

        // map configuration properties
        builder.append("configuration = pl.put.gi.running.universal.RunnerConfigurationProvider")
        builder.append("\n")

        builder.append("map.key_1 = 2")
        builder.append("\n")
        builder.append("map.key_2 = 17")
        builder.append("\n")

        var stream = this.dumpStringToStream(builder.toString)

        val conf = reader.load(stream)

        // dbHelper
        assert(conf.dbHelper.isInstanceOf[PostgreSQLHelper])
        assert(conf.dbHelper.currentURL === "some stupid string")

        // runConfigurationProvider
        val runs = conf.runConfigurationProvider.generate()
        assert(runs.size == 19)
        assert(runs.count(rConf => rConf.prototypeKey == "key_1") == 2)
        assert(runs.count(rConf => rConf.prototypeKey == "key_2") == 17)

    }
}