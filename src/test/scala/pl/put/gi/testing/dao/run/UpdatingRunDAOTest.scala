package pl.put.gi.testing.dao.run

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite
import java.sql.Date
import java.sql.Timestamp
import pl.put.gi.model.Run
import pl.put.gi.model.State
import pl.put.gi.testing.dao.utils.RunDAOTestBase
import pl.put.gi.util.scala.StringUitls

@RunWith(classOf[JUnitRunner])
class UpdatingRunDAOTest extends RunDAOTestBase with FunSuite with BeforeAndAfterEach {

    override def beforeEach() = {
        this.beforeEachMethod()
    }

    override def afterEach() = {
        this.afterEachMethod()
    }

    test("updating the run object in wrong way invokes exception") {
        val run = this.getRun()
        dao.save(run)

        run.description = Some("changed")

        intercept[IllegalStateException] {
            dao.save(run)
        }
    }

    test("updating the run object in wrong way invokes exception, second case") {
        val run = this.getRun()
        dao.save(run)

        run.description = Some("changed")
        run.finishTime = new Timestamp(2500L)

        intercept[IllegalStateException] {
            dao.save(run)
        }
    }

    test("updating the run object with state is valid always") {
        val run = this.getRun()
        dao.save(run)
        run.state = State.initializing

        // throws no exception
        dao.save(run)

        val retRun = dao.getRun(run.id)
        assert(retRun.state === State.initializing)

        // try updating the second time (no exception)
        run.state = State.initialized
        dao.save(run)

        val retRun2 = dao.getRun(run.id)
        assert(retRun2.state === State.initialized)
    }


    test("updating thr run with error long message saves this error messsage") {
        val run = this.getRun()
        val veryLongString = StringUitls.randomString(1000)
        run.error = Some(veryLongString)
        dao.save(run)

        val fetchRun = dao.getRun(run.id)
        expect(run.error)(fetchRun.error)
    }

    test("updating the run with error message is applicable once") {
        val run = this.getRun()
        dao.save(run)
        run.error = Some("this is different error message")

        intercept[IllegalStateException] {
            dao.save(run)
        }
    }

    test("updating the run object with start time is only applicable once") {
        val run = this.getRun()

        // setting null counts as no value
        run.startTime = null
        dao.save(run)

        run.startTime = new Timestamp(2500L)

        // throws no exception
        dao.save(run)

        val retRun = dao.getRun(run.id)
        assert(retRun.startTime === new Timestamp(2500L))

        // try updating the second time (on the Run level should be invoked)
        run.startTime = new Timestamp(10)
        intercept[IllegalStateException] {
            dao.save(run)
        }
    }

    test("updating the run object with finishing time is only applicable once") {
        val run = this.getRun()
        //setting null counts as no value (thus no creation is done_
        run.finishTime = null
        dao.save(run)

        run.finishTime = new Timestamp(2500L)

        // throws no exception
        dao.save(run)

        val retRun = dao.getRun(run.id)
        assert(retRun.finishTime === new Timestamp(2500L))

        // try updating the second time (on the Run level should be invoked)
        run.finishTime = new Timestamp(10)
        intercept[IllegalStateException] {
            dao.save(run)
        }
    }

}