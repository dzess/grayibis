package pl.put.gi.db

import pl.put.gi.conf.PropertiesConfigurationReader
import java.util.Properties
import org.slf4j.LoggerFactory
import org.scalaquery.session.Database

/**
 * Database helper class used for testing. This uses the
 * test-database.properties file place in the 'src/test/resources' from which
 * it reads the configuration of the database.
 *
 * The database configuration which is stored is the same format as in the class
 * PropertiesConfigurationReader.
 */
class TestDatabaseHelper extends DatabaseHelperBase {

    private val logger = LoggerFactory.getLogger(classOf[TestDatabaseHelper])
    
    // from where read the whole configuration
    val testDatabaseProperties = "test-database.properties"

    // constructor code goes below
    private val reader = new PropertiesConfigurationReader()

    private val config = new Properties();
    private val loader = this.getClass().getClassLoader();
    private val istream = loader.getResourceAsStream(testDatabaseProperties);

    try {
        config.load(istream);
    } catch {
        case e: Throwable => {
            logger.error("Exception happend during loading the test-database.properties from classpath")
            throw new IllegalStateException("Exception happend during loading the test-database.properties from classpath", e)
        }
    }

    val internalDBHelper = reader.readDatabase(config)

    // set up the code from the superclass with configuration of the internalDBHelper
    this.driver = internalDBHelper.driver
    this.currentURL = internalDBHelper.currentURL
    
    override def validate() = {
        super.validate()
        
        // check if the connection is possible using this settings
        val db = Database.forURL(this.currentURL, driver = this.driver)
    }
}