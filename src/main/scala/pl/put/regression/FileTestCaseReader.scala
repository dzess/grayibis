package pl.put.regression

import ec.util.Parameter
import ec.EvolutionState
import java.io.FileReader
import java.util.Locale
import java.util.Scanner
import pl.put.gi.util.scala.FileUtils.using
import java.io.BufferedReader

/**
 * Basic port of code from ecj-psh. This code is helper to read data from params file. This version
 * is enhanced reader of test cases which can load n-dimensional test cases for symbolic regression problems.
 */
class FileTestCaseReader extends ITestCaseReader {

    val P_TESTCASE_FILE = "test-cases-file"

    def load(state: EvolutionState, base: Parameter): Seq[(RegressionCase, Double)] = {
        val parameter = base.push(P_TESTCASE_FILE)
        val file = state.parameters.getFile(parameter, null)

        var result = Seq[(RegressionCase, Double)]()

        using(new FileReader(file))(reader => {
            val buff = new BufferedReader(reader)
            var line = "";
            line = buff.readLine()
            while (line != null) {
                val values = line.split("\\s+")
                val ins = values.slice(0, values.length - 1).map(x => x.toDouble)
                val r = new RegressionCase(ins.toSeq)
                val out = values.last.toDouble
                result = result ++ Seq((r, out))

                // onto next loop
                line = buff.readLine()
            }
        })
        result
    }
}