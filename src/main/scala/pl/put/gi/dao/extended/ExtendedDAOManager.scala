package pl.put.gi.dao.extended

import org.slf4j.LoggerFactory

import com.google.inject.Inject

import pl.put.gi.dao.ecj.IECJPrototypeDAO
import pl.put.gi.dao.impl.DAOManager
import pl.put.gi.dao.IDAO
import pl.put.gi.dao.IDAOManager
import pl.put.gi.dao.ILogDAO
import pl.put.gi.dao.IPrototypeDAO
import pl.put.gi.dao.IRunDAO

/**
 * Example how to extend the basic three types of model and corresponding DAOManager.
 * This special implementation of IDAOManager is to be used with ECJ run classes.
 */
class ExtendedDAOManager @Inject() (
    prototypeDAO: IPrototypeDAO,
    runDAO: IRunDAO,
    logDAO: ILogDAO,
    fitnessLogDAO: IFitnessLogDAO,
    messageLogDAO: IMessageLogDAO,
    ecjPrototypeDAO: IECJPrototypeDAO) extends DAOManager(prototypeDAO, runDAO, logDAO) with IDAOManager {

    private val logger = LoggerFactory.getLogger(classOf[ExtendedDAOManager])
    private val extendedDaos: List[IDAO] = List(
        // NOTE: extending this daos can be done here
        fitnessLogDAO,
        messageLogDAO,
        ecjPrototypeDAO)

    override def dropTables() = {
        extendedDaos.reverse.foreach(x => x.dropTable())
        super.dropTables()
    }

    override def createTables() = {
        super.createTables()
        extendedDaos.foreach(x => x.createTable())
    }

    override def safeCreateTables() = {
        // do not invoke the super.safeCreateTables()
        // because of the not proper positions of 
        val concatenatedDaos = this.getDAOs()
        val tablesNotExisting = this.checkSafeCreation(concatenatedDaos)

        if (tablesNotExisting) {
            logger.debug("All extended tables are not existing - creating")
            ExtendedDAOManager.this.createTables()
        } else {
            logger.debug("All extended tables are already defined - skupping creation")
        }
    }

    override def getDAO[T <: IDAO](cls: Class[T]): T = {

        val daos = this.getDAOs()
        val objOption = this.checkGetSingleDAO(daos, cls)

        if (objOption.isEmpty) {
            logger.warn("cannot find the search dao type {}", cls)
            throw new IllegalArgumentException("cannot find the searched dao type")
        }

        val obj = objOption.get

        logger.debug("Found dao {}", obj)

        return obj.asInstanceOf[T]
    }

    override def getDAOs(): List[IDAO] = {
        // nice scala collection features using the 
        super.getDAOs() ++ extendedDaos
    }
}