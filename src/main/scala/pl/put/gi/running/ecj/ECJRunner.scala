package pl.put.gi.running.ecj

import ec.util.ParameterDatabase
import ec.EvolutionState
import ec.Evolve
import java.util.Dictionary
import java.util.Hashtable
import org.slf4j.LoggerFactory
import pl.put.gi.dao.IDAOManager
import pl.put.gi.loggers.DBLog
import pl.put.gi.loggers.LoggerLevel
import pl.put.gi.loggers.LoggerLog
import pl.put.gi.running.RunnerBase
import pl.put.gi.running.ecj.security.ECJSecurityManager
import pl.put.gi.running.RunnerConfiguration
import pl.put.gi.running.ecj.prototype.GenericPrototypeReader
import scala.collection.JavaConversions.mapAsJavaMap
import pl.put.gi.running.ecj.harvesters.NullStatisticsHarvester

/**
 * Runs experiments for the ECJ library using the provided configuration.
 */
class ECJRunner(_configuration: RunnerConfiguration,
    _daoManager: IDAOManager,
    protected val statistics: IStatisticsHarvester = new NullStatisticsHarvester(),
    protected val prototypeReader: IPrototypeReader = new GenericPrototypeReader())
    extends RunnerBase(_configuration, _daoManager) {

    private val logger = LoggerFactory.getLogger(classOf[ECJRunner])

    protected var state: EvolutionState = null
    protected var result: Int = 0

    protected var ecjSecurityManager: SecurityManager = null

    private def mapToDictionary(m: Map[String, String]): Dictionary[String, String] = {
        new Hashtable(m)
    }

    override def initializeExperiment() = {

        // hijack the system security manager and change it to the specific one
        logger.debug("Changing security manager - started")
        val currentSecurityManager = System.getSecurityManager()
        if (currentSecurityManager == null) {
            ecjSecurityManager = new ECJSecurityManager()
            System.setSecurityManager(ecjSecurityManager)
            logger.debug("Changing security manager - finished - replaced")
        }
        logger.debug("Changing security manager - finished - stayed")

        // provide ECJ typical settings
        val m = prototypeReader.generateParameters(expPrototype)
        val paramDb = new ParameterDatabase(mapToDictionary(m))

        val seed = _configuration.seed

        // it is the seed - the seed should be part of configuration at least
        state = Evolve.initialize(paramDb, seed)
        state.startFresh()

        // connect with the DB Logging mechanism
        logger.debug("Initializing Logger subsystem - started")
        val dbLog = new DBLog(logDao, expRun, true)
        val loggerLog = new LoggerLog(LoggerLevel.info, true)
        state.output.addLog(dbLog)
        state.output.addLog(loggerLog)
        logger.debug("Initializing Logger subsystem - finished")

        // TODO: capture the standard output from the experiment into the logger
        // this behavior might be a bit weird for the
    }

    override def runExperiment() = {

        logger.debug("Begining the ECJ experiment")
        statistics.onStart(state.statistics, expRun)

        // the code which runs the ECJ experiment
        result = EvolutionState.R_NOTDONE;
        var counter = 0
        while (result == EvolutionState.R_NOTDONE) {
            result = state.evolve();

            logger.debug("Next generation {}", counter)
            counter += 1

            statistics.onChange(state.statistics, expRun)
        }

        statistics.onFinish(state.statistics, expRun)
        logger.debug("Finished the ECJ experiment")
    }

    override def cleanupExperiment() = {
        // ECJ specific cleanup here
        state.finish(result);
        Evolve.cleanup(state);

        // it seems that security manager in the jvm platform cannot be changed
        // after the initial assignment - thus we have to let ECJSecurityManager stay 
    }

}