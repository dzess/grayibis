package pl.put.gi.exporters

import java.io.OutputStream
import java.io.OutputStreamWriter
import java.text.SimpleDateFormat
import java.util.Date

import org.slf4j.LoggerFactory

import pl.put.gi.model.Prototype

/**
 *  Saves the data into stream in a java properties format.
 */
class FlatFileParametersExporter extends IParametersExporter {

    private val logger = LoggerFactory.getLogger(classOf[FlatFileParametersExporter])

    private val formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss")

    private def getLineFromParam(tuple: (String, String)): String = {
        val builder = new StringBuilder()
        builder.append(tuple._1)
        builder.append(" = ")
        builder.append(tuple._2)
        builder.append("\n")

        builder.toString
    }

    private def getLineFromHead(prototype: Prototype): String = {
        val builder = new StringBuilder()

        val exporterLine = "# %s\n".format(classOf[FlatFileParametersExporter].getName)
        val prototypeLine = "# Key: '%s'\n".format(prototype.key)
        val descriptionLine = "# Description: '%s'\n".format(prototype.description.getOrElse("<none>"))

        val currentDate = new Date()
        val formattedDate = formatter.format(currentDate)
        val dateLine = "# %s\n".format(currentDate)

        // main headline
        builder.append(prototypeLine)
        builder.append(descriptionLine)
        builder.append("# \n")
        
        // additional information
        builder.append("# Exported: \n")
        builder.append(dateLine)
        builder.append(exporterLine)
        builder.append("# \n")

        builder.toString
    }

    override def export(prototype: Prototype, stream: OutputStream) = {
        val parameters = prototype.parameters

        val writer = new OutputStreamWriter(stream, "UTF-8")

        val headLine = this.getLineFromHead(prototype)
        writer.write(headLine)
        
        // sort the parameters for readability
        val sortedParameters = parameters.toSeq.sortWith((x,y) => x._1 < y._1).toSeq

        for (param <- sortedParameters) {
            val line = this.getLineFromParam(param)
            writer.write(line)
        }
        writer.close()
    }
}