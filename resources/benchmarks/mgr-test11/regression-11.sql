﻿select * from "prototypes-ecj" ep join "prototypes" p on ep."prototypeId" = p.id;
select * from "prototypes" p where p.key like '%Perf_%' order by p.key;

-- all regression runs
select
	r."id",r."modify-time", r."state", r."start-time",r."finish-time", p."key",r."error-message"
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
where 
	p."key" like '%Perf%'
and
	r.state = 'done'
order by r."modify-time" desc;

-- get information about this fitness run
select 
	* 
from "runs" r 
join "prototypes" p on r."experimentId" = p."id" 
join "logs" l on l."runId" = r."id"
join "koza-adjusted-fitness-log" kaF on kAF."logId" = l."id"
where p.key like 'Perf%'
and r.state = 'done';


-- get the best adjusted fitness of all the runs
select 
	r.id
	,p.key
	,max(kaf.fitness) as "adjusted fitnes"
	,count(*) as "generations"
from "runs" r 
join "prototypes" p on r."experimentId" = p."id" 
join "logs" l on l."runId" = r."id"
join "koza-adjusted-fitness-log" kaf on kaf."logId" = l."id"
where p.key like '%Perf%'
and r.state = 'done'
group by r.id, p.key
order by r.id ;

-- show how the fitness changes during regression run
select 
	kaf.fitness,
	l.timestamp
from "runs" r 
join "prototypes" p on r."experimentId" = p."id" 
join "logs" l on l."runId" = r."id"
join "koza-adjusted-fitness-log" kaf on kaf."logId" = l."id"
where r.id = 59
order by l.timestamp;

-- CONCREATE ANALYSIS:

-- analysis of the runned experiments
select
	x.key
	,avg(x."adjusted-fitness") as "avg-fitness"
	,stddev(x."adjusted-fitness") as "stddev-fitness"
	,avg(x."generations") as "avg-generations"
	,stddev(x."generations") as "stddev-generations"
	,count(*) as "runs"
from 
(
	select 
	r.id
	,p.key as "key"
	,max(kaf.fitness) as "adjusted-fitness"
	,count(*) as "generations"
	from "runs" r 
	join "prototypes" p on r."experimentId" = p."id" 
	join "logs" l on l."runId" = r."id"
	join "koza-adjusted-fitness-log" kaf on kaf."logId" = l."id"
	where p.key like 'Double_Regression%Ins%'
	and r.state = 'done'
	group by r.id, p.key
	order by r.id 
) x
group by x.key;


-- see if there are some logs mostly
select * from "koza-adjusted-fitness-log";
select * from "koza-hits-fitness-log";

-- remove of all conected prototypes from tables
select remove_prototype('Regression_Psh__');
