package pl.put.gi.testing.gui.generators

import org.junit.runner.RunWith
import org.mockito.Matchers.any
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.when
import org.scalatest.junit.JUnitRunner
import org.scalatest.mock.MockitoSugar
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite

import pl.put.gi.exceptions.IllegalSequenceParametersException
import pl.put.gi.gui.sequences.generators.StepSequenceGenerator
import pl.put.gi.gui.sequences.IPrototypeGenerator
import pl.put.gi.gui.sequences.InputParameter
import pl.put.gi.gui.sequences.StringInputParameter
import pl.put.gi.model.ecj.ECJPrototype

@RunWith(classOf[JUnitRunner])
class StepSequenceGeneratorTest extends FunSuite with MockitoSugar with BeforeAndAfterEach {

    private var sequence: StepSequenceGenerator = null
    private var mockPrototypeGen: IPrototypeGenerator = null

    override def beforeEach() = {
        mockPrototypeGen = mock[IPrototypeGenerator]

        // this method stubbing is not the best, yet should be enough
        when(mockPrototypeGen.generateSingle(any[String], any[Any], any[ECJPrototype]))
            .thenReturn(new ECJPrototype())

        sequence = new StepSequenceGenerator(mockPrototypeGen)
    }

    private def insertValue(input: InputParameter, value: String): StringInputParameter = {
        StringInputParameter(input.name, value)
    }

    private def generateParams(start: String, stop: String, step: String) = {

        val startParam = insertValue(sequence.start, start)
        val stopParam = insertValue(sequence.stop, stop)
        val stepParam = insertValue(sequence.step, step)

        val parameterParam = insertValue(sequence.parameter, "foo")
        Seq(startParam, stopParam, stepParam, parameterParam)
    }

    test("series with wrong parameters, minus range, throws exception") {
        val start = "1.0"
        val stop = "0.5"
        val step = "0.1"

        val inputs = generateParams(start, stop, step)
        val basingPrototype = mock[ECJPrototype]

        intercept[IllegalSequenceParametersException] {
            sequence.generate(basingPrototype, inputs)
        }
    }

    test("series with wrong parameters, minus start, throws exception") {
        val start = "-1.0"
        val stop = "0.5"
        val step = "0.1"

        val inputs = generateParams(start, stop, step)
        val basingPrototype = mock[ECJPrototype]

        intercept[IllegalSequenceParametersException] {
            sequence.generate(basingPrototype, inputs)
        }
    }

    test("series with wrong parameters, minus step, throws exception") {
        val start = "0.0"
        val stop = "0.5"
        val step = "-0.1"

        val inputs = generateParams(start, stop, step)
        val basingPrototype = mock[ECJPrototype]

        intercept[IllegalSequenceParametersException] {
            sequence.generate(basingPrototype, inputs)
        }
    }

    test("series generate one case for start stop equal") {
        val start = "0.0"
        val stop = "0.0"
        val step = "0.1"

        val inputs = generateParams(start, stop, step)
        val basingPrototype = mock[ECJPrototype]

        val result = sequence.generate(basingPrototype, inputs)

        // assert
        expect(1)(result.size)
        verify(mockPrototypeGen, times(1)).generateSingle(any[String], any[Double], any[ECJPrototype])
    }

    test("series generate inclusive cases") {
        val start = "0.0"
        val stop = "0.2"
        val step = "0.1"

        val inputs = generateParams(start, stop, step)
        val basingPrototype = mock[ECJPrototype]

        val result = sequence.generate(basingPrototype, inputs)

        // assert
        expect(3)(result.size)
        verify(mockPrototypeGen, times(3)).generateSingle(any[String], any[Double], any[ECJPrototype])
    }
}