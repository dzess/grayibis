package pl.put.gi.use.hiff

import pl.put.gi.use.ExampleRunnerBase
import org.slf4j.LoggerFactory

/**
 * Sample benchmark using the HIFF problem. 
 */
object HiffProblemRunnerUMDA extends App with ExampleRunnerBase {

    private val logger = LoggerFactory.getLogger(this.getClass)

    logger.info("Starting the data preparation")

    // all data
    experimentKey = "Hiff_1"
    experimentDescription = "Hiff Problem from ECJ Tutorial"
    propertiesPath = "src/test/resources/configurations/hiff/hiff-umda.params"
    preparePrototype()

    logger.info("Starting the experiment preparation")
    prepareRun()
    conf.runDescription = "Hiff Run UMDA"

    logger.info("Startin the experiment")
    run()

}