﻿-- This file contains SQL for basic quieries against Gray Ibis schema
-- @author: Piotr Jessa

-- baisc queries
select * from prototypes;
select * from runs;
select * from logs;

select * from "fitness-log";
select * from "message-log";
select * from "prototypes-ecj" ep join "prototypes" p on ep."prototypeId" = p.id;

-- basic analytical queries
select count(*) from runs;
select count(*) from logs;
select count(*) from "fitness-log";
select count(*) from "message-log";

select * from logs l order by timestamp desc;

-- quering about the fitness
select * from "fitness-log" f join "logs" l on f."logId" = l.id where l."runId" = 87;

-- quering about the runs and how many to go still (grouped)
-- TODO: this kind of query should be refactored out into function
select x."current", x."all", 100 * cast(x."current" as numeric) /  cast(x."all" as numeric)  as "percent" from
(
select 
	(
	select count(*) from "prototypes" p join "runs" r on r."experimentId" = p."id" 
	where p.key like '%Perf_%' 
	) as "current",
	(select count(distinct p.key) from "prototypes" p where p.key like '%Perf_%') * 5 as "all"
) x;
	

-- quering about the runs and how many to go still (grouped)
select 
	p.key, count(*) as "total", count(*)/ 10.0 as "percent"
from 
	"prototypes" p
left outer join 
	"runs" r on r."experimentId" = p."id"
group by p.key
order by p.key;

-- quering about all the runs
select
	r."id",r."modify-time", r."state", r."start-time",r."finish-time", p."key"
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
order by r."modify-time" desc;

-- quering runs which are too long in transition state
select * from "runs" r 
where 
	-- transition state definition
	r."state" not in ('done','error','cancelled','stopped','undefined')
	-- stayed too long without update
	and ( NOW() - r."modify-time" > interval '1 minute' );


-- quering all information to dump into analisys
select 	
	r.id,
	r.state, 
	r."error-message",
	r."start-time",
	r."finish-time",
	p.key,
	l.timestamp,
	fl.fitness
from
	"runs" r join "prototypes" p on r."experimentId" = p."id" 
	join "logs" l on l."runId" = r."id"
	join "fitness-log" fl on fl."logId" = l."id";

-- quering about the runs using problem scoping
select
	r."id",r."modify-time", r."state", r."start-time",r."finish-time", p."key",r."error-message"
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
where p."key" like '%One%'
order by r."modify-time" desc;	

-- all data from the Hiff point of view
select 	
	r.id,
	r.state, 
	r."error-message",
	r."start-time",
	r."finish-time",
	p.key,
	l.timestamp,
	fl.fitness
from
	"runs" r join "prototypes" p on r."experimentId" = p."id" 
	join "logs" l on l."runId" = r."id"
	join "fitness-log" fl on fl."logId" = l."id"
where 
	p."key" like '%Hiff%';
