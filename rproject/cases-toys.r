# This is sample file for function generation for symbolic regression
# most of the functions come from the site: http://www.symbolicregression.com/?q=node/5

gi.combine <- function(in1, in2){
  
  # Note that input are two not binded sequences
  cartesian <- expand.grid(in1,in2)
  x1 <- cartesian$Var1
  x2 <- cartesian$Var2
  df <- data.frame(x1,x2)
  return (df)
}

gi.dump <- function(dataset,name){
  fileName <-  paste(name,".txt",separator="")
  write.table(dataset,file = fileName ,row.names = FALSE, col.names = FALSE)
}

gi.plot <- function(dataset){
  size <- abs(max(dataset$y) - min(dataset$y))*10000
  
  plot3d(
    dataset$x1,dataset$x2,dataset$y,
    ,xlab = "x1", ylab = "x2", zlab = "f(x1,x2)"
    #,type = c("shade","wire","dots")     
    #,type = 'l'
    ,size = '5'
    ,col = rainbow(1000)
    )
}

gi.kotanchek <- function(in1,in2){
  
  df <- gi.combine(in1,in2)
  x1 <- df$x1
  x2 <- df$x2
  
  y <- exp(-(x1 - 1)^2)/(3.2 + (x2 - 2.5)^2)
  out <- data.frame(x1,x2,y)
  return (out)
}

gi.salustowicz <- function(in1,in2){
  df <- gi.combine(in1,in2)
  x1 <- df$x1
  x2 <- df$x2
  
  y <- exp(-x1)*x1^3 * (x2-5) *cos(x1)*sin(x1) * (cos(x1)*sin(x1)^2 - 1)
  out <- data.frame(x1,x2,y)
  return (out)
}

gi.sinecosie <- function(in1,in2){
  
  df <- gi.combine(in1,in2)
  x1 <- df$x1
  x2 <- df$x2
  
  y <- 6*sin(x1)*cos(x2)
  out <- data.frame(x1,x2,y)
  return (out)
}

gi.rationalPolynomial <- function(in1,in2){
  
  df <- gi.combine(in1,in2)
  x1 <- df$x1
  x2 <- df$x2
  
  y <- ((x1 - 3)^4 + (x2 - 3)^3 - (x2 - 3))/((x2 - 2)^4 + 10)
  out <- data.frame(x1,x2,y)
  return (out)
}

gi.unwrappedball2d <- function(in1,in2){
  df <- gi.combine(in1,in2)
  x1 <- df$x1
  x2 <- df$x2
  
  y <- 10 / ( 5 + (x1 -3)^2 + (x2-3)^2 ) 
  out <- data.frame(x1,x2,y)
  return (out)
}