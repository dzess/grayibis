package pl.put.gi.dao

/**
 * Describes what DAO object should provide
 */
abstract trait IDAO {

    /**
     * Drops the table corresponding with DAO object.
     *
     * Note that the only this table will be dropped - no other so if there
     * is any foreign key relation the action will result in exception
     */
    def dropTable()

    /**
     * Creates the table corresponding with DAO object.
     *
     * Note that only this table will be created - any missing foreign key
     * relation would throw an exception.
     */
    def createTable()

    /**
     * Indicates if the table is already created or not. 
     */
    def isTableCreated(): Boolean
}