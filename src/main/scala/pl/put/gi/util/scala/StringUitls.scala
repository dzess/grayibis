package pl.put.gi.util.scala

import java.util.Random

/**
 * Utility methods for manipulating strings.
 */
object StringUitls {
    private val AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private val rnd = new Random();

    /**
     * Generates the random string of the demanded length
     */
    def randomString(len: Int): String = {

        val sb = new StringBuilder(len);
        for (i <- 0 to len) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }

        return sb.toString();
    }
}