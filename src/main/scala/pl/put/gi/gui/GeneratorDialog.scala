package pl.put.gi.gui

import scala.swing._
import scala.swing.event._
import org.slf4j.LoggerFactory
import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.settings.InjectionManager
import pl.put.gi.dao.ecj.IECJPrototypeDAO
import pl.put.gi.gui.sequences.SequenceGenerator
import pl.put.gi.gui.sequences.InputParameterPanel

/**
 * The dialog which is shown for generating series of prototypes
 */
class GeneratorDialog(owner: Window) extends Dialog(owner) {

    private val logger = LoggerFactory.getLogger(classOf[GeneratorDialog])

    private var _basePrototype: ECJPrototype = null
    def prototype = _basePrototype
    def prototype_=(other: ECJPrototype) {
        if (other == null) {
            throw new IllegalArgumentException("prototype cannot be null")
        }
        _basePrototype = other
    }

    // specify model
    val items = SequenceGenerator.getAll

    // specify controls
    val parameters = new InputParameterPanel()

    val combo = new ComboBox(items) {
        listenTo(selection)
        reactions += {
            case SelectionChanged(_) => {
                logger.debug("Selection Changed")

                val selectedGenerator = selection.item
                parameters.updateControls(selectedGenerator.inputs)
                parameters.revalidate()
                parameters.repaint()
            }
        }
    }

    val cancelButton = new Button("Cancel") {
        reactions += {
            case ButtonClicked(_) => {
                logger.debug("Cancel Button clicked")
                close()
            }
        }
    }
    val generateButton = new Button("Generate") {
        reactions += {
            case ButtonClicked(_) => {
                logger.debug("Generate Button clicked")

                // read the CURRENT context of database !!!
                val prototypeDAO = InjectionManager.injector.getInstance(classOf[IECJPrototypeDAO])

                val selectedGenerator = combo.selection.item
                val fields = parameters.readControls

                // invoke generate method
                val prototypes = selectedGenerator.generate(prototype, fields)
                logger.info("Generated new prototypes: {}", prototypes.size)
                for (prototype <- prototypes) {
                    logger.info("Generated prototype {}", prototype)
                    prototypeDAO.save(prototype)
                    logger.info("Saved Generated prototype {}", prototype)
                }
                close()
            }
        }
    }

    val buttonPanel = new FlowPanel {
        contents += generateButton
        contents += cancelButton
    }

    // specify the layout
    contents = new BorderPanel {
        add(combo, BorderPanel.Position.North)
        add(parameters, BorderPanel.Position.Center)
        add(buttonPanel, BorderPanel.Position.South)
    }

    title = "Generate series"
    preferredSize = new Dimension(640, 480)
    size = new Dimension(640, 480)

    // this code needs to be triggered to show
    parameters.updateControls(combo.selection.item.inputs)
    parameters.revalidate()
    parameters.repaint()

}