package pl.put.gi.model

/**
 * More advanced form of enumeration which describes the current state of the
 * experiment.
 *
 * Note that thanks to Enumeration implementation methods toString and withName are
 * already implemented.
 */
object State extends Enumeration {
    type State = Value

    // happy path state change scenario
    val initializing = Value("initializing")
    val initialized = Value("initialized")

    val running = Value("running")
    val runned = Value("runned")

    val cleaning = Value("cleaning")
    val done = Value("done")

    // not happy path scenarios
    val error = Value("error") // when execution ended with error (unexpected)
    val cancelled = Value("cancelled") // when execution was cancelled (cannot be resumed)
    val stopped = Value("stopped") // when execution was stopped (can be resumed)

    // undefined value
    val undefined = Value("undefined")
}