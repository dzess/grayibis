package pl.put.gi.gui.sequences.generators

import org.slf4j.LoggerFactory
import pl.put.gi.gui.sequences.InputParameter
import pl.put.gi.gui.sequences.SequenceGeneratorBase
import pl.put.gi.gui.sequences.StringInputParameter
import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.gui.sequences.IPrototypeGenerator
import pl.put.gi.exceptions.IllegalSequenceParametersException
import scala.collection.immutable.NumericRange

/**
 * Generates the exactly the same prototypes as base except for one parameter which
 * will be incremented using 3 options parameters.
 *
 * Generates inclusively parameters - check test cases for exact specification.
 */
class StepSequenceGenerator(private val prototypeGenerator: IPrototypeGenerator) extends SequenceGeneratorBase {

    private val logger = LoggerFactory.getLogger(classOf[StepSequenceGenerator])

    // define inputs for this kind of generator
    val parameter = StringInputParameter("parameter")
    val start = StringInputParameter("start")
    val stop = StringInputParameter("stop")
    val step = StringInputParameter("step")

    private val sequences = Seq(parameter, start, stop, step)

    override def inputs: Seq[InputParameter] = sequences

    override def generate(basingPrototype: ECJPrototype, inputs: Seq[InputParameter]): Seq[ECJPrototype] = {

        val parameterName = findParameter[String](parameter, inputs)
        val startValue = findParameter[String](start, inputs).toDouble
        val stopValue = findParameter[String](stop, inputs).toDouble
        val stepValue = findParameter[String](step, inputs).toDouble

        // validation of sequence
        if (startValue < 0 || stopValue < 0) {
            throw new IllegalSequenceParametersException("start value and stop value must be non negative")
        }

        if (stopValue < startValue) {
            throw new IllegalSequenceParametersException("stop value is lower than start value")
        }

        if (stepValue <= 0) {
            throw new IllegalSequenceParametersException("step value must be positive")
        }

        // generate the sequence

        val range = startValue.to(stopValue, stepValue)
        prototypeGenerator.reset()
        for (v <- range) yield {
            prototypeGenerator.generateSingle(parameterName, v, basingPrototype)
        }
    }

}