package pl.put.bi.problem

import ec.gp.GPProblem
import ec.simple.SimpleProblemForm
import ec.gp.GPData
import ec.EvolutionState
import ec.Individual
import pl.put.bi.data.DoubleData
import ec.gp.GPIndividual
import scala.math
import ec.gp.GPNode
import ec.gp.koza.KozaFitness
import pl.put.regression.RegressionCase
import pl.put.regression.RegressionProblem
import ec.util.Parameter
import pl.put.regression.RegressionCase

class DoubleRegressionProblem extends GPProblem
    with SimpleProblemForm with RegressionProblem {

    private var _output: DoubleData = new DoubleData

    def x: RegressionCase = _x
    private var _x: RegressionCase = null
    private var _gpInd: GPIndividual = null

    // some constants which should be parameters of the problem
    // TODO: introduce the better way of abstracting this parameters
    val maxTreeDepth = 7
    val maxNodes = 127

    override def setup(state: EvolutionState, base: Parameter) = {
        super.setup(state, base)

        loadTestCases(state, base)

        // TODO: put here loading any more possible things

        // safe check
        state.output.exitIfErrors()
    }

    override def clone(): AnyRef = {
        val copy = super.clone().asInstanceOf[DoubleRegressionProblem]
        copy._output = this._output.clone().asInstanceOf[DoubleData]
        copy
    }

    override def evaluateTestCase(testCase: RegressionCase, state: EvolutionState, thread: Int): Double = {
        // this is important to mark that X list is available for evaluations in problems
        _x = testCase

        // pass the output as reference down the tree, the result will be in the output variable
        _gpInd.trees(0).child.eval(state, thread, _output, stack, _gpInd, this)
        _output.y
    }

    override def evaluate(state: EvolutionState, ind: Individual, subpopulation: Int, thread: Int): Unit = {

        // do not bother reevaluating
        if (ind.evaluated) {
            return
        }

        _gpInd = ind.asInstanceOf[GPIndividual]
        evaluateTestCases(state, ind, thread)
        stack.reset()
    }
}