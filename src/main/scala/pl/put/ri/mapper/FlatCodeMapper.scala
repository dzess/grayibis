package pl.put.ri.mapper

import org.spiderland.Psh.Program
import org.spiderland.Psh.Interpreter
import org.slf4j.LoggerFactory

/**
 * First attempt to create proper code mapper for Psh interpreter
 */
class FlatCodeMapper extends ICodeMapper {

    private val logger = LoggerFactory.getLogger(classOf[FlatCodeMapper])

    private var cached: Boolean = false
    private var instructions: Seq[String] = null

    def setInterpreter(interpreter: Interpreter) = {

        if (!cached) {
            val instructionString = interpreter.GetInstructionsString()

            val instructionSet = instructionString.split(" ").toSet
            // sort instruction set
            val sortedInstructionSeq = instructionSet.toSeq.sortWith((x, y) => x < y)

            // generate mapping between vectors and instruction set 
            // the index is the position on the genome ;)
            instructions = sortedInstructionSeq.toSeq

            // perform safe checking of few instructions 
            validate()

            cached = true
        }
    }

    def validate() = {
        // in instruction set there should no more validation at all, in ecj-psh
        // the NOP instruction is used as result of bad position of the other instructions
    }

    private def toInstruction(gene: Short): String = {

        // TODO: this code should be refactored into some other methods ?
        // this is fail safe distribution changing ability
        if (gene >= instructions.size) {
            val rank = gene % instructions.size
            return instructions(rank)
        }
        if (gene < 0) {
            val absGene = math.abs(gene)
            val rank = absGene % instructions.size
            return instructions(rank)
        }
        return instructions(gene)
    }

    override def toProgram(genome: Array[Short]): Program = {
        logger.debug("Mapping genome: {}", genome)
        val innerCode = genome.map(x => toInstruction(x)).mkString(" ")
        val programCode = "(%s)".format(innerCode)
        logger.debug("Maping program: {}", programCode)
        new Program(programCode)
    }

    override def toGeome(program: Program): Array[Short] = {
        // TODO: this code is left to be written
        throw new RuntimeException("not yet implemented")
    }
}