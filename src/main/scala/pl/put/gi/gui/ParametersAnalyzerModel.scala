package pl.put.gi.gui

import java.io.File
import java.util.Properties
import pl.put.gi.model.Prototype
import pl.put.gi.db.PostgreSQLHelper
import pl.put.gi.dao.impl.PrototypeDAO
import pl.put.gi.loaders.HierarchicalFileParametersLoader
import org.slf4j.LoggerFactory
import pl.put.gi.gui.table.EntryStates
import pl.put.gi.gui.table.EntryStates._
import pl.put.gi.dao.ecj.impl.ECJPrototypeDAO
import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.settings.modules.DatabaseAccessObjectModule
import pl.put.gi.settings.InjectionManager
import pl.put.gi.settings.modules.HarvestersModule
import pl.put.gi.dao.ecj.IECJPrototypeDAO
import pl.put.gi.running.ecj.IStatisticsHarvester
import pl.put.gi.conf.PropertiesConfigurationReader
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.conf.DatabaseLocationReader
import pl.put.gi.util.scala.FileUtils.using
import java.io.OutputStreamWriter
import java.io.FileOutputStream
import pl.put.gi.exporters.FlatFileParametersExporter
import com.google.inject.util.Modules
import pl.put.gi.running.ecj.harvesters.NullStatisticsHarvester

/**
 * Helper for displaying proper values in ListView. Maybe overloading the renderer was
 * not so bad option
 */
case class PrototypeView(prototype: ECJPrototype) {
    override def toString(): String = {
        "%-25s".format(prototype.key)
    }
}

/**
 * Logic behind the editing the Prototype.
 */
class ParametersAnalyzerModel {

    private val logger = LoggerFactory.getLogger(classOf[ParametersAnalyzerModel])
    private val databaseReader = new DatabaseLocationReader()

    val resource = "database.properties"

    var dbHelper = databaseReader.load(resource)
    var dbModule = new DatabaseAccessObjectModule(dbHelper)
    val harvesterModule = new HarvestersModule()

    // note that not all modules are needed
    InjectionManager.createInjector(harvesterModule, dbModule)

    var prototypeDAO = InjectionManager.injector.getInstance(classOf[IECJPrototypeDAO])

    def reconnectToDatabase(jdbc: String) = {

        // TODO: this creation somehow should be informed that this code is not perfect for changing driver
        dbHelper = new PostgreSQLHelper(jdbc)

        // replace the injection manager 
        val modules = Modules.`override`(dbModule).`with`(new DatabaseAccessObjectModule(dbHelper))
        val inj = InjectionManager.createInjector(modules)

        // replace old dao system
        prototypeDAO = InjectionManager.injector.getInstance(classOf[IECJPrototypeDAO])
    }

    private var _currentPrototype: ECJPrototype = null
    private var _currentEntry: (String, String) = null
    private var _currentOption: (String, String) = null

    // TODO: this code can be refactored to be more generic
    def currentEntry = _currentEntry
    def currentEntry_=(other: (String, String)) {
        if (other == null) {
            throw new IllegalArgumentException("cannot select not existing entry")
        }
        logger.debug("Entry Set: {}", other)
        _currentEntry = other
    }

    def currentOption = _currentOption
    def currentOption_=(other: (String, String)) {
        if (other == null) {
            throw new IllegalArgumentException("cannot select not existing option")
        }
        logger.debug("Option Set: {}", other)
        _currentOption = other
    }

    def currentPrototype = _currentPrototype
    def currentPrototype_=(other: ECJPrototype) {
        if (other == null) {
            throw new IllegalArgumentException("cannot select not existing prototype")
        }
        logger.debug("Prototype Set: {}", other)
        _currentPrototype = other
    }

    def getPrototypeName(): String = {
        if (currentPrototype == null) {
            "No Prototype Selected"
        } else {
            "%s".format(currentPrototype.key)
        }
    }

    def getPrototypeDesc(): String = {
        if (currentPrototype == null) {
            "No Prototype Selected"
        } else {
            currentPrototype.description.getOrElse("")
        }
    }

    def getPrototypeParameters(): Seq[(String, String, EntryState)] = {
        val parametersMap = currentPrototype.parameters
        val neutral: EntryState = EntryStates.neutral
        parametersMap.map(x => (x._1, x._2, neutral)).toSeq
    }

    def getPrototypeOptions(): Seq[(String, String, EntryState)] = {

        // this job is far more difficult to do (now only harvesters are supported)
        val h = currentPrototype.harvesters
        val neutral: EntryState = EntryStates.neutral
        h.map(x => ("Harvester:%d".format(h.indexOf(x)), x.getClass.getName, neutral)).toSeq
    }

    def removeEntry() = {
        logger.debug("Droppping parameter: {}", currentEntry)
        _currentEntry = null
    }

    def removeOption() = {
        logger.debug("Droppping parameter: {}", currentEntry)
        _currentOption = null
    }

    def getData(): Seq[PrototypeView] = {
        prototypeDAO.getPrototypes.map(x => PrototypeView(x)).toSeq
    }

    private def parseHarvesters(input: Map[String, String]): Seq[IStatisticsHarvester] = {
        val harvesterOptions = input.filter(x => x._1.startsWith("Harvester:"))

        def toInstance(name: String): IStatisticsHarvester = {
            val cls = Class.forName(name)
            InjectionManager.injector.getInstance(cls).asInstanceOf[IStatisticsHarvester]
        }
        harvesterOptions.map(x => toInstance(x._2)).toSeq
    }

    def saveExisting(key: String, description: String, map: Map[String, String], options: Map[String, String]) = {

        currentPrototype.harvesters = parseHarvesters(options)
        currentPrototype.parameters = map
        currentPrototype.key = key
        if (description != null && (!description.isEmpty)) {
            currentPrototype.description = Some(description)
        } else {
            currentPrototype.description = None
        }

        prototypeDAO.save(currentPrototype)
    }

    def loadPrototypeFromFile(file: File) = {
        val loader = new HierarchicalFileParametersLoader(file.getAbsolutePath)
        val properties = loader.loadProperties()

        val prototype = new ECJPrototype()
        prototype.key = "loaded"
        prototype.parameters = properties
        prototype.harvesters = Seq(new NullStatisticsHarvester())
        currentPrototype = prototype
    }

    def exportToFile(file: File) = {
        val exportEngine = new FlatFileParametersExporter()

        using(new FileOutputStream(file))(stream => {
            exportEngine.export(currentPrototype, stream)
        })
    }

    def clonePrototype() = {
        if (currentPrototype != null) {
            val key = currentPrototype.key + ".clone"
            val description = Some(currentPrototype.description.getOrElse("") + ".clone")
            val parameters: Map[String, String] = currentPrototype.parameters.toMap
            val harvesters = currentPrototype.harvesters.toSeq

            val newPrototype = new ECJPrototype()
            newPrototype.key = key
            newPrototype.description = description

            // NOTE: add here any attributes that should be added in future
            newPrototype.parameters = parameters
            newPrototype.harvesters = harvesters

            currentPrototype = newPrototype
        } else {
            val newPrototype = new ECJPrototype()
            newPrototype.key = "empty.clone"
            newPrototype.description = Some("empty.clone")

            // no other attributes are being cloned
            currentPrototype = newPrototype
        }
    }
}