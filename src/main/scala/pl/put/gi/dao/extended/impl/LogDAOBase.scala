package pl.put.gi.dao.extended.impl

import org.slf4j.LoggerFactory
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.dao.impl.BaseDAO
import pl.put.gi.dao.extended.IMessageLogDAO
import pl.put.gi.model.extended.MessageLog
import pl.put.gi.dao.ILogDAO
import pl.put.gi.dao.impl.LogDAO
import pl.put.gi.model.Log
import org.scalaquery.ql.basic.BasicTable
import pl.put.gi.model.util.IMapable
import org.scalaquery.ql.basic.BasicDriver.Implicit._
import org.scalaquery.session.Database.threadLocalSession
import pl.put.gi.model.util.IdTable

abstract class LogDAOBase[Tuple, Model <: Log](
    dbHelper: IDatabaseHelper,
    protected val tableObject: IdTable[Tuple],
    protected val mappable: IMapable[Tuple, Model]) extends BaseDAO(dbHelper) {

    private val logger = LoggerFactory.getLogger(classOf[LogDAOBase[Tuple, Model]])

    // for foreign relation setting
    protected val logDAO: ILogDAO = new LogDAO(dbHelper)

    // this class uses only one query template
    private val getAllQuery = this.generateAllQuery(tableObject)

    def addLogBase(log: Model) = {
        logDAO.addLog(log)

        this.db.withSession {
            val tuple = mappable.fromModel(log)
            logger.debug("Creating object in DB: {}", tuple)
            tableObject.insert(tuple)
        }
    }

    def getAllBase(): List[Model] = {

        // TODO: introduce manifest for debugging purposes
        logger.debug("Quering DB for logs: ")
        var logs: List[Model] = null

        this.db.withSession {
            // query normally
            val resultList = getAllQuery.list
            logger.debug("Quering the DB for FitnessLgo - tuples {}", resultList)
            logs = resultList.map(x => mappable.toModel(x)).toList
        }

        logger.debug("Filling the missing properties from the upper class")
        logs.foreach(l => {

            // find log values using logDAO
            val superL = logDAO.getOne(l.id)

            // manual remapping (of this elements)
            l.timeStamp = superL.timeStamp
            l.run = superL.run
        })

        return logs
    }

    def dropTable() = this.dropTableBase(tableObject)
    def createTable() = this.recreateTableBase(tableObject)
    def isTableCreated() = this.isTableCreatedBase(tableObject)
}