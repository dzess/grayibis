package pl.put.gi.running.job

import pl.put.gi.Configuration
import org.slf4j.LoggerFactory

/**
 * Executes the runs sequentially. Uses the configuration as a way
 * to define what should be done.
 */
class JobQueue(val configuration: Configuration) extends IJob {

    private val logger = LoggerFactory.getLogger(classOf[JobQueue])

    // we do not want any changes during job run
    configuration.freeze()

    override def execute() = {
        logger.info("Starting Job queue")

        // get all the runs that are to be done
        val runConfs = configuration.runConfigurationProvider.generate()

        for (runConf <- runConfs) {
            logger.info("Generating run {}", runConf.print())

            // get the runner for this thing
            val runner = configuration.runProvider.produce(runConf)

            logger.info("Executing run {}", runner)

            runner.initialize()

            try {
                runner.run()
            } catch {
                case e: Exception => {
                    logger.warn("There was an exception during run", e)
                }
            }

            try {
                runner.cleanup()
            } catch {
                case e: Exception => {
                    logger.warn("There was an exception during cleanup", e)
                }
            }

        }

        logger.info("Finishing Job queue")
    }
}