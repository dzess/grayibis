package pl.put.gi.util.security

import java.io.FileDescriptor
import java.net.InetAddress

/**
 * Security manager that allows everything.
 */
class NullSecurityManager extends SecurityManager {
    override def checkCreateClassLoader() = {}
    override def checkAccess(g: Thread) = {}
    override def checkAccess(g: ThreadGroup) = {}
    override def checkExit(status: Int) = {}
    override def checkExec(cmd: String) = {}
    override def checkLink(lib: String) = {}
    override def checkRead(fd: FileDescriptor) = {}
    override def checkRead(file: String) = {}
    override def checkRead(file: String, context: Object) = {}
    override def checkWrite(fd: FileDescriptor) = {}
    override def checkWrite(file: String) = {}
    override def checkDelete(file: String) = {}
    override def checkConnect(host: String, port: Int) = {}
    override def checkConnect(host: String, port: Int, context: Object) = {}
    override def checkListen(port: Int) = {}
    override def checkAccept(host: String, port: Int) = {}
    override def checkMulticast(maddr: InetAddress) = {}
    override def checkMulticast(maddr: InetAddress, ttl: Byte) = {}
    override def checkPropertiesAccess() = {}
    override def checkPropertyAccess(key: String) = {}
    override def checkTopLevelWindow(window: Object): Boolean = { return true; }
    override def checkPrintJobAccess() = {}
    override def checkSystemClipboardAccess() = {}
    override def checkAwtEventQueueAccess() = {}
    override def checkPackageAccess(pkg: String) = {}
    override def checkPackageDefinition(pkg: String) = {}
    override def checkSetFactory() = {}
    override def checkMemberAccess(clazz: Class[_], which: Int) = {}
    override def checkSecurityAccess(provider: String) = {}
}