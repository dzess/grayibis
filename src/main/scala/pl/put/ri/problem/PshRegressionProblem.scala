package pl.put.ri.problem

import ec.Problem
import ec.simple.SimpleProblemForm
import pl.put.ri.mapper.ICodeMapper
import pl.put.ri.mapper.FlatCodeMapper
import ec.EvolutionState
import ec.util.Parameter
import ec.Individual
import org.ecj.psh.PshEvaluator
import ec.vector.ShortVectorIndividual
import ec.gp.koza.KozaFitness
import org.spiderland.Psh.Interpreter
import org.spiderland.Psh.Program
import pl.put.regression.RegressionProblem
import pl.put.regression.RegressionCase
import org.ecj.psh.PshEvolutionState

/**
 * Base class for performing the regression of the code
 */
abstract class PshRegressionProblem extends Problem
    with SimpleProblemForm with RegressionProblem {

    // TODO: some options need to be injected via setup method
    val P_REPEATINTSTACK = "repeat-stack"
    val P_MAPPER = "mapper"

    private var mapper: ICodeMapper = null

    // there protected values are used by inherited classes
    protected var repeatInStack: Int = 10
    
    protected var interpreter: Interpreter = null
    protected var program: Program = null

    override def setup(state: EvolutionState, base: Parameter) = {
        super.setup(state, base)

        // use the method defined in regression problem to load the test cases
        loadTestCases(state, base)

        // read code mapper 
        mapper = new FlatCodeMapper()
        repeatInStack = state.parameters.getIntWithDefault(base.push(P_REPEATINTSTACK), null, 1);
        state.output.warning("Repeat in stacks %d".format(repeatInStack))

        // safe check
        state.output.exitIfErrors()
    }

    override def evaluate(state: EvolutionState, ind: Individual, subpopulation: Int, thread: Int): Unit = {

        if (ind.evaluated) {
            return
        }

        // get interpreter
        val evaluator = state.evaluator.asInstanceOf[PshEvaluator]
        interpreter = state.asInstanceOf[PshEvolutionState].interpreter(thread)

        // set interpreter for current mapper
        mapper.setInterpreter(interpreter)

        // get program (using mapper ? or some better one ?)
        val shortIndividual = ind.asInstanceOf[ShortVectorIndividual]
        program = mapper.toProgram(shortIndividual.genome)

        // go with the code from regression problem
        evaluateTestCases(state, ind, thread)
    }
}