package pl.put.gi.dao

import pl.put.gi.model.Run

abstract trait IRunDAO extends IDAO {
    
    def getRuns() : List[Run]
    def getRun(id : Long) : Run
    
    def save(run : Run)
    
}