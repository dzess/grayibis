package pl.put.gi.db.util

import org.slf4j.LoggerFactory
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.dao.IDAOManager
import com.google.inject.Inject

/**
 * Aggregate class tool used for performing various operations on Database. Used as part of GrayIbis command line interface on the basis
 * maybe latter will allow some other options. Heavily dependent on Guice.
 */
class DatabaseUtil @Inject() (private val dbHelper: IDatabaseHelper,
    private val daoManager: IDAOManager) {

    private val logger = LoggerFactory.getLogger(classOf[DatabaseUtil])

    def createSchema() = {

        logger.info("Creating Schema - information")
        logger.info("JDBC: {}", dbHelper.currentURL)
        logger.info("Driver: {}", dbHelper.driver)

        logger.info("Creating Schema - started")
        daoManager.safeCreateTables()
        logger.info("Creating Schema - finsihed")
    }

    def dropSchema() = {
        logger.error("This function has not been implemented yet")
        throw new IllegalStateException("not yet implemented")
    }

    def migratePrototypes() = {
        // create concept of migrating already stored prototypes from one database to another - this is advanced feature
        // which is not needed by now, yet thereis a space for it
        logger.error("This function has not been implemented yet")
        throw new IllegalStateException("not yet implemented")
    }
}