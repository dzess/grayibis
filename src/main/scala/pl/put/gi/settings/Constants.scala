package pl.put.gi.settings

/**
 * Holds the most important constants from the outside
 * the code point of view.
 */
object Constants {

    /**
     * Makes the Gray Ibis throw exception when during the run prototype will not be found. If false then
     * NullRunner will be provided.
     */
    val THROW_UPON_MISSING_PROTOTYPE = false

    /**
     * Describes the name of the table where adjusted koza fitness will be stored.
     */
    val KOZA_FITNESS_TABLE = "koza-adjusted"

    /**
     * Describes the name of the table where hits from koza fitness will be stored.
     */
    val KOZA_HITS_FITNESS_TABLE = "koza-hits"
}