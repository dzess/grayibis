﻿-- basic max query about the hiff
select
	p.key, r.id, max(fl.fitness)
from 
	"runs" r join "prototypes" p on r."experimentId" = p."id"
	join "logs" l on l."runId" = r."id"
	join "fitness-log" fl on fl."logId" = l."id"
where
	p.key like 'Hiff_%'
and
	r.state = 'done'
group by r.id, p.key;


-- averages of the max fitness achieved
create or replace view v_max_fitness as 
select 
	inside.key as key,
	weigth.field as xField,
	exchange.field as yField,
	avg(inside.maxFitness),
	stddev(inside.maxFitness)
from
	(
	select
		p.key as key,
		r.id,
		max(fl.fitness) as maxFitness
	from 
		"runs" r join "prototypes" p on r."experimentId" = p."id"
		join "logs" l on l."runId" = r."id"
		join "fitness-log" fl on fl."logId" = l."id"
	where
		p.key like 'Hiff_%'
	and 
		r.state = 'done'
	group by r.id, p.key
	) inside
join 
	(
	select * from extract_field_numeric('breed\.updater\.weight')
	) weigth
on weigth.key = inside.key
join 
	(
	select * from extract_field_numeric('breed\.exchange-ratio')
	) exchange
on exchange.key = inside.key
group by inside.key, weigth.field, exchange.field;

-- get the max and min
select * from v_max_fitness where avg = (select max(avg) from v_max_fitness);
select * from v_max_fitness where avg = (select min(avg) from v_max_fitness);


select * from extract_field_numeric('breed\.updater\.weight');
select * from extract_field_numeric('breed\.exchange-ratio');