In the configuration section of Gray Ibis the ECJ parameter files with the examples
of the configuration are being stored.

The configuration section in this test resources is divided by the type of the problem.
For now there are few simple benchmarks implemented:
	- one max problem
	- sum problem
	- hiff problem 
	- hiff2 problem (specialized version of hiff where connections are swapped)
	- int symbolic regression problem -- mostly used as tryouts, for now folder is deprecated
	- float symbolic regression problem  -- mostly has only configuration of runs
	
At the top level folder there is parent.properties file which has all the general configuration
done for all of the problems.