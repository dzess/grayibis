package pl.put.gi.model.util

/**
 * Marks the class as being able to changing the
 * model of type M into tuples. Just used to provided aggregated
 * way of doing the mapping for all the data object in Gray Ibis.
 *
 * Beware that this changing of model is only done at the
 * level of simple fields not foreign keys. This type of mapper
 * is incapable of fetching missing relations to provide full blown model.
 *
 */
abstract trait IMapable[T, M] {

    /**
     * Changes the passed Tuple from ScalaQuery into resulted model.
     */
    def toModel(inputTuple: T): M

    /**
     * Changes the passed model into ScalaQuery Tuple.
     */
    def fromModel(inputModel: M): T

}