package pl.put.gi.db
import pl.put.gi.util.IFreezable
import pl.put.gi.util.IValidable

/**
 * Specifies the interface for all the DB helpers that should provide.
 * If not such connection can be provided then throw exception [[IllegalStateException]]
 */
abstract trait IDatabaseHelper extends IFreezable with IValidable {

    /**
     * Returns the class of the driver to DB that should be used.
     */
    def driver: String

    /**
     * Returns the URL which is too be used by the database
     */
    def currentURL: String

}