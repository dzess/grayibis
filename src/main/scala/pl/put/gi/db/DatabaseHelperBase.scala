package pl.put.gi.db

/**
 * Base class for [[DBHelper]] providing the already defined
 * code for validation and freeze.
 *
 * Subclasses have to only assign values to the properties.
 */
class DatabaseHelperBase extends IDatabaseHelper {

    private var _driver: String = null
    private var _currentURL: String = null

    // set accessory for driver property
    def driver_=(input: String) {
        DatabaseHelperBase.this.validateString(input)
        DatabaseHelperBase.this.assertNotFrozen()
        _driver = input
    }

    // public get accessory for driver property
    override def driver: String = {
        return _driver
    }

    // set accessory for driver property
    def currentURL_=(input: String) {
        DatabaseHelperBase.this.validateString(input)
        DatabaseHelperBase.this.assertNotFrozen()
        _currentURL = input
    }

    // public get accessory for URL property
    override def currentURL: String = {
        return _currentURL
    }

    override def validate() = {
        validateString(_driver)
        validateString(_currentURL)
    }

    private def validateString(input: String) = {
        if (input == null) {
            throw new IllegalArgumentException("Null is not proper value")
        }

        if (input.isEmpty()) {
            throw new IllegalArgumentException("Empty string is not proper value")
        }
    }

    override def freeze() = {
        // assert the validity of the currentURL 
        validate()
        
        super.freeze()
    }
}