package pl.put.gi.gui.sequences

import pl.put.gi.gui.sequences.generators.CartesianSequenceGenerator
import pl.put.gi.gui.sequences.generators.StepSequenceGenerator
import pl.put.gi.gui.sequences.generators.ValuesSequenceGenerator
import pl.put.gi.gui.sequences.generators.DummySequenceGenerator
import pl.put.gi.gui.sequences.cloners.SimplePrototypeGenerator

/**
 * Factory object for all ISequenceGenerators
 */
object SequenceGenerator {

    private def getPrototypeGenerator(): IPrototypeGenerator = {
        new SimplePrototypeGenerator()
    }

    def getAll: Seq[ISequenceGenerator] = {
        val others = Seq(
            new StepSequenceGenerator(getPrototypeGenerator),
            new ValuesSequenceGenerator(getPrototypeGenerator),
            new DummySequenceGenerator())

        val composite = new CartesianSequenceGenerator(others)

        return others ++ Seq(composite)
    }
}