package pl.put.gi.use.onemax

import org.slf4j.LoggerFactory
import pl.put.gi.use.ExampleRunnerBase

/**
 * Example that shows how to use Gray Ibis
 * codes.
 *
 * This class shows the utility to put prototype data
 * into database, how to configure the GrayIbis and how to
 * run sample tests.
 *
 * This class uses the EDA algorithms to run the data especially the
 * UMDA algorithm.
 *
 */
object OneMaxProblemRunnerUMDA extends App with ExampleRunnerBase {

    private val logger = LoggerFactory.getLogger(this.getClass)

    logger.info("Starting the data preparation")

    // prepare the needed data
    experimentKey = "OneMax_2"
    experimentDescription = "One Max Problem with EDA-UMDA solution"
    propertiesPath = "src/test/resources/configurations/one-max/one-max-umda.params"

    preparePrototype()

    logger.info("Starting the experiment preparation")
    prepareRun()
    conf.runDescription = "One Max Run EDA - UMDA"

    logger.info("Starting the experiment")
    run()

}