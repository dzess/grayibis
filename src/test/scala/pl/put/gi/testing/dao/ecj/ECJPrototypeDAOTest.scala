package pl.put.gi.testing.dao.ecj

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import pl.put.gi.dao.ecj.impl.ECJPrototypeDAO
import pl.put.gi.db.TestDatabaseHelper
import pl.put.gi.model.ecj.ECJPrototype
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfterEach
import pl.put.gi.dao.impl.PrototypeDAO

@RunWith(classOf[JUnitRunner])
class ECJPrototypeDAOTest extends FunSuite with BeforeAndAfterEach {

    private var prototypeDAO: PrototypeDAO = null
    private var dao: ECJPrototypeDAO = null
    private val dbHelper = new TestDatabaseHelper()

    private val key = "sample_key"
    private val description = "sample description"
    private val params = Map("a" -> "x", "b" -> "y")
    private val harvesterMap = Map("h1" -> "f1")

    override def beforeEach() = {
        prototypeDAO = new PrototypeDAO(dbHelper)
        dao = new ECJPrototypeDAO(dbHelper)

        // clear all the database of prototypes
        prototypeDAO.createTable()
        dao.createTable()

        // insert one prototype there
        val p: ECJPrototype = new ECJPrototype()
        p.key = key
        p.description = Some(description)
        p.parameters = params
        p.options = harvesterMap

        dao.save(p)
    }

    override def afterEach() = {
        dao.dropTable()
        prototypeDAO.dropTable()
    }

    def assertPrototype(result: ECJPrototype) = {
        assert(result.key == key)
        assert(result.description.get == description)

        assert(result.parameters.size === 2)
        assert(result.parameters.contains("a"))
        assert(result.parameters.contains("b"))
        assert(result.parameters.get("a").get === "x")
        assert(result.parameters.get("b").get === "y")
        assert(result.options.size === 1)
        assert(result.options.get("h1").get === "f1")
    }

    test("retriving all prototypes throws no errors") {
        val result = dao.getPrototypes()

        assert(result.size == 1)
    }

    test("retriving single prototype throw no erorr") {
        val result = dao.getPrototypeByKey(key)
        this.assertPrototype(result)
    }

    test("retriving single prototype throws no error") {
        val result = dao.getPrototype(0)
        this.assertPrototype(result)
    }

    test("retriving the inproper prototype results in null") {
        val result = dao.getPrototype(-1)
        assert(result == null)

    }

    test("updating the same prototype does not result in sensless update") {
        val prototype = dao.getPrototypes().head
        dao.save(prototype)

        // actually no save should be performed (but how to check it ?)
    }

    test("updating the protype on all parameters") {
        val result = dao.getPrototypes()
        this.assertPrototype(result.head)

        // provide some changes
        val proto = result.head
        proto.key = "super change"
        proto.description = Some("better description")
        proto.parameters = Map("k" -> "j")

        // run the update 
        dao.save(proto)

        val p = dao.getPrototypes().head
        assert(p.key === "super change")
        assert(p.description === Some("better description"))
        assert(p.parameters === Map("k" -> "j"))
    }

}