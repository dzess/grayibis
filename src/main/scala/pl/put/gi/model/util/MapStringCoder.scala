package pl.put.gi.model.util

import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.net.URLDecoder

/**
 * Encodes / Decodes the [String,String] map into single
 * string.
 *
 * Remark: Map in the way the scala collection is represented
 */
object MapStringCoder {

    private def encode(in: String): String = {
        if (in != null) {
            return URLEncoder.encode(in, "UTF-8")
        } else {
            return ""
        }
    }

    private def decode(in: String): String = {
        return URLDecoder.decode(in, "UTF-8")
    }

    def mapToString(map: Map[String, String]): String = {

        if (map == null) {
            throw new IllegalArgumentException("The map cannot be null")
        }

        val stringBuilder = new StringBuilder();
        for (key <- map.keySet) {

            if (stringBuilder.length() > 0) {
                stringBuilder.append("&");
            }
            val value = map.get(key).get;
            try {
                stringBuilder.append(this.encode(key));
                stringBuilder.append("=");
                stringBuilder.append(this.encode(value));
            } catch {
                case e: UnsupportedEncodingException => throw new RuntimeException("This method requires UTF-8 encoding support", e);
            }
        }
        return stringBuilder.toString()
    }

    def stringToMap(input: String): Map[String, String] = {

        if (input == null) {
            throw new IllegalArgumentException("The input string cannot be null")
        }

        if (input.isEmpty()) {
            return Map()
        }

        var map: Map[String, String] = Map()

        val nameValuePairs = input.split("&")
        val length = nameValuePairs.length
        for (index <- 0 until length) {
            val nameValue = nameValuePairs(index).split("=")
            try {

                val key = this.decode(nameValue(0))
                val value = // this is a case when the encoded is empty string
                    if (nameValue.size == 1) {
                        ""
                    } else {
                        this.decode(nameValue(1))
                    }

                // instead of map put ;)
                map = map + (key -> value)
            } catch {
                case e: UnsupportedEncodingException => throw new RuntimeException("This method requires UTF-8 encoding support", e)
            }
        }
        return map
    }
}