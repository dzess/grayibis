package pl.put.gi.running.universal

import pl.put.gi.running.IRunner
import org.slf4j.LoggerFactory

/**
 * The Nullable pattern implementation in context of IRunner.
 */
class NullRunner extends IRunner {

    private val logger = LoggerFactory.getLogger(classOf[NullRunner])

    override def initialize() = {
        logger.debug("Method 'initialize' does nothing")
    }

    override def run() = {
        logger.debug("Method 'run' does nothing")
    }

    override def cleanup() = {
        logger.debug("Method 'cleanup' does nothing")
    }

}