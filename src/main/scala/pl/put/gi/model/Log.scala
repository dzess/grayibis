package pl.put.gi.model

import java.sql.Timestamp
import org.scalaquery.ql.TypeMapper.LongTypeMapper
import org.scalaquery.ql.TypeMapper.StringTypeMapper
import org.scalaquery.ql.TypeMapper.TimestampTypeMapper
import org.scalaquery.ql.basic.BasicTable
import pl.put.gi.model.util.IMapable
import pl.put.gi.settings.InnerConstants
import org.scalaquery.ql.NamedColumn
import pl.put.gi.model.util.IdTable

/*
 * Models single log entry in the DB. The main idea behind this class is to be
 * used as the abstract base for all the logs used in database.
 * 
 * TODO: safe implementation should be extracted to some utility classes
 */
class Log(
    private var _timeStamp: Timestamp,
    private var _run: Run = null,
    private var _id: Long = InnerConstants.undefinedIdentifier) {

    // safe implementation of timestamp
    def timeStamp: Timestamp = _timeStamp
    def timeStamp_=(other: Timestamp) {
        if (other == null) {
            throw new IllegalArgumentException("timestamp cannot be null")
        }
        _timeStamp = other
    }

    // safe implementation of run property
    def run: Run = _run
    def run_=(other: Run) {
        if (other == null) {
            throw new IllegalArgumentException("run cannot be null")
        }
        _run = other
    }

    def id: Long = _id
    def id_=(other: Long) {
        if (other == InnerConstants.undefinedIdentifier) {
            throw new IllegalArgumentException("id can be undefined only for creation")
        }
        _id = other
    }
}

/**
 * ScalaQuery table mapping object for the Log
 */
object Log extends IdTable[(Long, Long, Timestamp)]("logs")
    with IMapable[(Long, Long, Timestamp), Log] {

    override def id = column[Long]("id", O PrimaryKey)

    def runId = column[Long]("runId", O NotNull)
    def timestamp = column[Timestamp]("timestamp", O NotNull)

    def * = id ~ runId ~ timestamp

    // foreign keys
    def logsFK = foreignKey("logsFK", runId, Run)(_.id)

    override def toModel(inputTuple: (Long, Long, Timestamp)): Log = {
        val time = inputTuple._3

        // no run to be injected (this code should be injected by DAO class)
        // which has control over lazy or eager loading
        val log = new Log(time)

        // the id should be passed
        log.id = inputTuple._1
        return log
    }

    override def fromModel(inputModel: Log): (Long, Long, Timestamp) = {
        (inputModel.id, InnerConstants.undefinedIdentifier, inputModel.timeStamp)
    }
}