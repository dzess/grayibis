package pl.put.gi.testing.dao.extended

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import pl.put.gi.testing.dao.utils.LogDAOTestBase
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfterEach
import pl.put.gi.dao.extended.impl.MessageLogDAO
import pl.put.gi.dao.impl.LogDAO
import pl.put.gi.model.extended.MessageLog

@RunWith(classOf[JUnitRunner])
class MessageLogDAOTest extends LogDAOTestBase with FunSuite with BeforeAndAfterEach {

    protected var logDAO: LogDAO = null
    protected var dao: MessageLogDAO = null
    val message = "sample message"

    override def beforeEach() = {
        super.beforeEachMethod()

        logDAO = new LogDAO(dbHelper)
        logDAO.createTable()

        dao = new MessageLogDAO(dbHelper)
        dao.createTable()

        this.prepareRun()

        // set up one fitness log
        val log = new MessageLog(message, timestamp, run)
        dao.addLog(log)
    }

    override def afterEach() = {
        dao.dropTable()
        logDAO.dropTable()

        super.afterEachMethod()
    }

    test("retriving all message logs") {
        val result = dao.getAll()
        assert(result.size == 1)

        val fLog = result.head
        assert(fLog.message == message)
    }

    test("retriving all logs checking eager loading of logs") {
        val result = dao.getAll()
        val log = result.head

        assert(log.run != null)
        assert(log.run.description === this.runDescription)
        assert(log.run.prototype != null)
        assert(log.run.prototype.key === this.prototypeKey)
    }
}