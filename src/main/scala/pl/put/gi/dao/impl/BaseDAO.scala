package pl.put.gi.dao.impl

import org.scalaquery.meta.MTable
import org.scalaquery.ql.ColumnOps.CountDistinct
import org.scalaquery.ql.basic.BasicDriver.Implicit._
import org.scalaquery.ql.basic.BasicTable
import org.scalaquery.ql.Query
import org.scalaquery.session.Database
import org.scalaquery.session.Session
import org.slf4j.LoggerFactory
import pl.put.gi.dao.IDAO
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.settings.InnerConstants
import org.scalaquery.ql.Column
import org.scalaquery.ql.basic.BasicQueryTemplate
import org.scalaquery.ql.Parameters
import org.scalaquery.ql.NamedColumn
import pl.put.gi.model.util.IdTable

/**
 * Base class for DAO operations on tables.
 *
 * Supports only basic, general functionalities for database
 * operations.
 *
 * Methods with name generate does not connect to the DB.
 * Methods with name get does connect to the DB (using implicit Session) or creating their own
 */
abstract class BaseDAO(dbHelper: IDatabaseHelper) {

    private val logger = LoggerFactory.getLogger(classOf[BaseDAO])

    logger.debug("Initializing the Database Context '{}'", this)
    logger.debug("Driver: {}", dbHelper.driver)
    logger.debug("JDBC: {}", dbHelper.currentURL)
    protected val db = Database.forURL(dbHelper.currentURL, driver = dbHelper.driver)

    protected def isObjectInDB[T <: { def id: Long }](updatedObject: T, retrivalFunction: Long => T): Boolean = {

        if (updatedObject.id != InnerConstants.undefinedIdentifier) {
            // check if the object is in the database (check by id)
            val inBase = retrivalFunction(updatedObject.id)
            if (inBase != null) {
                return true
            } else {
                return false
            }
        }
        return false
    }

    protected def generateOneQuery[T](tableObject: IdTable[T]): BasicQueryTemplate[Long, T] = {
        for {
            searchId <- Parameters[Long];
            x <- tableObject if x.id === searchId
        } yield x
    }

    protected def generateMaxQuery[T](tableObject: IdTable[T]): Query[Column[Option[Long]], Option[Long]] = {
        for { x <- tableObject } yield x.id.max
    }

    protected def generateAllQuery[T](tableObject: IdTable[T]): Query[IdTable[T], T] = {
        for { x <- tableObject } yield x
    }

    protected def getNewId(maxQuery: Query[Column[Option[Long]], Option[Long]])(implicit session: Session): Long = {
        val count = maxQuery.first(session)
        count.getOrElse(InnerConstants.undefinedIdentifier) + 1
    }

    protected def getTableMap(implicit session: Session): Map[String, MTable] = {
        val tableList = MTable.getTables.list()(session);
        val tableMap = tableList.map { t => (t.name.name, t) }.toMap;
        tableMap;
    }

    protected def isTableCreatedBase(tableObject: BasicTable[_]): Boolean = {
        val name = tableObject.tableName
        var result = false

        val functionInSession = (session: Session) => {
            result = this.getTableMap(session).contains(name)
        }

        db.withSession(functionInSession)
        return result
    }

    protected def recreateTableBase(tableObject: BasicTable[_]) = {

        if (tableObject == null) {
            throw new IllegalArgumentException("The passed tableObject cannot be null")
        }

        // drop tables is not mandatory
        this.dropTableBase(tableObject)

        val func = (s: Session) => tableObject.ddl.create(s)
        db.withSession(func)
    }

    protected def dropTableBase(tableObject: BasicTable[_]) = {
        if (tableObject == null) {
            throw new IllegalArgumentException("The passed tableObject cannot be null")
        }
        try {
            val func = (s: Session) => tableObject.ddl.drop(s)
            db.withSession(func)
        } catch {
            case e: Exception => {
                logger.warn("Drop tables failed")
            }
        }
    }

    protected def performEagerLoading[T, K](models: List[T],
        ids: List[Long],
        retriveFunc: (Long) => K,
        actionFunc: (T, K) => Unit): List[T] = {

        // this might be quite too big in future yet is nice caching done here
        var cached: Map[Long, K] = Map[Long, K]()

        val zipped = models.zip(ids)
        zipped.foreach(x => {
            val parentModel = x._1
            val childID = x._2

            def cachedRetrivalFunc(i: Long): K = {
                val r = retriveFunc(i)
                cached += (i -> r)
                r
            }

            val childModel = cached.getOrElse(childID, cachedRetrivalFunc(childID))
            actionFunc(parentModel, childModel)
        })
        return models
    }

}