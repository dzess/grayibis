package pl.put.gi.util
import org.slf4j.LoggerFactory
import java.lang.reflect.Field

/**
 * Marks the class as the one that can be
 * created once and after some milestone, cannot be changed anymore.
 */
abstract trait IFreezable {

    private var frozen: Boolean = false
    private val logger = LoggerFactory.getLogger(classOf[IFreezable])

    /**
     * Tries to make the instance marked with this trait frozen.
     * If instance is already frozen then IllegalStateException is
     * being thrown.
     */
    def freeze() = {
        if (frozen == true) {
            throw new IllegalStateException("The instance is already frozen")
        }
        frozen = true;

        logger.debug("Frozen the configuration {}", IFreezable.this.toString())
    }

    /**
     * Gets the value specifying if the instance has already been frozen.
     */
    def isFrozen(): Boolean = {
        frozen
    }

    /**
     * Asserts that the freezable item is not already frozen. If happens
     * to be frozen it is [[InvalidStateException]]
     */
    def assertNotFrozen() = {
        if (isFrozen()) {
            throw new IllegalStateException("The freezable item is already frozen" + this.toString())
        }
    }

    /**
     * Creates the safe setter function using some scala magic
     */
    protected def safeSetter[T](value: T)(setterFunction: T => Unit) = {
        this.assertNotFrozen()
        if (value == null) {
            throw new IllegalArgumentException("passed value cannot be null")
        }
        setterFunction(value)
    }
}