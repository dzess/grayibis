package pl.put.gi.db

/**
 * Helper class for Postgresql DB management. This class
 * has hard-coded driver for PostgreSQL DB. Just it.
 */
class PostgreSQLHelper(val connectionString: String) extends DatabaseHelperBase {

    // example of typical PostgreSQL JDBC connection string
    val typicalURL = "jdbc:postgresql://localhost/test?user=fred&password=secret&ssl=true"

    if (connectionString == null || connectionString.isEmpty) {
        // the null value is not the option
        throw new IllegalArgumentException("connection string cannot be null")
    }

    this.currentURL = connectionString
    this.driver = "org.postgresql.Driver"
}