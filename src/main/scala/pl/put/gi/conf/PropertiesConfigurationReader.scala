package pl.put.gi.conf

import java.io.InputStream
import pl.put.gi.Configuration
import java.io.FileInputStream
import java.util.Properties
import org.slf4j.LoggerFactory
import pl.put.gi.db.PostgreSQLHelper
import pl.put.gi.running.universal.RunnerConfigurationProvider
import pl.put.gi.running.ecj.ECJRunnerProvider
import pl.put.gi.db.MySQLHelper
import pl.put.gi.db.SQLiteHelper
import scala.collection.JavaConversions._
import scala.collection.mutable.Map
import java.util.IllegalFormatException
import pl.put.gi.db.IDatabaseHelper
import ch.qos.logback.classic.db.DBHelper
import pl.put.gi.settings.InjectionManager
import pl.put.gi.running.IRunnerConfigurationProvider
import pl.put.gi.running.IRunnerProvider

/**
 * Basic implementation of IConfigurationReader. Loads the configuration
 * from the stream which has to be InputFileStream. Then the loaded file
 * has to be java properties file.
 */
class PropertiesConfigurationReader extends IConfigurationReader {

    private val logger = LoggerFactory.getLogger(classOf[PropertiesConfigurationReader])

    // runner configuration provider part
    val runnerConfigurationClass = "configuration"

    // database part
    val databaseDriver = "database"
    val databaseAddress = "jdbc"
    val supportedDrivers = List(postgresqlDB, mysqlDB, sqliteDB)

    // inner list of supported databases
    private val postgresqlDB = "postgresql"
    private val mysqlDB = "mysql"
    private val sqliteDB = "sqlite"

    private def validateProperty(dbDriver: String, databaseDriver: String) = {
        if (dbDriver == null || dbDriver.isEmpty) {
            throw new IllegalArgumentException("missing property '" + databaseDriver + "'")
        }
    }

    def readDatabase(properties: Properties): IDatabaseHelper = {

        // read dbHelper
        val dbDriver = properties.getProperty(databaseDriver)
        val dbAdress = properties.getProperty(databaseAddress)

        // validate the properties file format (some fields are mandatory)
        this.validateProperty(dbDriver, databaseDriver)

        val dbHelper = dbDriver match {
            case "postgresql" => new PostgreSQLHelper(dbAdress)
            case "mysql" => new MySQLHelper(dbAdress)
            case "sqlite" => new SQLiteHelper(dbAdress)
            case _ => {
                throw new IllegalStateException("Could not recognize the database driver " + dbDriver)
            }
        }

        dbHelper

    }

    def loadDatabase(stream: InputStream): IDatabaseHelper = {
        val properties = new Properties()
        properties.load(stream)
        readDatabase(properties)
    }

    override def load(stream: InputStream): Configuration = {

        val properties = new Properties()

        properties.load(stream)

        val dbHelper = this.readDatabase(properties)

        // read run configuration provider
        val configurationClassName = properties.getProperty(runnerConfigurationClass)
        val configurationClass = Class.forName(configurationClassName)

        // this code up there works like a assertion for now - redesigning to allow
        // changing the current class would be too tiresome for now

        val mutableMap = Map[String, Int]()
        for (entry <- properties.entrySet()) {
            val key = entry.getKey().toString()

            // only the ones starting with 'map.' are valid things
            if (key.startsWith("map.")) {
                logger.debug("Found new possible map key {}", key)

                val value = entry.getValue().toString
                val valueNumber = value.toInt

                val foundKey = key.substring(4)
                logger.debug("Key found : {}", foundKey)

                mutableMap += (foundKey -> valueNumber)
            }
        }

        // create some assertion basing on the map
        if (mutableMap.size == 0) {
            throw new IllegalArgumentException("at least one entry in the map is needed")
        }

        logger.debug("Current Runner Provider Map: {}", mutableMap)
        val runConfigurationProvider = new RunnerConfigurationProvider(mutableMap.toMap)

        // make configuration
        val conf = Configuration.default
        conf.dbHelper = dbHelper
        conf.runConfigurationProvider = runConfigurationProvider
        conf.runProvider = InjectionManager.injector.getInstance(classOf[IRunnerProvider])

        logger.debug("Created configuration using properties file: {}", conf.print())

        return conf
    }
}