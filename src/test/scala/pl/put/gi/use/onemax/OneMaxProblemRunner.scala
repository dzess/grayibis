package pl.put.gi.use.onemax

import org.slf4j.LoggerFactory
import pl.put.gi.use.ExampleRunnerBase

/**
 * The most classic example of running the Gray Ibis code.
 *
 * This class shows the utility to put prototype data
 * into database, how to configure the GrayIbis and how to
 * run sample tests.
 */
object OneMaxProblemRunner extends App with ExampleRunnerBase {

    private val logger = LoggerFactory.getLogger(this.getClass)

    // data preparation - read from files and insert into
    // database using the provided tools
    logger.info("Starting the data preparation") 

    // prepare the needed data
    experimentKey = "OneMax_0"
    experimentDescription = "One Max Problem from ECJ Tutorial"
    propertiesPath = "src/test/resources/configurations/one-max/one-max-classic.params"
    preparePrototype()

    logger.info("Starting the experiment preparation")
    prepareRun()
    conf.runDescription = "One Max Run Classic"

    logger.info("Starting the experiment")
    run()
}