package pl.put.gi.testing.dao

import java.sql.Timestamp
import java.util.Date
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite
import pl.put.gi.db.PostgreSQLHelper
import pl.put.gi.model.Log
import pl.put.gi.model.Prototype
import pl.put.gi.model.Run
import pl.put.gi.dao.impl.LogDAO
import pl.put.gi.dao.impl.RunDAO
import pl.put.gi.dao.impl.PrototypeDAO
import pl.put.gi.testing.dao.utils.LogDAOTestBase

@RunWith(classOf[JUnitRunner])
class LogDAOTest extends LogDAOTestBase with FunSuite with BeforeAndAfterEach {

    protected var dao: LogDAO = null
    protected var log : Log = null

    override def beforeEach() = {
        super.beforeEachMethod()

        dao = new LogDAO(dbHelper)
        dao.createTable()

        this.prepareRun()
        this.addOneLogToDB()
    }

    override def afterEach() = {
        dao.dropTable()

        super.afterEachMethod()
    }

    def addOneLogToDB() = {
        log = new Log(timestamp, run)

        // this method works totally different than save method 
        // the log can be created only once
        dao.addLog(log)
    }
    
    test("retriving one log by id"){
        
        val result = dao.getOne(log.id)
        assert(result != null)
        assert(log.timeStamp === result.timeStamp)
        assert(log.run.description === result.run.description)
    }

    test("retriving all logs") {
        val result = dao.getLogs()
        assert(result.size == 1)

        val log = result.head

        // assert sample fields
        assert(log.timeStamp == timestamp)
    }

    test("retriving all logs and checking eager loading od runs and prototypes") {
        val result = dao.getLogs()
        val log = result.head

        // assert eager loading of elements
        assert(log.run != null)
        val r = log.run
        assert(r.id == 0L)
        assert(r.prototype != null)
        assert(r.prototype.id == 0L)
    }
    
}