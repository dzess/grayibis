package pl.put.gi.settings.modules

import org.slf4j.LoggerFactory

import com.google.inject.Singleton
import com.google.inject.AbstractModule
import com.google.inject.Provider

import pl.put.gi.dao.ecj.impl.ECJPrototypeDAO
import pl.put.gi.dao.ecj.IECJPrototypeDAO
import pl.put.gi.dao.extended.impl.MessageLogDAO
import pl.put.gi.dao.extended.impl.NamedFitnessLogDAO
import pl.put.gi.dao.extended.ExtendedDAOManager
import pl.put.gi.dao.extended.IFitnessLogDAO
import pl.put.gi.dao.extended.IMessageLogDAO
import pl.put.gi.dao.impl.LogDAO
import pl.put.gi.dao.impl.PrototypeDAO
import pl.put.gi.dao.impl.RunDAO
import pl.put.gi.dao.IDAOManager
import pl.put.gi.dao.ILogDAO
import pl.put.gi.dao.IPrototypeDAO
import pl.put.gi.dao.IRunDAO
import pl.put.gi.db.IDatabaseHelper

/**
 * Configures all the database access for Guice Module.
 */
class DatabaseAccessObjectModule(private val dbHelper: IDatabaseHelper) extends AbstractModule {

    private val logger = LoggerFactory.getLogger(classOf[DatabaseAccessObjectModule])

    override def configure() = {

        bind(classOf[IDatabaseHelper])
            .toInstance(dbHelper)

        bind(classOf[IDAOManager])
            .to(classOf[ExtendedDAOManager])
            .in(classOf[Singleton])

        bindDAO(classOf[IPrototypeDAO], classOf[PrototypeDAO])
        bindDAO(classOf[IRunDAO], classOf[RunDAO])
        bindDAO(classOf[ILogDAO], classOf[LogDAO])
        bindDAO(classOf[IFitnessLogDAO], classOf[NamedFitnessLogDAO])
        bindDAO(classOf[IMessageLogDAO], classOf[MessageLogDAO])
        bindDAO(classOf[IECJPrototypeDAO], classOf[ECJPrototypeDAO])

        logger.debug("DatabaseAccessObject Module configured")

    }

    private def bindDAO[T](interface: Class[T], implementation: Class[_ <: T]) = {
        bind[T](interface).to(implementation).in(classOf[Singleton])
    }
}

/**
 * Utility class for creating factory methods for Guice Provider
 */
class GenericProvider[T](private val func: Unit => T) extends Provider[T] {

    def get(): T = {
        func()
    }
}