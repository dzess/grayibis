package pl.put.gi.dao.extended.impl

import pl.put.gi.dao.extended.IFitnessLogDAO
import pl.put.gi.model.extended.FitnessLog
import pl.put.gi.dao.impl.BaseDAO
import pl.put.gi.db.IDatabaseHelper
import org.slf4j.LoggerFactory
import com.google.inject.Inject

/**
 * Manages the ORM for the FitnessLog class used for storing the fitness values
 * along experiment.
 *
 * The FitnessLogDAO loads the logs eagerly alongside with the run object.
 */
class FitnessLogDAO @Inject() (dbHelper: IDatabaseHelper)
    extends LogDAOBase(dbHelper, FitnessLog, FitnessLog) with IFitnessLogDAO {

    private val logger = LoggerFactory.getLogger(classOf[FitnessLogDAO])

    override def addLog(log: FitnessLog) = this.addLogBase(log)
    override def getAll(): List[FitnessLog] = this.getAllBase()
}