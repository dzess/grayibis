﻿-- This file contains some SQL basic DML commands on the Gray Ibis datamodel



-- Some dropping queries for cleansing all the data in the database
DROP VIEW v_all;
DROP VIEW v_hiff;
DROP VIEW v_onemax;

DROP TABLE "message-log";
DROP TABLE "fitness-log";
DROP TABLE logs;
DROP TABLE runs;
DROP TABLE "prototypes-ecj";
DROP TABLE prototypes;

-- Fixing quer after Parameters Analyzer destroyed some data
begin;
	delete from 
		"prototypes-ecj" ep
	where 
		ep.id > 7;
	delete from 
		"prototypes" p
	where 
		p.id > 7;
	
commit;


-- Fixing query for the runs which are not properly ended basing on the query from basic.sql
update "runs"
set
	"state" = 'cancelled'
where
	"state" not in ('done','error','cancelled','stopped','undefined')
	and ( NOW() - "modify-time" > interval '1 minute' );

-- Some useful deletion queries
delete from logs x 
where x.id in 
(
select l.id from logs l where l."runId" in (
	select r."id" from runs r where r."experimentID" = 2
	)
);

delete from runs x 
where x.id in (
	select r."id" from runs r where r."experimentID" = 2
);

delete from prototypes p
where p."id" = 2 ;