package pl.put.gi.gui.sequences

import pl.put.gi.model.ecj.ECJPrototype

abstract trait IPrototypeGenerator {
    def generateSingle[T](paramKey: String, paramValue: T,
        basingPrototype: ECJPrototype): ECJPrototype

    def reset()
}