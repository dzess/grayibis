package pl.put.gi.testing.dao.utils

import pl.put.gi.dao.impl.RunDAO
import pl.put.gi.dao.impl.PrototypeDAO
import pl.put.gi.model.Prototype
import pl.put.gi.model.Run
import java.sql.Timestamp
import java.util.Date
import pl.put.gi.db.PostgreSQLHelper
import pl.put.gi.db.TestDatabaseHelper

class LogDAOTestBase {

    protected var runDao: RunDAO = null
    protected var prototypeDao: PrototypeDAO = null

    protected val dbHelper = new TestDatabaseHelper()

    // values to be checked
    protected val timestamp = new Timestamp(new Date().getTime())

    protected var run: Run = null
    protected var prototype: Prototype = null
    
    protected val prototypeKey = "sample key"
    protected val runDescription = Some("Run description")

    protected def beforeEachMethod() = {
        prototypeDao = new PrototypeDAO(dbHelper)
        runDao = new RunDAO(dbHelper)

        // NOTE: that this is a correct sequence of creating the tables
        prototypeDao.createTable()
        runDao.createTable()
    }

    protected def afterEachMethod() = {
        runDao.dropTable()
        prototypeDao.dropTable()
    }

    def prepareRun() = {
        prototype = new Prototype()
        prototype.key = this.prototypeKey

        prototypeDao.save(prototype)

        run = new Run()
        run.prototype = prototype
        run.description = this.runDescription
        run.startTime = new Timestamp(new Date().getTime())

        runDao.save(run)
    }

}