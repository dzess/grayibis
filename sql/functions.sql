﻿-- Author: Piotr Jessa
-- Description: Reads parameters of prototype and selects the proper values of the name

CREATE OR REPLACE FUNCTION extract_field_numeric(fieldName varchar) RETURNS TABLE(key varchar,field double precision) AS
$$ 
DECLARE
	fieldRegexp varchar := '';
	sql varchar := '';
BEGIN
	fieldRegexp :=  fieldName || '=(\d+\.\d+)';
	RAISE NOTICE 'RE [%]',fieldRegexp;
	RETURN QUERY select x."key" ,cast(substring(x."parameters" from fieldRegexp) as double precision) as field from "prototypes" x;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION extract_field_text(fieldName varchar) RETURNS TABLE(key varchar,field text) AS
$$ 
DECLARE
	fieldRegexp varchar := '';
	sql varchar := '';
BEGIN
	fieldRegexp :=  fieldName || '=([\w\.]+)';
	RAISE NOTICE 'RE [%]',fieldRegexp;
	RETURN QUERY select x."key" , substring(x."parameters" from fieldRegexp ) as field from "prototypes" x;
END;
$$
LANGUAGE plpgsql;


-- testing queries
select * from extract_field_numeric('breed\.exchange\-ratio');
select * from extract_field_text('breed');
