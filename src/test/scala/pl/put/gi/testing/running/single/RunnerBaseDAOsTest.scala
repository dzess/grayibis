package pl.put.gi.testing.running.single

import org.junit.runner.RunWith
import org.mockito.Matchers.anyObject
import org.mockito.Matchers.anyString
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.when
import org.scalatest.junit.JUnitRunner
import org.scalatest.mock.MockitoSugar
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite
import pl.put.gi.dao.ILogDAO
import pl.put.gi.dao.IPrototypeDAO
import pl.put.gi.dao.IRunDAO
import pl.put.gi.dao.IDAOManager
import pl.put.gi.model.Prototype
import pl.put.gi.model.State
import pl.put.gi.running.RunnerConfiguration
import pl.put.gi.running.RunnerBase

/**
 * Helper class for testing the inheritance based code
 */
private class SampleRunner(_conf: RunnerConfiguration, _daoManager: IDAOManager) extends RunnerBase(_conf, _daoManager) {

    override def initializeExperiment() = assert(this.expRun.state == State.initializing)
    override def runExperiment() = assert(this.expRun.state == State.running)
    override def cleanupExperiment() = assert(this.expRun.state == State.cleaning)
}

/**
 * Helper class for testing the error handling in the inheritance based code
 */
private class FailingRunner(_conf: RunnerConfiguration, _daoManager: IDAOManager) extends RunnerBase(_conf, _daoManager) {

    override def initializeExperiment() = throw new RuntimeException("Yay im the imba exception")
    override def runExperiment() = {}
    override def cleanupExperiment() = {}
}

/**
 * Checks if the sample running of Runner base makes all the proper sets using daos. Uses
 * the SampleRunne from the up to test the inheritance based code - which is not so great.
 */
@RunWith(classOf[JUnitRunner])
class RunnerBaseDAOsTest extends FunSuite with BeforeAndAfterEach with MockitoSugar {

    var conf: RunnerConfiguration = null
    var runner: RunnerBase = null
    var daoManager: IDAOManager = null

    var runDAO: IRunDAO = null
    var prototypeDAO: IPrototypeDAO = null
    var logDAO: ILogDAO = null

    override def beforeEach() = {

        // mock the persistence layer
        daoManager = mock[IDAOManager]

        runDAO = mock[IRunDAO]
        prototypeDAO = mock[IPrototypeDAO]
        logDAO = mock[ILogDAO]

        // other things from dao manager are not used
        when(daoManager.getDAO(classOf[IRunDAO])).thenReturn(runDAO)
        when(daoManager.getDAO(classOf[IPrototypeDAO])).thenReturn(prototypeDAO)
        when(daoManager.getDAO(classOf[ILogDAO])).thenReturn(logDAO)
        when(daoManager.getDAOs()).thenReturn(List(runDAO, prototypeDAO, logDAO))

        // expected prototype search should also be stubbed
        val experimentPrototype = new Prototype()
        when(prototypeDAO.getPrototypeByKey(anyString()))
            .thenReturn(experimentPrototype)

        // use the default configuration
        conf = new RunnerConfiguration()
        conf.prototypeKey = "Sample prototype key"
        conf.runDescription = "Sample run description"
        runner = new SampleRunner(conf, daoManager)
    }

    test("experiment prototype is avaliable only after initialize") {
        assert(runner.expPrototype == null)
        runner.initialize()
        assert(runner.expPrototype != null)
    }

    test("experiment run state changes with the happy path flow") {

        // no experiment Run before initialization 
        assert(runner.expRun == null)

        // after initialize
        runner.initialize()
        assert(runner.expRun.state == State.initialized)
        assert(runner.expRun.error.isEmpty)

        // after run
        runner.run()
        assert(runner.expRun.state == State.runned)
        assert(runner.expRun.error.isEmpty)

        // after cleanup
        runner.cleanup()
        assert(runner.expRun.state == State.done)
        assert(runner.expRun.error.isEmpty)

        // verification about the number of invocation
        verify(runDAO, times(6)).save(anyObject())
    }

    test("experiment run state changes with the unhappy path") {

        // swap the runner into some more decent
        conf = new RunnerConfiguration()
        conf.prototypeKey = "Sample prototype key"
        conf.runDescription = "Sample run description"
        runner = new FailingRunner(conf, daoManager)

        intercept[Exception] {
            runner.initialize()
        }

        assert(runner.expRun.state == State.error)
        assert(runner.expRun.error.isEmpty == false)
    }

}