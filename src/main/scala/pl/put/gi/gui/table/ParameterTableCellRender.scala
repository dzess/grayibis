package pl.put.gi.gui.table

import java.awt.Color
import java.awt.Font
import scala.swing.BorderPanel
import scala.swing.Label
import javax.swing.table.TableCellRenderer
import javax.swing.JTable
import javax.swing.UIManager
import pl.put.gi.gui.ParametersAnalyzerModel

/**
 * Represents the render used for rendering the rows and columns
 * of the table. Depends on the ParametersAnalyzerModel for managing if the
 * cell was already modified or not.
 */
class ParameterTableCellRender extends BorderPanel with TableCellRenderer {

    private val lightRed = new Color(250, 133, 133)
    private val lightYellow = new Color(255, 236, 139)
    private val lightGreen = new Color(185, 251, 171)
    private val lightViolet = new Color(197, 170, 255)

    val label = new Label {
        foreground = Color.GRAY
        font = new Font("Monospace", 0, 12)
    }

    // this label is added here because of the performance reasons
    add(label, BorderPanel.Position.West)

    def getTableCellRendererComponent(table: JTable, value: Any, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int) = {
        label.text = String.valueOf(value)
        if (isSelected)
            background = table.getSelectionBackground
        else {
            // this is basic background color for the element
            lazy val neutralBackground = if (row % 2 == 0) UIManager.getColor("Table.alternateRowColor") else table.getBackground

            // but color has to be changed due to validation using some
            val tableModel = table.getModel().asInstanceOf[ParametersTableModel]
            val tuple = tableModel.rowData(row)

            background = tuple._3 match {
                case EntryStates.added => lightGreen
                case EntryStates.removed => lightRed
                case EntryStates.changedValue => lightYellow
                case EntryStates.changedKey => lightViolet
                case _ => neutralBackground
            }

        }
        peer
    }
}