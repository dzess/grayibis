package pl.put.gi.model.ecj

import pl.put.gi.model.Prototype
import pl.put.gi.model.util.IdTable
import pl.put.gi.model.util.IMapable
import pl.put.gi.model.util.MapStringCoder
import pl.put.gi.running.ecj.IStatisticsHarvester
import pl.put.gi.model.ecj.util.HarvesterCoder

/**
 * Models the single experiment with detailed attributes not only generic map
 * like it is done in Prototype.
 *
 * This detailed attributes are stored in options map. Which is the basis for all
 * other attributes stored.
 */
class ECJPrototype extends Prototype {

    // the keys of the options map
    val entryHarvesters = "harvesters"

    // this code could be injected by some DI framework yet this is not need to be configurable
    private val harvesterCoder = new HarvesterCoder()

    // internal storage of options
    private var _options: Map[String, String] = Map()
    def options = _options
    def options_=(other: Map[String, String]) = safeSetter(other) { x =>
        _options = x
        updateAttributes()
    }

    // attributes
    private var _harvesters: Seq[IStatisticsHarvester] = Seq()

    def harvesters = _harvesters
    def harvesters_=(other: Seq[IStatisticsHarvester]) = safeSetter(other) { x =>
        _harvesters = x
        updateOptions()
    }

    // useful code
    private def safeSetter[T](value: T)(setterFunc: T => Unit) = {
        if (value == null) {
            throw new IllegalArgumentException("passed value cannot be null");
        }
        setterFunc(value)
    }

    private def updateAttributes() = {
        // lets make attributes consistent with currently set options

        // harvesters
        val encodedHarvesters = options.get(entryHarvesters)
        val h = harvesterCoder.decode(encodedHarvesters.getOrElse(""))
        _harvesters = h

        // NOTE: add here  more options which might be needed
    }

    private def updateOptions() = {
        // lets make options consistent with currently set attributes
        val encodedHarvesters = harvesterCoder.encode(harvesters)
        _options = Map(entryHarvesters -> encodedHarvesters)

        // NOTE add here more attributes
    }
}

object ECJPrototype extends IdTable[(Long, Long, String)]("prototypes-ecj")
    with IMapable[(Long, Long, String), ECJPrototype] {

    override def id = column[Long]("id", O PrimaryKey)

    def prototypeId = column[Long]("prototypeId", O NotNull)
    def harvesters = column[String]("options", O NotNull, O.DBType("varchar(16384)"))

    def * = id ~ prototypeId ~ harvesters
    def prototypeFK = foreignKey("prototype_FK", prototypeId, Prototype)(_.id)

    override def toModel(inputTuple: (Long, Long, String)): ECJPrototype = {
        val p = new ECJPrototype()
        p.id = inputTuple._1
        p.options = MapStringCoder.stringToMap(inputTuple._3)

        return p
    }

    override def fromModel(inputModel: ECJPrototype): (Long, Long, String) = {
        val harvestersString = MapStringCoder.mapToString(inputModel.options)
        (inputModel.id, inputModel.id, harvestersString)
    }
}