package pl.put.gi.model.extended

import java.sql.Timestamp
import org.scalaquery.ql.TypeMapper.LongTypeMapper
import org.scalaquery.ql.TypeMapper.FloatTypeMapper
import org.scalaquery.ql.basic.BasicTable
import pl.put.gi.settings.InnerConstants
import pl.put.gi.model.util.IMapable
import pl.put.gi.model.Log
import pl.put.gi.model.Run
import pl.put.gi.model.util.IdTable

/**
 * Extension of normal not categorized fitness log
 */
class NamedFitnessLog(
    val category: String,
    value: Float,
    timeStamp: Timestamp = null,
    run: Run = null)
    extends FitnessLog(value, timeStamp, run) {

    if (category == null || category.isEmpty) {
        throw new IllegalArgumentException("category cannot be null or empty")
    }
}

class NamedFitnessLogTable(schemaName: String) extends IdTable[(Long, Long, Float)]("%s-fitness-log".format(schemaName))
    with IMapable[(Long, Long, Float), FitnessLog] {

    override def id = column[Long]("id", O PrimaryKey)
    def logId = column[Long]("logId", O NotNull)
    def fitness = column[Float]("fitness", O NotNull)

    def * = id ~ logId ~ fitness

    // foreign key on the Log table
    def fitnessLogFK = foreignKey("%s-fitnessLogFK".format(schemaName), logId, Log)(_.id)

    override def fromModel(inputModel: FitnessLog): (Long, Long, Float) = {
        (inputModel.id, inputModel.id, inputModel.value)
    }

    override def toModel(inputTuple: (Long, Long, Float)): FitnessLog = {
        val logId = inputTuple._1
        val refLogId = inputTuple._2
        val value = inputTuple._3

        // construct the fitness log (but some value are meant to be missing)
        val log = new NamedFitnessLog(schemaName, value)
        log.id = logId

        return log
    }
}