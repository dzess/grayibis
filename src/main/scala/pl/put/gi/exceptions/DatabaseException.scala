package pl.put.gi.exceptions

/**
 * Kind of all type of exception of the database subsystem.
 */
abstract class DatabaseException extends RuntimeException {

}