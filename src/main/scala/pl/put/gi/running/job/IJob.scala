package pl.put.gi.running.job

/**
 * Represents the queue of runs to be conducted. Each of the run
 * must be modeled by IRunner instance.
 */
abstract trait IJob {
    
    /**
     * Runs the all the defined runners and 
     */
    def execute() 
}