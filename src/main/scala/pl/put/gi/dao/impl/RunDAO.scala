package pl.put.gi.dao.impl

import org.scalaquery.ql.basic.BasicDriver.Implicit.baseColumnToColumnOps
import org.scalaquery.ql.basic.BasicDriver.Implicit.columnBaseToInsertInvoker
import org.scalaquery.ql.basic.BasicDriver.Implicit.namedColumnQueryToUpdateInvoker
import org.scalaquery.ql.basic.BasicDriver.Implicit.queryToQueryInvoker
import org.scalaquery.ql.basic.BasicDriver.Implicit.scalaQueryDriver
import org.scalaquery.ql.basic.BasicDriver.Implicit.tableToQuery
import org.scalaquery.ql.basic.BasicDriver.Implicit.valueToConstColumn
import org.scalaquery.ql.Parameters
import org.scalaquery.session.Database.threadLocalSession
import org.slf4j.LoggerFactory
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.model.Prototype
import pl.put.gi.model.Run
import pl.put.gi.settings.InnerConstants
import pl.put.gi.dao.IRunDAO
import org.scalaquery.ql.Query
import org.scalaquery.ql.NamedColumn
import java.sql.Timestamp
import org.scalaquery.session.Session
import com.google.inject.Inject

/**
 * Shows the methods for manipulating the Run class
 * used for experiments.
 *
 * Also behaves as the very thin object relational mapping.
 */
class RunDAO @Inject() (dbHelper: IDatabaseHelper) extends BaseDAO(dbHelper) with IRunDAO {

    private val logger = LoggerFactory.getLogger(classOf[RunDAO])

    // create the prototypeDAO for eager fetching 
    private val prototypeDAO = new PrototypeDAO(dbHelper)

    // queries templates
    private val getRunsQuery = this.generateAllQuery(Run)
    private val getRunByIdQuery = this.generateOneQuery(Run)
    private val maxIdQuery = this.generateMaxQuery(Run)

    override def getRuns(): List[Run] = {
        logger.debug("Querying DB for Runs")

        var runsList: List[Run] = null
        var prototypesIDs: List[Long] = null
        db.withSession {
            var result = getRunsQuery.list
            logger.debug("Quering DB for Runs - tuples {}", result)

            // re mapping from tuples into the object model
            // TODO: this zipping could be done at this step
            runsList = result.map(x => { Run.toModel(x) }).toList
            prototypesIDs = result.map(x => x._2).toList
        }

        def actionFunc(x: Run, y: Prototype) = { x.prototype = y }
        def retFunc(i: Long): Prototype = { prototypeDAO.getPrototype(i) }

        runsList = this.performEagerLoading(runsList, prototypesIDs, retFunc, actionFunc)

        logger.debug("Quering DB for Runs - resulted {}", runsList)
        return runsList
    }

    override def getRun(id: Long): Run = {
        logger.debug("Quering DB for Run by ID: {}", id)

        var run: Run = null
        var prototypeID: Long = InnerConstants.undefinedIdentifier
        db.withSession {

            val result = getRunByIdQuery(id).first
            logger.debug("Quering DB for Run by ID - tuples {}", result)
            if (result != null) {
                run = Run.toModel(result)
                // the second value on this is the id of the element
                prototypeID = result._2
            }
        }

        if (prototypeID != InnerConstants.undefinedIdentifier) {
            // this code should be called only if the any value has
            // been retrieved in the previous step
            val prototype = this.prototypeDAO.getPrototype(prototypeID)
            run.prototype = prototype
        }

        logger.debug("Quering DB for Run by ID {}", run)
        return run
    }

    override def save(run: Run) = {
        logger.debug("Saving the Run: {}", run)

        // validate if the prototype is already existing
        if (run.prototype == null) {
            throw new IllegalStateException("Prototype not referenced by run")
        }

        // static check if prototype can be stored in DB
        if (run.prototype.id == InnerConstants.undefinedIdentifier) {
            throw new IllegalStateException("Prototype is not persisted in DB")
        }

        val inBase = this.isObjectInDB(run, this.getRun)
        if (!inBase) {
            logger.debug("Object not in DB  - creating")

            try {
                this.db.withTransaction {

                    // generate new id  
                    run.id = this.getNewId(this.maxIdQuery)

                    val notComplete = Run.fromModel(run)
                    val tuple = (notComplete._1, run.prototype.id,
                        notComplete._3, notComplete._4, notComplete._5, notComplete._6, notComplete._7, notComplete._8)

                    logger.debug("Creating the object in DB: {}", tuple)
                    Run.insert(tuple)
                }
            } catch {
                // anything that will happen is out of ordinary 
                // and should be logged, before passing further
                case e: Exception => {
                    logger.warn("Exception during transaction to DB", e)
                    throw new RuntimeException("Exception during transaction", e)
                }
            }
        } else {
            logger.debug("Object already in DB - updating")

            // check if the only difference is on the finish date - if not then
            // signal such update as the inappropriate  update only if necessary)
            db.withSession {
                val inBaseObj = getRun(run.id)

                if (inBaseObj.hashCode() == run.hashCode()) {
                    logger.debug("Passed object is the same as the object in DB - no updated needed")
                } else {
                    logger.debug("Passed object is NOT the same as the object in DB")

                    if (inBaseObj.weakHashCode() != run.weakHashCode()) {
                        // basing that object differed with hashCode and their finishDates are the same
                        // only means some other thing is not equal
                        throw new IllegalStateException("Updating the already run experiment not supported except times and state")
                    } else {
                        // the only things different must be finish time so update is a
                        logger.debug("There are some differences on state or times")

                        // check if the finish time is already defined in
                        // NOTE: that those queries would not be called so often yet this is optimization chance here
                        val finishQuery = for { r <- Run if r.id === run.id } yield r.finishTime
                        val startQuery = for { r <- Run if r.id === run.id } yield r.startTime
                        val errorQuery = for { r <- Run if r.id === run.id } yield r.error

                        this.assertUpdatePossible(finishQuery, run.finishTime)
                        this.assertUpdatePossible(startQuery, run.startTime)
                        this.assertUpdatePossible(errorQuery, run.error)

                        // always update the state 
                        val stateQuery = for { r <- Run if r.id === run.id } yield r.state
                        val updatedState = stateQuery.update(run.state.toString)
                        val updaterError = errorQuery.update(run.error)

                        val updatedF = finishQuery.update(run.finishTime)
                        val updatedS = startQuery.update(run.startTime)

                        logger.debug("The object has been updated - modified {} rows", (updatedF + updatedS + updatedState + updaterError) / 4)
                    }
                }
            }

        }
        logger.debug("Saved finished")
    }

    private def assertUpdatePossible[T](query: Query[NamedColumn[T], T], oldValue: T)(implicit session: Session) = {
        val value = query.first

        if (value.asInstanceOf[Option[T]].isDefined) {
            logger.debug("Query returned not null so there is sime problem with that")

            if (value.equals(oldValue)) {
                logger.debug("Everything OK the value is the same")
            } else {
                throw new IllegalStateException("The value in some column already exists " + query)
            }
        } else {
            logger.debug("Query returned null or empty value, so can be updated")
        }
    }

    /**
     * Upon failure throws an exception. Otherwise passes.
     */
    private def assertUpdatePossible(query: Query[NamedColumn[Timestamp], Timestamp], time: Timestamp)(implicit session: Session) = {
        val value = query.first

        // the scala query returns instead of null the first time (O ms after 01.01.1970)
        val emptyTimeStamp = new Timestamp(0)
        if (value.equals(emptyTimeStamp)) {
            logger.debug("The query returned the empty timestamp thus returning")
            // TODO: put sample return here (scala skill is missing here)
        } else {

            if (value != null || time != null) {
                // there were already some value (check if it is the same as the passe one)
                logger.debug("Time already in this row {}", value)

                if (!value.equals(time)) {
                    throw new IllegalStateException("The time can be updated only once, old '" + time + "' new: '" + value + "'")
                }
                logger.debug("The value is the same")
            } else {
                logger.debug("No value for such time in DB {}", query)
            }
        }

    }

    override def createTable() = this.recreateTableBase(Run)
    override def dropTable() = this.dropTableBase(Run)
    override def isTableCreated(): Boolean = this.isTableCreatedBase(Run)
}