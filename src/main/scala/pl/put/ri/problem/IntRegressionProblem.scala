package pl.put.ri.problem

import ec.Problem
import ec.simple.SimpleProblemForm
import ec.util.Parameter
import ec.EvolutionState
import ec.Individual
import org.ecj.psh.PshProblem
import org.ecj.psh.PshEvaluator
import pl.put.ri.mapper.ICodeMapper
import ec.vector.ShortVectorIndividual
import ec.gp.koza.KozaFitness
import pl.put.ri.mapper.FlatCodeMapper
import org.spiderland.Psh.Interpreter
import org.spiderland.Psh.Program
import pl.put.regression.RegressionCase
import org.slf4j.LoggerFactory

/**
 * Simple Integer Symbolic Regression Problem.
 *
 * Based on ECJ-PSH IntRegressionProblem.
 *
 * @author Piotr Jessa
 *
 */
class IntRegressionProblem extends PshRegressionProblem {

    private val logger = LoggerFactory.getLogger(classOf[IntRegressionProblem])

    override protected def evaluateTestCase(testCase: RegressionCase, state: EvolutionState, thread: Int): Double = {

        // NOTE: this code should match higher dimensionality
        interpreter.ClearStacks()

        // NOTE: this int case here is very very bad beacause of this some things are 
        // not typesafe, for the ease of the programming
        val input = testCase.variables(0).toInt

        // pushing input value to int stack
        for (i <- 0 until repeatInStack) {
            for (value <- testCase.variables) {
                interpreter.intStack().push(value.toInt)
            }
        }

        // setting input value to input stack
        interpreter.inputStack().push(input);

        // executing the program
        interpreter.Execute(program, interpreter.getExecutionLimit());

        // Penalize individual if there is no result on the stack.
        if (interpreter.intStack().size() == 0) {
            return Float.NaN
        }

        return interpreter.intStack().top()
    }
}