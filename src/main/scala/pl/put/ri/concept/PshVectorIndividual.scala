package pl.put.ri.concept

import ec.vector.ShortVectorIndividual
import org.spiderland.Psh.Program
import pl.put.ri.mapper.ICodeMapper

/**
 * Basic Individual for Psh problem based on Vector Genome.
 */
@deprecated(message = "this code is not finished", since = "0.0.4")
class PshVectorIndividual(private val mapper: ICodeMapper) extends ShortVectorIndividual with IPshNatured {

    // TODO: this code needs special species to be made (for factory design pattern)

    override def getProgram(): Program = {
        val program = mapper.toProgram(genome)
        return program
    }
}