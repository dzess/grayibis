﻿-- all the runs
select
	p.key, count(*) as total
from 
 	"runs" r join "prototypes" p on r."experimentId" = p."id"
where
	p.key like 'OneMax_0_%' and r.state = 'done'
group by p.key;

-- get keys of one exchange ratio
select * from extract_field_numeric('breeder\.exchange\-ratio') where field = 0.5;

select * from extract_field_text('breed\.updater\.quality') where key like 'Reworked_%';
select * from extract_field_text('breed\.updater\.search') where key like 'Reworked_%';


-- basic query about the fitness
select
	fl.fitness,
	p.key
from  "runs" r 
join "prototypes" p on r."experimentId" = p."id" 
join "logs" l on l."runId" = r.id
join "fitness-log" fl on fl."logId" = l.id
where 
  p.key in (select key from extract_field_numeric('breeder\.exchange\-ratio') where field = 0.5)
and 
  r.state = 'done';

-- get the max fitness
select
  p.key,
  x.field,
  y.field,
  max(fl.fitness) as m,
  count(*) as total
from  "runs" r 
join "prototypes" p on r."experimentId" = p."id" 
left outer join "logs" l on l."runId" = r.id
left outer join "fitness-log" fl on fl."logId" = l.id
join (select * from extract_field_text('breed\.updater\.quality') where key like 'Reworked_%' ) x on p.key = x.key
join (select * from extract_field_text('breed\.updater\.search') where key like 'Reworked_%' ) y on p.key = y.key
where 
  p.key like 'Reworked_%'
and 
  r.state = 'done'
group by 
  p.key,
  x.field,
  y.field
order by m desc;


select p.key from "prototypes" p where p.key like 'Reworked_%';

-- go with runs and options
select r.state,p.key,x.field,y.field, count(*) as total
from "runs" r 
join "prototypes" p on r."experimentId" = p."id" 
join (select * from extract_field_text('breed\.updater\.quality') where key like 'Reworked_%' ) x on p.key = x.key
join (select * from extract_field_text('breed\.updater\.search') where key like 'Reworked_%' ) y on p.key = y.key
where p.key like 'Reworked_%'
group by r.state,p.key,x.field, y.field;


select * from extract_field_text('breed\.updater\.quality') where key like 'Reworked_%';
select * from extract_field_text('breed\.updater\.search') where key like 'Reworked_%';

