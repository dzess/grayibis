package pl.put.gi.dao

import pl.put.gi.model.Log

abstract trait ILogDAO extends IDAO {

    def addLog(log: Log)
    def getLogs() : List[Log]
    def getOne(id : Long) : Log
}