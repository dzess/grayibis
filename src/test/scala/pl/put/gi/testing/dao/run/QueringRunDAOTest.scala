package pl.put.gi.testing.dao.run

import java.sql.Timestamp
import java.util.Date
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfterEach
import pl.put.gi.testing.dao.utils.RunDAOTestBase

@RunWith(classOf[JUnitRunner])
class QueringRunDAOTest extends RunDAOTestBase with FunSuite with BeforeAndAfterEach {
    override def beforeEach() = {
        this.beforeEachMethod()
    }

    override def afterEach() = {
        this.afterEachMethod()
    }

    test("retriving the list of runs") {
        val result = dao.getRuns()
        assert(result.size == 0)
    }

    test("retriving the single element from runs - simple") {

        val run = this.getRun()
        dao.save(run)

        // presume the id used by the database 
        // not the best way to this anyway
        var result = dao.getRun(run.id)
        assert(result.description.get == "foo")
        assert(result.error.get == "this is some error message")
        assert(result.startTime === new Timestamp(startDate.getTime()))
        assert(result.finishTime === new Timestamp(finishDate.getTime()))

    }

    test("retriving the single element from run - complex") {
        // prepare data for testing
        val run = this.getRun()
        dao.save(run)

        val retrievedRun = dao.getRun(run.id)

        assert(retrievedRun.description === runDesc)
        assert(retrievedRun.error === errorMsg)
        assert(retrievedRun.prototype != null)

        val retPrototype = retrievedRun.prototype
        assert(retPrototype.key === prototypeKey)
        assert(retPrototype.description === prototypeDesc)
    }

    test("retriving the single element from runs list - complex") {

        // prepare data for testing
        val run = this.getRun()
        dao.save(run)

        val retrivedRuns = dao.getRuns()
        assert(retrivedRuns.size === 1)

        val retrievedRun = retrivedRuns.head
        assert(retrievedRun.description === runDesc)
        assert(retrievedRun.prototype != null)

        val retPrototype = retrievedRun.prototype
        assert(retPrototype.key === prototypeKey)
        assert(retPrototype.description === prototypeDesc)

    }
}