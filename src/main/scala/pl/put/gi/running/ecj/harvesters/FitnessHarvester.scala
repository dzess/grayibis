package pl.put.gi.running.ecj.harvesters

import java.sql.Timestamp
import java.util.Date

import org.slf4j.LoggerFactory

import com.google.inject.Inject

import ec.simple.SimpleFitness
import ec.simple.SimpleStatistics
import ec.Statistics
import pl.put.gi.dao.extended.IFitnessLogDAO
import pl.put.gi.model.extended.FitnessLog
import pl.put.gi.model.Run
import pl.put.gi.running.ecj.IStatisticsHarvester

/**
 * Logs only the fitness from the experiment assuming that passed statistics in the
 * ECJ is the SimpleStatistics class.
 */
class FitnessHarvester @Inject()(private val fitnessLog: IFitnessLogDAO) extends IStatisticsHarvester {

    private val logger = LoggerFactory.getLogger(classOf[FitnessHarvester])
    private var value: Float = 0

    override def onStart(statistic: Statistics, run: Run) = {
        logger.debug("On Start logging nothing")
    }

    override def onFinish(statistic: Statistics, run: Run) = {
        logger.debug("On Finish logging the best fitness value {}", value)
    }

    override def onChange(statistic: Statistics, run: Run) = {
        val stat = statistic.asInstanceOf[SimpleStatistics];

        // only one individual is the best one
        val ind = stat.best_of_run(0)
        val fitness = ind.fitness.asInstanceOf[SimpleFitness]
        FitnessHarvester.this.value = fitness.fitness()

        logger.debug("On Change logging the fitness value {}", value)

        val timestamp = new Timestamp(new Date().getTime())
        val log = new FitnessLog(value, timestamp, run)

        // saving to the DB
        fitnessLog.addLog(log)
    }
}