package pl.put.gi.testing.loaders

import java.io.File
import java.io.FileWriter

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite

import pl.put.gi.loaders.FlatFileParametersLoader
import pl.put.gi.util.scala.FileUtils.using

/**
 * Loading the data from Flat file for the ECJ.
 */
@RunWith(classOf[JUnitRunner])
class FlatFileParameterLoaderTest extends FunSuite with BeforeAndAfterEach {

    private val fileName = "sample-test-file-name"
    private var reader: FlatFileParametersLoader = null

    override def beforeEach() = {
        reader = new FlatFileParametersLoader(fileName)
    }

    override def afterEach() = {
        val f = new File(fileName)
        if (f.exists) {
            f.delete()
        }
    }

    def writeContentIntoFile(input: String) = {
        using(new FileWriter(fileName)) {
            f => f.write(input)
        }
    }

    test("reads the flat params file properly") {
        val properties = """
            # Lines in comments are not read
            # sample0 = foo0
            sample1 = foo1
            sample2 = foo2
            """
        writeContentIntoFile(properties)

        val map = reader.loadProperties()

        expect(2)(map.size)
        expect("foo1")(map("sample1"))
        expect("foo2")(map("sample2"))
        expect(false)(map.contains("foo0"))
    }

    test("reads the hierachical params file - throws exception") {
        val properties = """
            # Lines in comments are not read
            # sample0 = foo0
            sample1 = foo1
            sample2 = foo2
            
            # This is a parental reference on the ECJ parameter file 
            parent.0 = ../../simple/simple.params
            """

        writeContentIntoFile(properties)
        val error = intercept[IllegalArgumentException] {
            reader.loadProperties()
        }
        val expectedMessage = "Parent PARAMS file are not supported"

        assert(error.getMessage === expectedMessage)
    }

    test("reads not existing file - throws exception") {
        val error = intercept[IllegalArgumentException] {
            reader.loadProperties()
        }
    }
}