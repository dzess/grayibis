package pl.put.gi.testing.model
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import pl.put.gi.model.util.MapStringCoder

@RunWith(classOf[JUnitRunner])
class MapStringCoderTest extends FunSuite {

    private val map = Map("a" -> "x", "b" -> "y")

    test("encoding null map results in argument exception") {
        intercept[IllegalArgumentException] {
            MapStringCoder.mapToString(null)
        }
    }

    test("decoding the null sring results in argument exception") {
        intercept[IllegalArgumentException] {
            MapStringCoder.stringToMap(null)
        }
    }

    test("encoding map with en empty string") {
        val weirdMap = Map("x" -> "")
        val result = MapStringCoder.mapToString(weirdMap)

        val expectedResult = "x="
        expect(expectedResult)(result)
    }

    test("decoding map with an empty string") {
        val inputString = "x="
        val m = MapStringCoder.stringToMap(inputString)
        
        expect(1)(m.size)
        expect("")(m("x"))
    }

    test("decoding the empty string results in empty map") {
        val result = MapStringCoder.stringToMap("")
        assert(result === Map())
    }

    test("encoding empty map results in empty string") {
        val result = MapStringCoder.mapToString(Map())
        assert(result === "")
    }

    test("encoding map results in proper string") {
        val result = MapStringCoder.mapToString(map)
        assert(result === "a=x&b=y")
        val rMap = MapStringCoder.stringToMap(result)
        assert(map === rMap)
    }

    test("decoding map resutls in proper string") {
        val input = "a=x&b=y"
        val result = MapStringCoder.stringToMap(input)

        assert(map === result)

        val rString = MapStringCoder.mapToString(result)
        assert(rString === input)
    }

}