package pl.put.gi.testing.utils

import org.mockito.Mockito
import org.slf4j.LoggerFactory

import com.google.inject.AbstractModule

import pl.put.gi.running.IRunnerProvider

/**
 * Mock implementation of Guice profile
 */
class MockModule extends AbstractModule {

    private val logger = LoggerFactory.getLogger(classOf[MockModule])

    override def configure() = {

        bind(classOf[IRunnerProvider])
            .toInstance(getMockRunnerProvider())

        // make this information about what mock is being used available in logger
        logger.debug("Mock Module configured with '{}'", classOf[IRunnerProvider])
    }

    private def getMockRunnerProvider(): IRunnerProvider = {
        Mockito.mock(classOf[IRunnerProvider])
    }
}