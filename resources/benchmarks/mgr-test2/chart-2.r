# This file does ploting of the data of MGR-TEST-2 using some views
# Requires: gi.setup
# Author: Piotr Jessa

gi.test2.load <- function() {
  all_dataset <- gi.load.view('v_max_fitness','2')
  return(all_dataset)
}

gi.test2.chart <- function(dataset) {
  
  dr <- "weight"
  df <- "avg"
  dk <- "stddev"
  
  p <- ggplot(dataset,aes_string(x = dr,y =df))
  p <- p + geom_point()
  P <- p + labs(x = "weigth", y = "average fitness")
  ggsave("mgr2.pdf",useDingbats=FALSE)
  return (p)
}