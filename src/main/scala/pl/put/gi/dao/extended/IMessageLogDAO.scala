package pl.put.gi.dao.extended

import pl.put.gi.dao.IDAO
import pl.put.gi.model.extended.MessageLog

trait IMessageLogDAO extends IDAO {
    def addLog(log: MessageLog)
    def getAll(): List[MessageLog]
}