package pl.put.regression

class RegressionCase(value: Seq[Double]) {
    var variables: Array[Double] = value.toArray
    def rank = variables.length
}