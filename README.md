### Gray Ibis 

#### Conception

Set of utility Scala classes and tools for running experiments. Consits of:

* datamodel and data access classes - for modeling the database schema for running experiments
> packages: *dao,db,model*


* runners - for providing the easy to use integration features with ECJ library such as database logger or database aware statistics
> packages: *running*


* gui - for analisys of *ECJ* *params* files. 
> packages: *gui*

* loaders - tools for loading the *ECJ* *params* files into map structures (alongside with parental recovery)
> packages: *loaders*


#### How to use it

* **Parameter Analyzer** - run script to start the GUI editor of *Prototypes* stored in the database
* **Gray Ibis** - run *JAR* with dependecies and provide configuration as is in the _resources/*.properties_ files

#### Requirements

* Maven 3.x
* Java 1.6.x


#### Building

1. edit *test-database.properties* in the src/test/resources to match your current database
2. `mvn package`

#### License
*LGPL v.3*

#### Author

Piotr Jessa



