package pl.put.gi.testing.loaders

import java.io.FileWriter
import java.io.File
import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import pl.put.gi.util.scala.FileUtils.using
import org.scalatest.BeforeAndAfterEach
import pl.put.gi.loaders.HierarchicalFileParametersLoader
import pl.put.gi.util.scala.OSUtils

@RunWith(classOf[JUnitRunner])
class HierachicalFileParemeterLoaderTest extends FunSuite with BeforeAndAfterEach {

    private val leafFileName = "leaf-file-name"
    private val nodeFileName = "node-file-name"
    private val rootFileName = "root-file-name"

    private var reader: HierarchicalFileParametersLoader = null
    private var m: Map[String, String] = null

    val properties =
        """
            # Lines in comments are not read
            # sample0 = foo0
            sample1 = foo1
            sample2 = foo2
            """

    val rootProperties =
        """
            # rootProperty0 = root0
            rootProperty1 = root1
            """

    val nodeProperties =
        """
            # nodePropert0 = node0
            nodeProperty1 = node1
            """

    override def beforeEach() = {
        reader = new HierarchicalFileParametersLoader(leafFileName)
        m = null
    }

    override def afterEach() = {
        delete(leafFileName)
        delete(nodeFileName)
        delete(rootFileName)
    }

    def delete(where: String) = {
        val f = new File(where)
        if (f.exists) {
            f.delete()
        }
    }

    def write(input: String, where: String) = {
        using(new FileWriter(where)) {
            f => f.write(input)
        }
    }

    def addParent(builder: StringBuilder, parentFileName: String, parentNumber: Int = 0): StringBuilder = {
        builder.append("parent." + parentNumber + " = " + parentFileName)
        if(OSUtils.isWindows){
          builder.append("\r")
        }
        builder.append("\n")
        builder
    }

    test("reading leaf file") {
        write(properties, leafFileName)

        m = reader.loadProperties()

        expect(2)(m.size)
        expect("foo1")(m("sample1"))
        expect("foo2")(m("sample2"))
        expect(false)(m.contains("foo0"))
    }

    test("reading root leaf file") {
        val leafBuilder = new StringBuilder(properties)
        addParent(leafBuilder, rootFileName)

        write(leafBuilder.toString(), leafFileName)
        write(rootProperties, rootFileName)

        m = reader.loadProperties()

        expect(3)(m.size)
        expect("root1")(m("rootProperty1"))
        expect("foo1")(m("sample1"))
        expect("foo2")(m("sample2"))

        assert(m.contains("rootProperty0") === false)
        assert(m.contains("foo0") === false)

        // this reference should also not be visible
        assert(m.contains("parent.0") === false)
    }

    test("reading root leaf files with different folder and relavtive locations") {

        // leaf node will be in folder ./test_folder/leaf.params
        // and will be referencing the ./root.params

        val inFolderLocation = "test_folder"
        // make some folders 
        val folder = new File(inFolderLocation)
        if (!folder.exists) {
            folder.mkdir()
        }
        val path = inFolderLocation + File.separator + leafFileName

        try {
            // root will be upper
            val leafBuilder = new StringBuilder(properties)
            // note that properties files in windows have character '\'
            // which is regarded as the escape character all the, thus use OS
            // selection of the 
            if(OSUtils.isWindows){
            	addParent(leafBuilder, "../" + rootFileName)  
            }
            else{
            	addParent(leafBuilder, "..%c".format(File.separatorChar) + rootFileName)
            }
            

            write(rootProperties, rootFileName)
            write(leafBuilder.toString(), path)

            reader = new HierarchicalFileParametersLoader(path)
            m = reader.loadProperties()

            expect(3)(m.size)
            expect("root1")(m("rootProperty1"))
            expect("foo1")(m("sample1"))
            expect("foo2")(m("sample2"))

        } finally {
            // clean up a
            val f = new File(path)
            if (f.exists) {
                f.delete()
            }

            if (folder.exists) {
                folder.delete()
            }

        }
    }

    ignore("reading leaf file with no leaf file - throws an exception") {
        val error = intercept[IllegalArgumentException] {
            reader.loadProperties()
        }

        assert(error.getMessage === "Not found file " + leafFileName)
    }

    ignore("reading root leaf file with no root file - throws an exception") {
        val leafBuilder = new StringBuilder(properties)
        addParent(leafBuilder, rootFileName)

        write(leafBuilder.toString(), leafFileName)
        // do not write root file

        val error = intercept[IllegalArgumentException] {
            reader.loadProperties()
        }

        assert(error.getMessage === "Not found file " + new File(rootFileName).getAbsolutePath)
    }

    test("reading root node leaf file") {
        val leafBuilder = new StringBuilder(properties)
        addParent(leafBuilder, nodeFileName)

        val nodeBuilder = new StringBuilder(nodeProperties)
        addParent(nodeBuilder, rootFileName)

        write(leafBuilder.toString(), leafFileName)
        write(nodeBuilder.toString(), nodeFileName)
        write(rootProperties, rootFileName)

        m = reader.loadProperties()

        expect(4)(m.size)
        expect("root1")(m("rootProperty1"))
        expect("foo1")(m("sample1"))
        expect("foo2")(m("sample2"))
        expect("node1")(m("nodeProperty1"))

        assert(m.contains("nodeProperty0") === false)
        assert(m.contains("rootProperty0") === false)
        assert(m.contains("foo0") === false)

        // this reference should also not be visible
        assert(m.contains("parent.0") === false)
    }

    test("reading leaf and two parents file - checking of ordering") {
        val leafBuilder = new StringBuilder()

        // swapping those elements is really important because the other way 
        // round the numbering might not be taken into account 
        addParent(leafBuilder, rootFileName, 1)
        addParent(leafBuilder, nodeFileName, 0)

        val node0 = """
            property = foo0
            """

        val node1 = """
            property = foo1
            """

        write(leafBuilder.toString(), leafFileName)
        write(node0, nodeFileName)
        write(node1, rootFileName)

        m = reader.loadProperties()

        expect(1)(m.size)
        expect("foo1")(m("property"))
    }

    test("reading leaf and root - checking visibility of duplicates") {
        val leafRootProperties = """
               rootProperty1 = leaf1
               """
        val leafBuilder = new StringBuilder(leafRootProperties)
        addParent(leafBuilder, rootFileName)

        write(leafRootProperties.toString(), leafFileName)
        write(rootProperties, rootFileName)

        m = reader.loadProperties()

        expect(1)(m.size)
        expect("leaf1")(m("rootProperty1"))
    }

    test("reading leaf node leaf files cycle - throws an exception") {
        val leafBuilder = new StringBuilder(properties)
        addParent(leafBuilder, nodeFileName)

        val nodeBuilder = new StringBuilder(nodeProperties)
        addParent(nodeBuilder, leafFileName)

        write(leafBuilder.toString(), leafFileName)
        write(nodeBuilder.toString(), nodeFileName)

        val error = intercept[IllegalArgumentException] {
            m = reader.loadProperties()
        }

        assert(error.getMessage === "Cycles found in the parental relations")
    }

    ignore("reading leaf node leaf files cycle, mixed paths relative and absolute - throws an exception") {
        val leafBuilder = new StringBuilder(properties)
        addParent(leafBuilder, nodeFileName)

        val nodeBuilder = new StringBuilder(nodeProperties)
        val absoluteLeafFileName = new File(leafFileName).getAbsolutePath()
        addParent(nodeBuilder, absoluteLeafFileName)

        write(leafBuilder.toString(), leafFileName)
        write(nodeBuilder.toString(), nodeFileName)

        val error = intercept[IllegalArgumentException] {
            m = reader.loadProperties()
        }

        assert(error.getMessage === "Cycles found in the parental relations")
    }

}