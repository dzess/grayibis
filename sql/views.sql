﻿-- Stores the views which might be usable by Data Analisys

-- View: v_all
CREATE OR REPLACE VIEW v_all AS 
 SELECT r.id, r.state, r."error-message", r."start-time", r."finish-time", p.key, l."timestamp", fl.fitness
   FROM runs r
   JOIN prototypes p ON r."experimentId" = p.id
   JOIN logs l ON l."runId" = r.id
   JOIN "fitness-log" fl ON fl."logId" = l.id;

ALTER TABLE v_all
  OWNER TO dzess;
COMMENT ON VIEW v_all
  IS 'Fitness log consolidation on ALL data';

-- View: v_hiff
CREATE OR REPLACE VIEW v_hiff AS 
 SELECT r.id, r.state, r."error-message", r."start-time", r."finish-time", p.key, l."timestamp", fl.fitness
   FROM runs r
   JOIN prototypes p ON r."experimentId" = p.id
   JOIN logs l ON l."runId" = r.id
   JOIN "fitness-log" fl ON fl."logId" = l.id
  WHERE p.key::text ~~ '%Hiff%'::text;

ALTER TABLE v_hiff
  OWNER TO dzess;
COMMENT ON VIEW v_hiff
  IS 'Fitness log consolidation on Hiff Problem';


-- View: v_onemax
CREATE OR REPLACE VIEW v_onemax AS 
 SELECT r.id, r.state, r."error-message", r."start-time", r."finish-time", p.key, l."timestamp", fl.fitness
   FROM runs r
   JOIN prototypes p ON r."experimentId" = p.id
   JOIN logs l ON l."runId" = r.id
   JOIN "fitness-log" fl ON fl."logId" = l.id
  WHERE p.key::text ~~ '%OneMax%'::text;

ALTER TABLE v_onemax
  OWNER TO dzess;
COMMENT ON VIEW v_onemax
  IS 'Fitness log consolidation on OneMax problem';