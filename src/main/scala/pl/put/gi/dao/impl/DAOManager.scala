package pl.put.gi.dao.impl

import org.slf4j.LoggerFactory

import com.google.inject.Inject

import pl.put.gi.dao.IDAO
import pl.put.gi.dao.IDAOManager
import pl.put.gi.dao.ILogDAO
import pl.put.gi.dao.IPrototypeDAO
import pl.put.gi.dao.IRunDAO

/**
 * Manages the basic DAO usages. Bases on the assumption that all the
 * DAOs use the same database.
 */
class DAOManager @Inject() (
    prototypeDAO: IPrototypeDAO,
    runDAO: IRunDAO,
    logDAO: ILogDAO) extends IDAOManager {

    private val logger = LoggerFactory.getLogger(classOf[DAOManager])

    // NOTE: the sequence of creation of those object is quite important
    // because of the foreign key relation between them
    private val _dao: List[IDAO] = List(
        prototypeDAO,
        runDAO,
        logDAO)

    override def dropTables() = {
        _dao.reverse.foreach(x => x.dropTable())
    }

    override def createTables() = {
        _dao.foreach(x => x.createTable())
    }

    protected def checkSafeCreation(daos: List[IDAO]): Boolean = {
        val existingList = daos.map(x => x.isTableCreated()).toList

        val falseEx = existingList.exists(x => x == false)
        val trueEx = existingList.exists(x => x == true)

        if (falseEx && trueEx) {
            throw new IllegalStateException("at least one table is already created")
        }

        return falseEx
    }

    override def safeCreateTables() = {

        // check if tables are already defined  (one reevaluation against DB)
        val areAllTableNotExisting = this.checkSafeCreation(_dao)

        // means all are false
        if (areAllTableNotExisting) {
            logger.debug("All tables does not exist - creating")
            this.createTables()
        } else {
            logger.debug("All tables already defined skipping creation")
        }

    }

    protected def checkGetSingleDAO[T <: IDAO](daos: List[IDAO], cls: Class[T]): Option[IDAO] = {
        daos.find((x: IDAO) => cls.isAssignableFrom(x.getClass()))
    }

    override def getDAO[T <: IDAO](cls: Class[T]): T = {

        val objOption = this.checkGetSingleDAO(_dao, cls)

        if (objOption.isEmpty) {
            logger.warn("cannot find the search dao type {}", cls)
            throw new IllegalArgumentException("cannot find the searched dao type")
        }

        val obj = objOption.get

        logger.debug("Found dao {}", obj)

        return obj.asInstanceOf[T]
    }

    override def getDAOs(): List[IDAO] = {
        _dao.toList
    }

}