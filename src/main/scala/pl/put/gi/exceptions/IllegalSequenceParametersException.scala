package pl.put.gi.exceptions

/**
 * Class invoked when arguments for SequenceGenerator are not correct.
 */
class IllegalSequenceParametersException(message: String) extends IllegalArgumentException(message) {
}