package pl.put.gi.running

import org.slf4j.LoggerFactory
import pl.put.gi.util.IFreezable
import pl.put.gi.util.IValidable
import pl.put.gi.util.IPrintable

/**
 * Defines the configuration for the single configuration of experiment. Mainly
 * has to options.
 */
class RunnerConfiguration extends IFreezable with IValidable with IPrintable {
    private val logger = LoggerFactory.getLogger(classOf[RunnerConfiguration])

    // default plain items
    private var _prototypeKey: String = null
    private var _runDescription: String = null
    private var _seed: Int = 0

    def prototypeKey = _prototypeKey
    def prototypeKey_=(o: String) = RunnerConfiguration.this.safeSetter(o) { (x => _prototypeKey = x) }

    def runDescription = _runDescription
    def runDescription_=(o: String) = RunnerConfiguration.this.safeSetter(o) { (x => _runDescription = x) }

    def seed = _seed
    def seed_=(o: Int) = RunnerConfiguration.this.safeSetter(o) { (x => _seed = x) }

    override def validate() = {

        logger.info("Validating the run configuration object {}", RunnerConfiguration.this.toString())

        if (_prototypeKey == null || _runDescription.isEmpty()) {
            throw new IllegalArgumentException("The prototype key cannot be null")
        }

        if (_runDescription == null || _runDescription.isEmpty()) {
            throw new IllegalArgumentException("The run description cannot be null or empty")
        }

        // NOTE: add here validation

        logger.info("Validating the run configuration object {} successful", RunnerConfiguration.this.toString())
    }

    override def freeze() = {
        super.freeze()
    }

    override def print(): String = {
        var builder = new StringBuilder()
        builder = this.insertHeader(builder)
        builder = this.insertBeginMark(builder)

        builder.append("Prototype = " + prototypeKey + "\n")
        builder.append("Run Description = " + runDescription + "\n")
        
        // seed is not a option here

        builder = this.insertEndMark(builder)
        builder.toString()
    }
}