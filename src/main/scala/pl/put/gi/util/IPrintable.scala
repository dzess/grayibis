package pl.put.gi.util

/**
 * Allows pretty print of the stored values.
 */
abstract trait IPrintable {

    /**
     * Returns formatted print of the element marked IPrintable
     */
    def print(): String

    protected def insertHeader(builder: StringBuilder): StringBuilder = {
        val clsName = this.getClass().getName()
        val hashCode = this.hashCode()
        
        builder.append("\n")
        builder.append(clsName)
        builder.append("@")
        builder.append(hashCode)
    }

    protected def insertBeginMark(builder: StringBuilder): StringBuilder = {
        builder.append(" {\n")
    }

    protected def insertEndMark(builder: StringBuilder): StringBuilder = {
        builder.append("}\n")
    }
}