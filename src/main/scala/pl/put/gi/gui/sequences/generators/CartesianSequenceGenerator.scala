package pl.put.gi.gui.sequences.generators

import org.slf4j.LoggerFactory
import pl.put.gi.gui.sequences.CompositeGeneratorInputParameter
import pl.put.gi.gui.sequences.InputParameter
import pl.put.gi.gui.sequences.SequenceGenerator
import pl.put.gi.gui.sequences.SequenceGeneratorBase
import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.gui.sequences.ISequenceGenerator
import pl.put.gi.exceptions.IllegalSequenceParametersException

/**
 * Generates Cartesian product of other Sequence Generators.
 */
class CartesianSequenceGenerator(private val otherGenerators: Seq[ISequenceGenerator]) extends SequenceGeneratorBase {
    private val logger = LoggerFactory.getLogger(classOf[CartesianSequenceGenerator])

    val generatorsParameter = CompositeGeneratorInputParameter("generators", otherGenerators)

    private val sequences = Seq(generatorsParameter)

    override def inputs: Seq[InputParameter] = sequences

    override def generate(basingPrototype: ECJPrototype, inputs: Seq[InputParameter]): Seq[ECJPrototype] = {

        val usedGenerators = this.findParameter[Seq[(ISequenceGenerator, Seq[InputParameter])]](generatorsParameter, inputs)

        if (usedGenerators.isEmpty) {
            throw new IllegalSequenceParametersException("gennerator must be provided")
        }

        logger.debug("Cartesian Sequence Generator will fold {} times", usedGenerators.size)

        var foldedPrototypes: Seq[ECJPrototype] = Seq(basingPrototype)

        // create the Cartesian product out of the generators ;)
        for (i <- 0 until usedGenerators.size) {
            logger.debug("Generating fold {}", i)
            val (generator, params) = usedGenerators(i)

            val doubleSeqCollection = for (basingOne <- foldedPrototypes) yield {
                generator.generate(basingOne, params)
            }

            // replace the base for the prototypes
            foldedPrototypes = doubleSeqCollection.flatten
        }
        foldedPrototypes
    }
}