package pl.put.gi.settings.modules

import com.google.inject.AbstractModule
import pl.put.gi.running.IRunnerProvider
import pl.put.gi.running.IRunnerConfigurationProvider
import pl.put.gi.running.IRunner
import pl.put.gi.running.universal.RunnerConfigurationProvider
import pl.put.gi.running.ecj.ECJRunnerProvider
import org.slf4j.LoggerFactory

/**
 * Basic implementation of inner configuration of Guice Dependency Injection schemes.
 * This module is responsible only for IRunnerProvider and IRunnerConfigurationProvider
 * initialization.
 */
class ConfigurationModule extends AbstractModule {

    private val logger = LoggerFactory.getLogger(classOf[ConfigurationModule])

    override protected def configure() {

        bind(classOf[IRunnerProvider])
            .to(classOf[ECJRunnerProvider])

        logger.debug("Configuration Module configured")
    }
}