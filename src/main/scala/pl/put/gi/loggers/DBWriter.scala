package pl.put.gi.loggers

import java.io.Writer
import java.sql.Timestamp
import java.util.Date
import pl.put.gi.model.Run
import org.slf4j.LoggerFactory
import pl.put.gi.model.extended.MessageLog
import pl.put.gi.dao.extended.IMessageLogDAO

/**
 * Writer implementation which has access to the DB for dumping the stream into
 * database
 *
 * Each dump which uses the write method creates the new Log entry.
 */
class DBWriter(private val _dao: IMessageLogDAO, private val _run: Run) extends Writer {

    private val logger = LoggerFactory.getLogger(classOf[DBWriter])

    override def flush() = {
        // actually not used
    }

    override def close() = {
        // actually not used
    }

    override def write(buff: Array[Char], offset: Int, length: Int) = {

        // write the whole char array into stream
        var message = new String(buff)
        message = message.replaceAll("[\uFEFF-\uFFFF]", "")
        message = message.trim()

        val timestamp = new Timestamp(new Date().getTime())

        val log = new MessageLog(message, timestamp, _run)
        _dao.addLog(log)
    }
}