package pl.put.gi.gui.table

import javax.swing.table.AbstractTableModel
import pl.put.gi.gui.table.EntryStates._
import org.slf4j.LoggerFactory

/**
 * Models the table model for Key-Value pairs for ECJ Parameters.
 */
class ParametersTableModel(val columnNames: Seq[String]) extends AbstractTableModel {

    private val logger = LoggerFactory.getLogger(classOf[ParametersTableModel])
    private var _rowData: Seq[(String, String, EntryState)] = List()

    override def getColumnName(column: Int) = columnNames(column).toString

    def getRowCount() = rowData.length
    def getColumnCount() = columnNames.length

    def getValueAt(row: Int, col: Int): AnyRef = {
        val tuple = rowData(row)
        if (col == 0) {
            tuple._1
        } else if (col == 1) {
            tuple._2
        } else {
            throw new IllegalStateException("not supported column")
        }
    }

    def rowData = _rowData
    def rowData_=(other: Seq[(String, String, EntryState)]) {
        if (other == null) {
            throw new IllegalArgumentException("parameters data cannot be null")
        }

        // sort the rows to be of better order (easier to navigate)
        val sorted = other.sortWith((x, y) => x._1 < y._1).toSeq
        _rowData = sorted
    }

    override def isCellEditable(row: Int, column: Int) = true

    override def setValueAt(value: Any, row: Int, col: Int) = {
        if (col == 0) {
            val oldTuple = rowData(row)
            val newValue = value.toString()
            val newTuple = if (oldTuple._3 != EntryStates.added) {
                (newValue, oldTuple._2, EntryStates.changedKey)
            } else {
                (newValue, oldTuple._2, oldTuple._3)
            }
            logger.debug("Setting new {}", newTuple)

            // swap this one element
            rowData = rowData.filter(x => x._1 != oldTuple._1).toSeq
            rowData = rowData ++ List(newTuple)
        } else if (col == 1) {
            val oldTuple = rowData(row)
            val newValue = value.toString()
            val newTuple = if (oldTuple._3 != EntryStates.added) {
                (oldTuple._1, newValue, EntryStates.changedValue)
            } else {
                (oldTuple._1, newValue, oldTuple._3)
            }

            logger.debug("Setting new {}", newTuple)

            // swap this one element
            rowData = rowData.filter(x => x._1 != oldTuple._1).toSeq
            rowData = rowData ++ List(newTuple)

        } else {
            throw new IllegalStateException("not supported operation")
        }
    }

    def removeEntry(entry: (String, String)) = {
        val changedRowData = rowData.filter(x => x._1 != entry._1).toSeq
        val removedTuple = (entry._1, entry._2, EntryStates.removed)
        rowData = changedRowData ++ List(removedTuple)
    }

    def addEntry(entry: (String, String)) = {
        val changedRowData = rowData.filter(x => x._1 != entry._1).toSeq
        val addedTuple = (entry._1, entry._2, EntryStates.added)
        rowData = changedRowData ++ List(addedTuple)
    }
}