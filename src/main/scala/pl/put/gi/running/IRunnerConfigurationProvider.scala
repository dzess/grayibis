package pl.put.gi.running

import pl.put.gi.util.IFreezable
import pl.put.gi.util.IValidable

/**
 * Describes what class responsible for producing the RunConfigurations should do.
 */
abstract trait IRunnerConfigurationProvider extends IFreezable with IValidable {

    /**
     * Generates the RunConfiguration basing on the internal knowledge
     */
    def generate(): List[RunnerConfiguration]
}