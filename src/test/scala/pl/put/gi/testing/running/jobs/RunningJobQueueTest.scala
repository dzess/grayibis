package pl.put.gi.testing.running.jobs

import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mock.MockitoSugar
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import pl.put.gi.running.job.IJob
import pl.put.gi.running.job.JobQueue
import pl.put.gi.Configuration
import pl.put.gi.running.IRunnerProvider
import pl.put.gi.running.IRunnerConfigurationProvider
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.running.IRunner
import org.mockito.Matchers._
import org.mockito.Mockito._
import pl.put.gi.running.IRunnerProvider
import pl.put.gi.running.RunnerConfiguration

@RunWith(classOf[JUnitRunner])
class RunningJobQueueTest extends FunSuite
    with BeforeAndAfterEach with MockitoSugar {

    var job: IJob = null
    var configuration: Configuration = null
    var runConfiguration: RunnerConfiguration = null

    var runProviderMock: IRunnerProvider = null
    var runConfigurationMock: IRunnerConfigurationProvider = null
    var databaseHelperMock: IDatabaseHelper = null
    var runMock: IRunner = null

    override def beforeEach() = {

        // reset all mock
        runProviderMock = mock[IRunnerProvider]
        runConfigurationMock = mock[IRunnerConfigurationProvider]
        databaseHelperMock = mock[IDatabaseHelper]
        runMock = mock[IRunner]

        runConfiguration = new RunnerConfiguration()

        configuration = Configuration.default
        configuration.dbHelper = databaseHelperMock
        configuration.runProvider = runProviderMock
        configuration.runConfigurationProvider = runConfigurationMock

        job = new JobQueue(configuration)

        // this is quite obvious
        when(runProviderMock.produce(anyObject())).thenReturn(runMock)
        when(runConfigurationMock.generate).thenReturn(List(runConfiguration))
    }

    override def afterEach() = {

    }

    test("running one job invokes all 3 methods on runner class") {
        job.execute()

        verify(runMock, times(1)).initialize()
        verify(runMock, times(1)).run()
        verify(runMock, times(1)).cleanup()
    }

    test("runns are conducted sequentially") {
        (pending)
    }

    test("failing in the initialization blocks throws exception") {
        when(runMock.initialize).thenThrow(new RuntimeException("some initiailze exception"))

        intercept[Exception] {
            job.execute()
        }
    }

    test("failing in the running does NOT block execution") {
        when(runMock.run).thenThrow(new RuntimeException("some run exception"))
        // does not throw exception
        job.execute()

    }

    test("failing in the cleaning does NOT block execution") {
        when(runMock.cleanup).thenThrow(new RuntimeException("some cleanup exception"))
        // does not throw exception
        job.execute()
    }
}