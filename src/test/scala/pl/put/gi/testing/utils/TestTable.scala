package pl.put.gi.testing.utils

import org.scalaquery.ql.basic.BasicTable

/**
 * Simples TestTable Object possible - for learing tests.
 */
object TestTable extends BasicTable[(Long,String)]("test-table") {
  
  def id = column[Long]("id", O PrimaryKey)
  def text = column[String]("text", O NotNull)
  
  def * = id ~ text
  
}