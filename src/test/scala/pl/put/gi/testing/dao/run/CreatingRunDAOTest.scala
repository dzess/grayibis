package pl.put.gi.testing.dao.run

import java.sql.Timestamp
import java.util.Date
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FunSuite
import pl.put.gi.dao.impl.PrototypeDAO
import pl.put.gi.dao.impl.RunDAO
import pl.put.gi.db.PostgreSQLHelper
import pl.put.gi.model.Prototype
import pl.put.gi.model.Run
import pl.put.gi.testing.dao.utils.RunDAOTestBase

@RunWith(classOf[JUnitRunner])
class CreatingRunDAOTest extends RunDAOTestBase with FunSuite with BeforeAndAfterEach {

    override def beforeEach() = {
        this.beforeEachMethod()
    }

    override def afterEach() = {
        this.afterEachMethod()
    }

    test("creating run with no reference for prototype invokes exception") {
        val run = new Run()
        run.description = runDesc
        run.startTime = new Timestamp(startDate.getTime())
        run.finishTime = new Timestamp(finishDate.getTime())
        //NOTE: no prototype set 

        intercept[IllegalStateException] {
            dao.save(run)
        }
    }

    test("creatin run with no persited prototype invokes exception") {
        val prototype = new Prototype()
        prototype.description = prototypeDesc
        prototype.key = prototypeKey
        val run = new Run()
        run.description = runDesc
        run.startTime = new Timestamp(startDate.getTime())
        run.finishTime = new Timestamp(finishDate.getTime())
        // NOTE: the prototype id is undefined thus not persistable
        run.prototype = prototype

        intercept[IllegalStateException] {
            dao.save(run)
        }
    }

    test("creating run with non existing prototype invokes exception") {
        val run = CreatingRunDAOTest.this.getRun()
        // some non existing id of prototype
        run.prototype.id = -123

        intercept[RuntimeException] {
            dao.save(run)
        }
    }

}