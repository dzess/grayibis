package pl.put.gi.db

/**
 * Helper class for MySQL DB management. This class has hard coded
 * driver for MySQL.
 * 
 * FIXME: the mysql is not supported yet
 */
class MySQLHelper(val connectionString: String) extends DatabaseHelperBase {

    val typcialURL = "jdbc:mysql://localhost/test?user=fred&password=secret&ssl=true"

    if (connectionString == null || connectionString.isEmpty) {
        throw new IllegalArgumentException("connection string cannot be null")
    }

    this.currentURL = connectionString
    this.driver = "com.mysql.jdbc.Driver"
}