# Test cases for 1D symbolic regression, those come from Koza's orignals articles

x <- seq(-10,10,0.1)

koza1<- function(x) {
  y <- x^4 + x^3 + x^2 + x
  return (y)
}

koza2 <- function(x){
  y <- x^5 - 2*x^3 + x
  return (y)
}

koza3 <- function(x){
  y <- x^6 - 2*x^4 + x^2
  return (y)
}

k1 <- data.frame(x = x, y = koza1(x))
k2 <- data.frame(x = x, y = koza2(x))
k3 <- data.frame(x = x, y = koza3(x))

library('ggplot2')

gi.plot.2d <- function(df){
  # Helper for thesis work -- not used in any experiment
  p <- ggplot(df,aes(x=x,y=y)) 
  p <- p + geom_line(aes(x=x,y=y,colour="f(x)"))
  p <- p + geom_point() 
  p <- p + scale_color_manual("",values=c("#FF0000"))
  
  ggsave("regression.pdf",useDingbats=FALSE)  
}
