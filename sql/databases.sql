﻿-- Very simple scripts for creating schemas (databases) to provide
-- separate enviorments for testing some various things. 

-- Create data to makeup for testing 1
CREATE DATABASE "mgr-test-1"
  WITH OWNER = dzess
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;

COMMENT ON DATABASE "mgr-test-1"
  IS 'Test database for basic EDA testing. Version 1.';

-- Create database to makeup for testing 2
CREATE DATABASE "mgr-test-2"
  WITH OWNER = dzess
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;

COMMENT ON DATABASE "mgr-test-2"
  IS 'Test database for basic EDA testing. Version 2';