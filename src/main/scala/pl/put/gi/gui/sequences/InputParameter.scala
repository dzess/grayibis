package pl.put.gi.gui.sequences

/**
 * What user provided, and what is needed to be provided
 */
abstract class InputParameter {
    def name: String
}

case class StringInputParameter(name: String, 
        value: String = null) extends InputParameter {

}

case class CompositeGeneratorInputParameter(name: String,
    predefined: Seq[ISequenceGenerator],
    value: Seq[(ISequenceGenerator, Seq[InputParameter])] = null) extends InputParameter {

}
