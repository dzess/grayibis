package pl.put.gi.loggers

import ec.util.Log
import java.io.Writer
import java.io.PrintWriter
import java.io.BufferedWriter
import pl.put.gi.model.Run
import pl.put.gi.dao.extended.IMessageLogDAO

/**
 * Implementation of ECJ Log which uses the database writer.
 */
class DBLog(
    private val _logDAO: IMessageLogDAO,
    private val _run: Run,
    postAnnoucments: Boolean) extends Log(new DBWriter(_logDAO, _run), null, postAnnoucments, false) {

    override def restart(): Log = {
        return this
    }

    override def reopen(): Log = {
        return this
    }
}