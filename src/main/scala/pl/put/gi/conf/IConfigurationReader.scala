package pl.put.gi.conf

import java.io.InputStream;
import pl.put.gi.Configuration

/**
 * Reads the configuration of gray ibis from some stream.
 */
abstract trait IConfigurationReader {

    /**
     * Loads the configuration from the stream. Loaded configuration
     * is not frozen and not validated.
     */
    def load(stream: InputStream): Configuration
}