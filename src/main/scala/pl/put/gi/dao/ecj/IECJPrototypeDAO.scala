package pl.put.gi.dao.ecj

import pl.put.gi.model.ecj.ECJPrototype
import pl.put.gi.dao.IDAO

abstract trait IECJPrototypeDAO extends IDAO {
    def getPrototypes(): List[ECJPrototype]
    def getPrototypeByKey(key: String): ECJPrototype
    def getPrototype(id: Long): ECJPrototype
    def save(prototype: ECJPrototype)
}