package pl.put.gi.running.ecj.security

import org.slf4j.LoggerFactory
import pl.put.gi.util.security.NullSecurityManager

/**
 * Class created only for the purpose of catching the System.exit() calls
 * and stopping them.
 *
 * Based on the: http://stackoverflow.com/questions/5549720/how-to-prevent-calls-to-system-exit-from-terminating-the-jvm
 */
class ECJSecurityManager extends NullSecurityManager {

    private val logger = LoggerFactory.getLogger(classOf[ECJSecurityManager])

    override def checkExit(status: Int) = {
        super.checkExit(status)
        if (status != 0) {
            logger.debug("Catching the System.exit() call with invalid status message - throwing")
            throw new SecurityException()
        }

        logger.debug("Catching the System.exit() call with proper status - forwarding")
    }
}