package pl.put.gi.gui.ui

import javax.swing.JPopupMenu
import scala.swing.Component
import scala.swing.SequentialContainer

/**
 * Wrapper for the JPopupMenu based on scalax implementation.
 */
class PopupMenu(title0: String) extends Component with SequentialContainer.Wrapper { self: PopupMenu =>
    override lazy val peer: JPopupMenu = new JPopupMenu(title0)

    def show(invoker: Component, x: Int, y: Int) {
        peer.show(invoker.peer, x, y)
    }
}