package pl.put.bi.functions.real

import pl.put.bi.functions.NullaryOperation
import ec.EvolutionState
import ec.gp.GPData
import ec.gp.ADFStack
import ec.Problem
import ec.gp.GPIndividual
import pl.put.bi.data.DoubleData
import pl.put.bi.problem.DoubleRegressionProblem

abstract class Variable extends NullaryOperation {

    override def eval(state: EvolutionState, thread: Int, input: GPData, stack: ADFStack, individual: GPIndividual, problem: Problem): Unit = {
        val data = input.asInstanceOf[DoubleData]
        val rProblem = problem.asInstanceOf[DoubleRegressionProblem]
        val index = getIndex
        if (index >= rProblem.x.rank) {
            val msg = "There is no variable of %d index within case of %d rank".format(index, rProblem.x.rank)
            state.output.error(msg)
        }
        data.y = rProblem.x.variables(index)
    }

    protected def getIndex: Int
}

class X0 extends Variable {
    override def toString(): String = "x0"
    override def getIndex: Int = 0
}

class X1 extends Variable {
    override def toString(): String = "x1"
    override def getIndex: Int = 1
}

class X2 extends Variable {
    override def toString(): String = "x2"
    override def getIndex: Int = 2
}