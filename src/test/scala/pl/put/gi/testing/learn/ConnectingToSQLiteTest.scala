package pl.put.gi.testing.learn

import org.junit.runner.RunWith
import org.scalaquery.ql.basic.BasicDriver.Implicit.columnBaseToInsertInvoker
import org.scalaquery.ql.basic.BasicDriver.Implicit.queryToQueryInvoker
import org.scalaquery.ql.basic.BasicDriver.Implicit.scalaQueryDriver
import org.scalaquery.ql.basic.BasicDriver.Implicit.tableToQuery
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.session.Database
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import pl.put.gi.db.SQLiteHelper
import pl.put.gi.testing.utils.TestTable

@RunWith(classOf[JUnitRunner])
class ConnectingToSQLiteTest extends FunSuite {

    val helper = new SQLiteHelper("jdbc:sqlite::memory:")

    // this test has been disabled because is environment dependent
    ignore("Writes the table into file DB") {
        val db = Database.forURL(helper.currentURL, driver = helper.driver)

        db.withSession {
            TestTable.ddl.create

            TestTable.insert(1L, "foo")

            val query = for { t <- TestTable } yield t

            val result = query.first

            assert(result._1 === 1L)
            assert(result._2 === "foo")

            TestTable.ddl.drop
        }

    }
}