package pl.put.gi.running.ecj

import ec.Statistics
import org.slf4j.LoggerFactory
import pl.put.gi.model.Run

/**
 *  Interface for implementing simple statistics measures using ECJRunner.
 *  Note that every of this method is invoked during the time measured section.
 *
 *  By default ECJRunner uses the NullStatistics implementation below.
 */
abstract trait IStatisticsHarvester {
    def onStart(statistic: Statistics, run: Run)
    def onFinish(statistic: Statistics, run: Run)
    def onChange(statistic: Statistics, run: Run)
}
