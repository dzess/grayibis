package pl.put.gi.dao.impl

import org.scalaquery.ql.basic.BasicDriver.Implicit._
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.ql.Parameters
import org.slf4j.LoggerFactory
import pl.put.gi.db.IDatabaseHelper
import pl.put.gi.model.Log
import pl.put.gi.model.Run
import pl.put.gi.dao.ILogDAO
import pl.put.gi.settings.InnerConstants
import com.google.inject.Inject

/**
 *  Manages the ORM for [[Log]] classes used from Gray Ibis for storing logging
 *  for experiments.
 *
 *  The logDAO functionality is quite smaller than the other Data Access Objects. Note that
 *  any update on log table (which refers the run table, provides update on the run object).
 */
class LogDAO @Inject()(dbHelper: IDatabaseHelper) extends BaseDAO(dbHelper) with ILogDAO {

    private val logger = LoggerFactory.getLogger(classOf[LogDAO])

    private val runDAO = new RunDAO(dbHelper)

    // queries templates
    private val getLogQuery = this.generateOneQuery(Log)
    private val getLogsQuery = this.generateAllQuery(Log)
    private val maxIdQuery = this.generateMaxQuery(Log)

    override def addLog(log: Log) = {
        logger.debug("Adding the Log: {}", log)

        this.db.withTransaction {

            log.id = this.getNewId(this.maxIdQuery)
            val tuple = Log.fromModel(log)

            // simple access to run (should be already there)
            val runId = log.run.id
            val superTuple = (tuple._1, runId, tuple._3)

            logger.debug("Creating the object in DB: {}", superTuple)
            Log.insert(superTuple)
        }

        // TODO: every update on the log table updates the constraint relation to the run table
        // note that this code might be a bit more 
        // val run = runDAO.getRun(log.run.id)
        // runDAO.save(run)

        logger.debug("Adding finished")
    }

    override def getLogs(): List[Log] = {
        logger.debug("Querying DB for Logs")

        var logs: List[Log] = null
        var runsIDs: List[Long] = null

        this.db.withSession {
            // this logs retrieval results the non paginated values
            val resultList = getLogsQuery.list

            logger.debug("Quering the DB for Log - tuples {}", resultList)

            // remapping requires some sort of intense caching to be done
            logs = resultList.map(x => Log.toModel(x)).toList
            runsIDs = resultList.map(x => x._2).toList

        }

        def actionFunc(x: Log, y: Run) = { x.run = y }
        def retFunc(i: Long): Run = { runDAO.getRun(i) }

        logs = this.performEagerLoading(logs, runsIDs, retFunc, actionFunc)

        logger.debug("Quering DB for Logs - resulted {}", logs)
        return logs
    }

    override def getOne(id: Long): Log = {
        logger.debug("Quering DB for the Log with ID {}", id)

        var log: Log = null
        var runId: Long = InnerConstants.undefinedIdentifier
        this.db.withSession {
            val query = this.getLogQuery(id)
            val tuple = query.first
            logger.debug("Found log tuples - {}", log)
            log = Log.toModel(tuple)
            runId = tuple._2
        }

        // perform eager loading of this guys
        val r = runDAO.getRun(runId)
        log.run = r

        return log
    }

    override def dropTable() = this.dropTableBase(Log)
    override def createTable() = this.recreateTableBase(Log)
    override def isTableCreated(): Boolean = this.isTableCreatedBase(Log)
}