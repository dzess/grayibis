package pl.put.gi.gui.sequences

import pl.put.gi.model.ecj.ECJPrototype

abstract class SequenceGeneratorBase extends ISequenceGenerator {
    override def toString(): String = this.getClass.getName

    protected def findParameter[T](param: InputParameter, params: Seq[InputParameter]): T = {
        val found = params.find(x => (x.name == param.name))

        val result = found match {
            case Some(StringInputParameter(_, value)) => value
            case Some(CompositeGeneratorInputParameter(_, _, values)) => values
            case None => throw new IllegalStateException("not found parameter from inputs")
        }
        // this ugly cast is needed to make this method flawless in inherited classes
        result.asInstanceOf[T]
    }
}